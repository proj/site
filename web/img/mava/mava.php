<?php

/// On précise à PHP que le fichier sera une image
header('Content-type: image/png');

/// On définit la graine
$username = false;

if (isset($_GET['username'])) {
	srand(hexdec(substr(md5($_GET['username']), 0, 6)));
	$username = $_GET['username'];
} else {
	srand();
}

/// Création de l'image
$mava = imagecreatetruecolor(75, 75) or die('Erreur lors de la generation de l\'image');

/// Création des couleurs
$pink  = imagecolorallocate($mava, 255, 0, 255);
$white = imagecolorallocate($mava, 255, 255, 255);

$file = 'cache/' . $username . '.png';

// Si l'image existe déjà, utilisation de celle-ci
if (file_exists($file)) {
	readfile($file);
	exit;
}

/// Fond rose
imagefill($mava, 0, 0, $pink);

/// Création des parties du corps
$what = rand(1, 6);

switch ($what) {
	case 1: // morp
		$parts = [
			'bg'        => rand(1, 10),
			'body'      => rand(1, 1),
			'mouth'     => rand(1, 12),
			'arms'      => rand(1, 9),
			'eyes'      => rand(1, 15),
			'accessory' => rand(1, 9)
		];
		break;

	case 2: // smiley-survivor (v2)
		$parts = [
			'body'  => rand(1, 8),
			'mouth' => rand(1, 6),
			'eyes'  => rand(1, 4),
			'hat'   => rand(0, 1)
		];
		break;

	case 3: // pifyz
		$parts = [
			'bg'    => rand(1, 2),
			'body'  => rand(1, 14),
			'legs'  => rand(1, 9),
			'arm'   => rand(1, 7),
			'eyes'  => rand(1, 19),
			'mouth' => rand(1, 23),
			'hat'   => rand(1, 10)
		];
		break;

	case 4: // smiley-survivor
		$parts = [
			'body'  => rand(1, 8),
			'mouth' => rand(1, 11),
			'eyes'  => rand(1, 10),
			'hat'   => rand(0, 4)
		];
		break;

	case 5: // rplife
		$parts = [
			'body'  => rand(1, 4),
			'head'  => rand(1, 1),
			'legs'  => rand(1, 4),
			'mouth' => rand(1, 8),
			'hair'  => rand(1, 1)
		];
		break;

	case 6: // bonhomme-de-neige
		$parts = [
			'bg'  => rand(1, 2),
			'body'  => rand(1, 1),
			'sweater'  => rand(1, 2),
			'eyes' => rand(1, 3),
			'mouth'  => rand(1, 5),
			'scarf'  => rand(1, 2),
			'arms'  => rand(1, 2),
			'nose'  => rand(1, 3),
			'hat'  => rand(1, 6)
		];
		break;
}

/// Assemblage des parties du corps
foreach ($parts as $k => $v) {
	$file    = dirname(__FILE__) . '/parts/' . $what . '/' . $k . '/' . $v . '.png';
	$tmp_img = imagecreatefrompng($file);

	if (!$tmp_img)
		die('Erreur de chargement du fichier : ' . $file);

	$x = 0;

	if ($what == 2)
		$x = -2.5;

	imagecopy($mava, $tmp_img, $x, 0, 0, 0, 75, 75);
	imagedestroy($tmp_img);
}

/// Fond rose rendu transparent
imagecolortransparent($mava, $pink);

/// Affiche l'image
if ($username)
	imagepng($mava, 'cache/' . $username . '.png');

imagepng($mava);
imagedestroy($mava);

