(function() {
  var i, toggle_box, toggle_boxs, toogle_links;

  toggle_boxs  = document.getElementsByClassName('toggle');
  toogle_links = document.getElementsByClassName('toggle-link');

  toggle_box = function() {
    var box;

    box = document.getElementById(this.getAttribute('data-toggle'));
    box.style.display = box.style.display === 'none' ? 'block' : 'none';

    return false;
  };

  for (i = 0 ; i < toggle_boxs.length ; i++) {
    toggle_boxs[i].style.display = 'none';
  }

  for (i = 0 ; i < toogle_links.length ; i++) {
    toogle_links[i].onclick = toggle_box;
  }
})();