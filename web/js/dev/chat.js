(function() {
  var $ = function (e) {
    return document.querySelector(e);
  };

  /* Pour faire un xhr rapidement, comme avec JQuery (même syntaxe)
   -----------------------------------------------------------------*/
  $.xhr = function () {
    return new XMLHttpRequest();
  };

  $.get = function (url, callback) {
    var xhr = this.xhr();
    xhr.onreadystatechange = function () {
      if (xhr.readyState == 4) {
        if (xhr.status == 200)
          callback(xhr.responseText);
      }
    };

    xhr.open('GET', url, true);
    xhr.send(null);
  };

  $.post = function (url, data, callback) {
    var xhr = this.xhr();
    xhr.onreadystatechange = function () {
      if (xhr.readyState == 4)
        if (xhr.status == 200)
          callback(xhr.responseText);
    };

    var postData = '';

    for (var i in data)
      postData += encodeURIComponent(i) + '=' + encodeURIComponent(data[i]) + '&';

    postData = postData.slice(0, -1);

    xhr.open('POST', url, true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send(postData);
  };

  $.sync_post = function (url, data) {
    var xhr = this.xhr();
    var postData = '';

    for (var i in data)
      postData += encodeURIComponent(i) + '=' + encodeURIComponent(data[i]) + '&';

    postData = postData.slice(0, -1);

    xhr.open('POST', url, false);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send(postData);
  };

  /* Namespace pour les fonctions du chat
   --------------------------------------- */
  var Chat = {};

  Chat.allow_update = true;

  /* Initialisation du chat
   ------------------------- */
  Chat.init = function () {
    // on implémente l'UI
    $('#input').onkeydown = function (e) {
      e = e || window.event; // « Parce qu'on sait jamais ! » -Christophe Maé
      var key = e.keyCode || e.which; // ²

      if (key == 13) //Entrée
        Chat.post();
    };

    $('#submit').onclick = function () {
      Chat.post();
    };

    window.onfocus = function () {
      Chat.nbNotif = 0;
    };

    // initialise le truc des notifs
    this.nbNotif = 0;

    // on lance le timer de refresh
    this.update();
    this.timer = window.setInterval(this.update, 500);
  };

  /* Ajoute un ou plusieurs messages
   ---------------------------------- */
  Chat.add = function (msg) {
    var div_msg = $('#msg');

    div_msg.innerHTML = msg + div_msg.innerHTML;
  };

  /* Poste un message et efface le contenu de l'input
   --------------------------------------------------- */
  Chat.post = function () {
    Chat.allow_update = false;

    var input = $('#input');

    var msg = input.value;
    input.value = '';
    input.focus();

    $.sync_post('/ajax/chat.php?post', {msg: msg}, function (data) {

    });

    Chat.allow_update = true;
  };

  /* Update des messages
   ---------------------- */
  Chat.update = function () {
    if (Chat.allow_update) {
      $.get('/ajax/chat.php?new=' + Chat.lmid, function (data) {
        data = JSON.parse(data);

        // id du dernier msg
        Chat.lmid = data.lmid;

        // ajout du dernier msg
        Chat.add(data.messages);

        // update des gens connectés
        $('#logged').innerHTML = 'En ligne : ' + data.connectes;

        // update des notifs
        Chat.nbNotif += data.mcount;
        Chat.notify();
      });
    }
  };

  /* Nettoyage des messages
   ------------------------- */
  Chat.clean = function () {
    $('#msg').innerHTML = '';
  };

  /* Notification dans le title
   ----------------------------- */
  Chat.notify = function () {
    if (this.nbNotif === 0 || document.hasFocus()) {
      document.title = 'Chat BMC';
      this.nbNotif = 0;
    } else {
      document.title = '(' + this.nbNotif + ') Chat BMC';
    }
  };

  /* On met tout ça en action! :)
   ------------------------------- */
  Chat.init();
})();
