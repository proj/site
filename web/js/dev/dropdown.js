(function() {
	var dropdowns = document.getElementsByClassName('dropdown-toggle');

	for (var i = 0, l = dropdowns.length ; i < l ; i++) {
		var dropdown = dropdowns[i];

		dropdown.addEventListener('click', function() {
			var parent = this.parentNode;
			var menu = parent.getElementsByClassName('dropdown-menu')[0];

			menu.style.display = menu.style.display == 'none' ? 'block' : 'none';
		});

		var parent = dropdown.parentNode;
		var menu   = parent.getElementsByClassName('dropdown-menu')[0];

		menu.style.display = 'none';

		console.log(menu);
	}

	document.addEventListener('click', function(e) {
		for (var i = 0, l = dropdowns.length ; i < l ; i++) {
			var dropdown = dropdowns[i];
			var target = e.target;

			do {
				if (target == document.body) {
					var parent = dropdown.parentNode;
					var menu   = parent.getElementsByClassName('dropdown-menu')[0];

					menu.style.display = 'none';
					break;
				}

				if ((target == dropdown) || target.classList && target.classList.contains('dropdown-menu'))
					break;

			} while (target = target.parentNode);
		}
	});
})();
