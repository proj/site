(function() {
	var dropdowns = document.getElementsByClassName('dropdown-toggle');

	for (var i = 0, l = dropdowns.length ; i < l ; i++) {
		var dropdown = dropdowns[i];

		dropdown.addEventListener('click', function() {
			var parent = this.parentNode;
			var menu = parent.getElementsByClassName('dropdown-menu')[0];

			menu.style.display = menu.style.display == 'none' ? 'block' : 'none';
		});

		var parent = dropdown.parentNode;
		var menu   = parent.getElementsByClassName('dropdown-menu')[0];

		menu.style.display = 'none';

		console.log(menu);
	}

	document.addEventListener('click', function(e) {
		for (var i = 0, l = dropdowns.length ; i < l ; i++) {
			var dropdown = dropdowns[i];
			var target = e.target;

			do {
				if (target == document.body) {
					var parent = dropdown.parentNode;
					var menu   = parent.getElementsByClassName('dropdown-menu')[0];

					menu.style.display = 'none';
					break;
				}

				if ((target == dropdown) || target.classList && target.classList.contains('dropdown-menu'))
					break;

			} while (target = target.parentNode);
		}
	});
})();

(function() {
  var i, toggle_box, toggle_boxs, toogle_links;

  toggle_boxs  = document.getElementsByClassName('toggle');
  toogle_links = document.getElementsByClassName('toggle-link');

  toggle_box = function() {
    var box;

    box = document.getElementById(this.getAttribute('data-toggle'));
    box.style.display = box.style.display === 'none' ? 'block' : 'none';

    return false;
  };

  for (i = 0 ; i < toggle_boxs.length ; i++) {
    toggle_boxs[i].style.display = 'none';
  }

  for (i = 0 ; i < toogle_links.length ; i++) {
    toogle_links[i].onclick = toggle_box;
  }
})();