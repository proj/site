insert into forum_category (id, title, slug, description, position, visible)
values
(1, 'Blabla', 'blabla', '', 1, 1),
(2, 'Projets', 'projets', '', 2, 1),
(3, 'Aide', 'aide', '', 3, 1),
(4, 'Ressources', 'ressources', '', 4, 1),
(5, 'Exercices', 'exercices', '', 7, 1),
(6, 'Autre', 'autre', '', 5, 1),
(7, 'Important', 'important', '', 6, 1);

insert into user_info (name, password, signature, site, permissions,
	chat_heartbeat, date_register, date_last_activity, notes, deleted, sid, token)
values
('admin', '$2a$12$aU/JZQUAOOqN7fi35LkGve.rVqAn8B0l62slbCWYiLCtTj9JhmF5C', '',
	'', 255, 0, 0, 0, '', 0, '', '');
