create table if not exists article_info (
  id int(12) unsigned not null,
  id_author int(12) unsigned not null,
  title varchar(512) null,
  content text null,
  date_article int(10) unsigned null,
  tags varchar(512) null,
  status tinyint null default 2,
  primary key (id)
) engine = innodb;

create table if not exists article_category (
  id int(12) unsigned not null,
  title varchar(512) null,
  slug varchar(512) null,
  description text null,
  primary key (id)
) engine = innodb;

create table if not exists article_info_category (
  id_article int(12) unsigned not null,
  id_category int(12) unsigned not null,
  primary key (id_article, id_category)
) engine = innodb;
