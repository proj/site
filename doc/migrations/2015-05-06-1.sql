create table if not exists tuto_contributor (
  id_tuto int(12) unsigned not null,
  id_user int(12) unsigned not null,
  permissions int(12) unsigned null default 0,
  primary key (id_tuto, id_user)
) engine = InnoDB;

insert into tuto_contributor
select id, id_author, (8 + 4 + 2 + 1)
from tuto_info;

alter table tuto_info
drop column id_author;

alter table tuto_info
add column visibility tinyint(1) null;

update tuto_info
set visibility = if (visible <> 1, 0, 3);

alter table tuto_info
drop column visible;
