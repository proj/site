alter table tuto_contributor
add column id int(12) unsigned not null auto_increment primary key;

alter table  user_contact
add column id int(12) unsigned not null auto_increment primary key;

alter table  mail_read
add column id int(12) unsigned not null auto_increment primary key;

alter table  mail_participant
add column id int(12) unsigned not null auto_increment primary key;

alter table  forum_read
add column id int(12) unsigned not null auto_increment primary key;
