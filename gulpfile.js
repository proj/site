var gulp = require('gulp'),
	stylus = require('gulp-stylus'),
	mincss = require('gulp-minify-css');

gulp.task('stylus', function() {
	gulp.src([ './web/stylus/style.styl' ])
		.pipe(stylus({ compress: true, 'include css': true }))
		.pipe(mincss())
		.pipe(gulp.dest('./web/css'));
});

gulp.task('watch', function() {
	gulp.watch('./web/stylus/**/*.styl', [ 'stylus' ]);
});

// à faire : rajouter la compilation des fichiers coffeescript
