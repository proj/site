<?php

abstract class TutorialContributorPermission {
	const CAN_CHANGE_INFORMATIONS = 1;
	const CAN_CHANGE_VISIBILITY   = 2;
	const CAN_MANAGE_CONTRIBUTORS = 4;
	const IS_CREATOR              = 8;
}
