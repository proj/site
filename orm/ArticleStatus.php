<?php

abstract class ArticleStatus {
	const STATUS_PRIVATE     = 0;
	const STATUS_UNPUBLISHED = 1;
	const STATUS_PULISHED    = 2;
}
