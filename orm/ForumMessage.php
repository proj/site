<?php

class ForumMessage extends Model {
	protected $columns = 'content';
	protected $hasDateAdd = true;
	protected $hasDateUpd = true;

	public function topic() {
		return $this->belongsTo('ForumMessage');
	}

	public function author() {
		return $this->belongsTo('User');
	}
}
