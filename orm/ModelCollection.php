<?php

class ModelCollection implements IteratorAggregate, ArrayAccess, Countable {
	private $elements = [];

	public function __construct() {
	}

	// foreach ($var as $v)
	public function getIterator() {
		foreach ($this->elements as $element)
			yield $element;
	}

	// $var[$key]
	public function offsetGet($key) {
		return $this->elements[$key];
	}

	// $var[$key] = $value
	public function offsetSet($key, $value) {
		if (is_null($key))
			$this->elements[] = $value;
		else
			$this->elements[$key] = $value;
	}

	// isset($var)
	public function offsetExists($key) {
		return array_key_exists($key, $this->elements);
	}

	// unset($var)
	public function offsetUnset($key) {
		unset($this->elements[$key]);
	}

	// count($var)
	public function count() {
		return count($this->elements);
	}
}
