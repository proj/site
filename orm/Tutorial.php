<?php

class Tutorial extends Model {
	protected $tableName = 'tutorial_info';
	protected $columns = [ 'title', 'slug', 'progression', 'content', 'tags',
                          'visibility' ];
	protected $hasDateAdd = true;
	protected $hasDateUpd = true;

	public function contributors() {
		return $this->belonsToMany('User')->withColumns('permissions');
	}
}
