<?php

class ArticleCategory extends Model {
	protected $columns = [ 'title', 'slug', 'description' ];

	public function articles() {
		return $this->hasMany('Article');
	}
}
