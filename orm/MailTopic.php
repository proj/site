<?php

class MailTopic extends Model {
	protected $columns = [ 'title', 'slug' ];

	public function messages() {
		return $this->hasMany('MailMessage');
	}

	public function participants() {
		return $this->hasMany('MailParticipant');
	}
}
