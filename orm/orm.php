<?php

/*

Normalisation :
  Date & heure ajout = "date_add"
	Date & heure modification = "date_upd"
	Clé primaire = "id"
	Clé étrangère = "id_"[nom_table_parent]

*/

require '../core/Format.php';

require 'Model.php';
require 'ModelCollection.php';
require 'ModelFormat.php';
require 'ModelDb.php';

require 'Article.php';
require 'ArticleCategory.php';
require 'ArticleStatus.php';

require 'ChatMessage.php';

require 'FluxMessage.php';
require 'FluxMessageComment.php';
require 'FluxMessageVisibility.php';

require 'ForumCategory.php';
require 'ForumMessage.php';
require 'ForumTopic.php';

require 'MailMessage.php';
require 'MailTopic.php';

require 'Tutorial.php';
require 'TutorialContributorPermission.php';
require 'TutorialVisibility.php';

require 'User.php';
require 'UserBanishment.php';
require 'UserPermission.php';

$dbConfig = require '../conf/db.php';

$db = new ModelDb(
	$dbConfig['host'],
	$dbConfig['dbname'],
	$dbConfig['username'],
	$dbConfig['passwd']);

Model::setProvider($db);

$category = ForumCategory::find(1);
$topic = ForumTopic::find(3);

$topic->category = $category;

echo '<pre>';
var_dump($topic);
exit;
var_dump($topic->slug);
$topic->title = 'essai';
$topic->save();
echo '</pre>';

/*
$user = User::find(1);
$category = ForumCategory::find(1);

$topic = new ForumTopic();
$topic->category = $category;
$topic->title = 'Hello World!';
$topic->save();

$message = new ForumMessage();
$message->topic = $topic;
$message->user = $user;
$message->content = 'Hello everyone! How are you?';
$message->save();
*/
