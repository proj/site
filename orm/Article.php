<?php

class Article extends Model {
	protected $tableName = 'article_info';
	protected $columns = [ 'title', 'slug', 'content', 'tags', 'status' ];
	protected $hasDateAdd = true;

	public function author() {
		return $this->belongsTo('User', 'id_author');
	}

	public function categories() {
		return $this->belongsToMany('ArticleCategory', 'article_info_category',
			'id_article', 'id_category');
	}
}
