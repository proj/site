<?php

class FluxMessageComment extends Model {
	protected $columns = [ 'content' ];
	protected $hasDateAdd = true;

	public function author() {
		return $this->belongsTo('User');
	}
}
