<?php

class UserBanishment extends Model {
	protected $columns = [ 'dateBegin', 'dateEnd', 'reason', 'permissions',
                          'permissionsBefore' ];

	public function banned() {
		return $this->belongsTo('User');
	}

	public function banisher() {
		return $this->belongsTo('User');
	}
}
