<?php

class ForumCategory extends Model {
	protected $columns = [ 'title', 'slug', 'description', 'position',
	                        'visible' ];

	public function topics() {
		return $this->hasMany('ForumTopic');
	}
}
