<?php

class FluxMessage extends Model {
	protected $columns = [ 'content', 'visibility' ];
	protected $hasDateAdd = true;

	public function author() {
		return $this->belongsTo('User');
	}

	public function comments() {
		return $this->hasMany('FluxMessageComment');
	}
}
