<?php

class ModelFormat {
	// AaaBbbCcc => aaa_bbb_ccc
	public static function underscorify($name) {
		$matches = preg_split('/(?=[A-Z])/', $name, -1, PREG_SPLIT_NO_EMPTY);

		$matches = array_map(function($match) {
			return strtolower($match);
		}, $matches);

		return join('_', $matches);
	}
}
