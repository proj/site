<?php

class Model {
	/** @var string Nom de la table. Si aucun nom n'est fournit, il est
	                automatiquement déduit du nom de la classe */
	protected $tableName;

	/** @var array Attributs récupéres depuis la base de données */
	protected $attributes;

	/** @var array Colonnes de la table, hors date_add, date_upd et les clés
	               étrangères */
	protected $columns = [];

	/** @var ModelDb Connexion à la base de données */
	protected $connection;

	/** @var ModelDb Connexion à la base de données */
	protected static $provider;

	/** @var bool La tableau possède-t-elle une colonne « date_add » ? */
	protected $hasDateAdd = false;

	/** @var bool La tableau possède-t-elle une colonne « date_upd » ? */
	protected $hasDateUpd = false;

	/**
	 * @param $id
	 *
	 * @return static
	 */
	public static function find($id) {
		$instance = new static();

		$sql = '
			select *
			from ' . $instance->getTableName() . '
			where id = :id;
		';

		$result = $instance->connection->select($sql, [
			'id' => $id
		])->one();

		$instance->attributes = $result ?: [];

		return $instance;
	}

	/**
	 * @return array
	 */
	public static function all() {
		return [];
	}

	/**
	 * @param $provider
	 */
	public static function setProvider($provider) {
		static::$provider = $provider;
	}

	/**
	 * @param array $attributes
	 */
	public function __construct($attributes = []) {
		$this->connection = static::$provider;

		$this->attributes = $attributes;
	}

	/**
	 * @param $related
	 * @param null $localKey
	 * @param null $parentKey
	 *
	 * @return mixed
	 */
	public function belongsTo($related, $localKey = null, $parentKey = null) {
		$instance = new $related();

		$localKey = $localKey ?: 'id_' . $instance->getTableName();
		$parentKey = $parentKey ?: 'id';

		$sql = '
			select *
			from ' . $instance->getTableName() . '
			where id = :id;
		';

		$result = $this->connection->select($sql, [
			'id' => $this->$localKey
		])->one();

		$instance->setAttributes($result);

		return $instance;
	}

	/**
	 * @param $related
	 *
	 * @return string
	 */
	public function hasOne($related) {
		$instance = new $related();

		return '
			select *
			from ' . $instance->getTableName() . '
			where id_' . $this->getTableName() . ' = '
				. $this->{'id_' . $this->getTableName()} . ';
		';
	}

	/**
	 * @param $related
	 * @param null $foreignKey
	 * @param null $localKey
	 *
	 * @return array
	 */
	public function hasMany($related, $foreignKey = null, $localKey = null) {
		$foreignKey = $foreignKey ?: 'id_' . $this->getTableName();
		$localKey = $localKey ?: 'id';

		$lines = [];

		$instance = new $related();

		$sql = '
			select t2.*
			from ' . $this->getTableName() . ' as t1
			inner join ' . $instance->getTableName() . ' as t2
				on t1.' . $localKey . ' = t2.' . $foreignKey . '
			where t1.id = :id;
		';

		$result = $instance->connection->select($sql, [
			'id' => $this->id
		])->all();

		foreach ($result as $line) {
			$instance = new $related();
			$instance->setAttributes($line);
			$lines[] = $instance;
		}

		return $lines;
	}

	/**
	 * @param $related
	 * @param null $pivotTableName
	 * @param null $t1Key
	 * @param null $t2Key
	 *
	 * @return array
	 */
	public function belongsToMany($related, $pivotTableName = null,
	                              $t1Key = null, $t2Key = null
	) {
		/** @var Model */
		$instance = new $related();

		// à revoir : ordre alphabétique
		$pivotTableName = $pivotTableName ?:
			$instance->getTableName() . '_' . $this->getTableName();

		$t1Key = $t1Key ?: 'id_' . $instance->getTableName();
		$t2Key = $t2Key ?: 'id_' . $instance->getTableName();

		$sql = '
			select t.*
			from ' . $instance->getTableName() . ' as t
			inner join ' . $pivotTableName . ' as pivot
				on pivot.' . $t2Key . ' = t.id
			where pivot.' . $t1Key . ' = :id;
		';

		$result = $instance->connection->select($sql, [
			'id' => $this->id
		])->all();

		$lines = [];

		foreach ($result as $line) {
			$instance = new $related();
			$instance->setAttributes($line);
			$lines[] = $instance;
		}

		return $lines;
	}

	/**
	 *
	 */
	public function save() {
		if ($this->isNew())
			$this->insert();
		else
			$this->update();
	}

	/**
	 *
	 */
	protected function insert() {
		static::$provider->insert($this->getTableName(), $this->getFields());
	}

	/**
	 *
	 */
	protected function update() {

	}

	/**
	 * @return bool
	 */
	protected function isNew() {
		return true;
	}

	/**
	 * @return string
	 */
	public function getTableName() {
		if ($this->tableName)
			return $this->tableName;
		else
			return ModelFormat::underscorify(get_called_class());
	}

	/**
	 * @return array
	 */
	public function getColumns() {
		$columns = $this->columns;

		if ($this->hasDateAdd)
			$columns[] = 'date_add';

		if ($this->hasDateUpd)
			$columns[] = 'date_upd';

		return $columns;
	}

	/**
	 * @return array
	 */
	public function getExtendedColumns() {
		$columns = $this->getColumns();

		return $columns;
	}

	/**
	 * @return array
	 */
	public function getFields() {
		$columns = $this->getColumns();

		$fields = [];

		foreach ($columns as $column)
			$fields[$column] = $this->$column;

		return $fields;
	}

	/**
	 * @param $key
	 *
	 * @return mixed|null
	 */
	public function getAttribute($key) {
		$attribute = Format::underscorify($key);

		if ($this->isAttributeExists($attribute))
			return $this->attributes[$attribute];
		else if (is_callable([ $this, $key ]))
			return $this->$key();
		else
			return null;
	}

	/**
	 * @param $key
	 * @param $value
	 */
	public function setAttribute($key, $value) {
		if ($this->isColumnFillable($key))
			$this->attributes[$key] = $value;
		else if ($this->isForeignKey($key))
			$this->attributes['id_' . $key] = $value->id;

		return;
	}

	/**
	 * @param $key
	 *
	 * @return bool
	 */
	public function isAttributeExists($key) {
		return isset($this->attributes[$key]);
	}

	/**
	 * @param $key
	 *
	 * @return bool
	 */
	public function isColumnFillable($key) {
		return in_array($key, $this->columns);
	}

	/**
	 * @param string $key
	 *
	 * @return bool
	 */
	public function isForeignKey($key) {
		return method_exists($this, $key);
	}

	/**
	 * @param $attributes
	 */
	public function setAttributes($attributes) {
		$this->attributes = $attributes;
	}

	/**
	 * @param $key
	 *
	 * @return mixed|null
	 */
	public function __get($key) {
		return $this->getAttribute($key);
	}

	/**
	 * @param $key
	 * @param $value
	 */
	public function __set($key, $value) {
		$this->setAttribute($key, $value);
	}
}
