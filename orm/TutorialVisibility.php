<?php

abstract class TutorialVisibility {
	const VISIBILITY_CLOSED = 0;
	const VISIBILITY_SHARED = 1;
	const VISIBILITY_BETA   = 2;
	const VISIBILITY_PUBLIC = 3;
}
