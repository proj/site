<?php

abstract class FluxMessageVisibility {
	const VISIBILITY_FRIENDS = 0;
	const VISIBILITY_PUBLIC  = 1;
}
