<?php

class MailMessage extends Model {
	protected $columns = [ 'content' ];
	protected $hasDateAdd = true;

	public function topic() {
		return $this->belongsTo('MailTopic');
	}

	public function author() {
		return $this->belongsTo('User');
	}
}
