<?php

class ModelDb {
	const ARR = false;
	const OBJ = true;

	protected $connection;
	protected $lastInsertSuccess;
	protected $nbQueries;

	public function __construct($host, $dbname, $username, $passwd,
		$driver = 'mysql', $driver_options = [])
	{
		try {
			$pdo_options = array_merge([
				PDO::ATTR_ERRMODE          => PDO::ERRMODE_EXCEPTION,
				PDO::ATTR_EMULATE_PREPARES => false
			], $driver_options);

			$this->connection = new PDO($driver . ':host=' . $host . ';dbname=' .
				$dbname, $username, $passwd, $pdo_options);
		} catch (Exception $e) {
			exit('<strong>Problème avec la base de données :</strong><br />' .
				$e->getMessage());
		}

		$this->nbQueries = 0;
		$this->lastInsertSuccess = false;
	}

	public function insert($table, $values = []) {
		$this->nbQueries++;

		$req = $this->connection->prepare('insert into ' . $table .
			'(' . implode(', ', array_keys($values)) . ') ' .
			'VALUES(:' . implode(', :', array_keys($values)) . ');');

		$this->lastInsertSuccess = $req->execute($values);

		return static::lastInsertId();
	}

	public function isLastInsertSuccess() {
		return $this->lastInsertSuccess;
	}

	public function update($query, $values = []) {
		$this->nbQueries++;

		return $this->connection->prepare($query)->execute($values);
	}

	public function delete($query, $values = []) {
		$this->nbQueries++;

		return $this->connection->prepare($query)->execute($values);
	}

	public function select($query, $values = []) {
		return new ModelDbStatement($query, $values, $this);
	}

	public function query($query) {
		$this->nbQueries++;

		return $this->connection->prepare($query);
	}

	public function lastInsertId($name = null) {
		return $this->connection->lastInsertId($name);
	}

	public function begin() {
		return $this->connection->beginTransaction();
	}

	public function send() {
		return $this->connection->commit();
	}

	public function back() {
		return $this->connection->rollBack();
	}

	public function getNbQueries() {
		return $this->nbQueries;
	}

	public function incrementNbQueries() {
		$this->nbQueries++;
	}

	public function getConnection() {
		return $this->connection;
	}
}

class ModelDbStatement {
	protected $query;
	protected $values;
	protected $db;

	public function __construct($query, $values, $db) {
		$this->query  = $query;
		$this->values = $values;
		$this->db     = $db;
	}

	public function count() {
		return $this->select()->rowCount();
	}

	public function one() {
		return $this->select()->fetch(PDO::FETCH_ASSOC);
	}

	public function all() {
		return $this->select()->fetchAll(PDO::FETCH_ASSOC);
	}

	protected function select() {
		$this->db->incrementNbQueries();

		$return = $this->db->getConnection()->prepare($this->query);
		$return->execute($this->values);

		return $return;
	}

	public function add($query, $values = []) {
		$this->query  .= ' ' . $query;
		$this->values  = array_merge($this->values, $values);

		return $this;
	}
}
