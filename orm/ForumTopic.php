<?php

class ForumTopic extends Model {
	protected $columns = [ 'title', 'slug', 'locked', 'pinned', 'archived' ];

	public function category() {
		return $this->belongsTo('ForumCategory');
	}

	public function messages() {
		return $this->hasMany('ForumMessage');
	}
}
