<?php

class User extends Model {
	protected $tableName = 'user_info';
	protected $columns = [ 'username', 'password', 'signature', 'site',
                           'chatHeartbeat', 'notes', 'dateRegister',
												   'dateLastActivity', 'permissions', 'deleted',
												   'sid', 'token' ];

	public function banishments() {
		return $this->hasMany('UserBanishment', 'id_user');
	}
}
