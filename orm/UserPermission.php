<?php

abstract class UserPermission {
	const POST_FLUX           = 1;
	const POST_FORUM          = 2;
	const POST_CHAT           = 4;
	const POST_TUTORIAL       = 8;
	const POST_MAIL           = 16;
	const MODERATOR           = 32;
	const ADMINISTRATOR       = 64;
	const SUPER_ADMINISTRATOR = 128;
}
