<?php

class Notification extends Model {
	protected $tableName = 'notif_info';
	protected $columns = [ 'content' ];
	protected $hasDateAdd = true;

	public function users() {
		return $this->hasMany('User');
	}
}
