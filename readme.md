     _____ _ _      _____        _____       _
    | __  |_| |_   |     |_ _   |     |___ _| |___
    | __ -| |  _|  | | | | | |  |   --| . | . | -_|
    |_____|_|_|    |_|_|_|_  |  |_____|___|___|___|
                         |___|

Code source officiel du site Bit My Code
========================================
Création du groupe : 27 avril 2013

Encore en vie en : janvier 2020

Les contributeurs du site :
- Yannick [PifyZ]
- Guillaume [Booti386]
- Aurélie [Naemy]
- [HPotter]
- Théo [Theo]
- Elisa [Elsia]
- Aymeric [SuperMonkey]
- Benoît [BSimo]
- Pedro [Algorythmis]

Conventions de nommage
=======================
- Classes : `CamelCase`
- Fonctions et méthodes : `camelCase`  
- Variables et attributs : `under_score`

Permissions
===========
- `POST_FLUX`
    - poster des messages sur son flux  
    - poster des commentaires sur les flux des contacts  

- `POST_FORUM`
   - poster des réponses sur le forum  
   - poster de nouveaux sujets sur le forum  
   - modifier ses messages sur le forum  

- `POST_CHAT`
   - poster sur le chat  

- `POST_TUTORIAL`
   - créer de nouveaux tutoriels  
   - publier ses tutoriels  
   - modifier ses tutoriels  

- `POST_MAIL`
   - créer des discussions privés  
   - répondre à des messages privés  

- `MODERATOR`
   - déplacer les sujets sur le forum  
   - renommer les sujets sur le forum  
   - modifier les messages des autres sur le forum  
   - supprimer les messages des autres sur le forum  
   - verrouiller et épingler les sujets sur le forum  
   - poster des messages sur les sujets verrouillés  

- `ADMINISTRATOR`
   - créer de nouveaux utilisateurs  
   - archiver des sujets sur le forum  
   - poster des messages sur le forum dans la catégorie "Important"  

- `SUPER_ADMINISTRATOR`
   - supprimer des utilisateurs  
   - supprimer des sujets sur le forum  
   - créer des catégories sur le forum  
   - renommer les catégories sur le forum  
   - supprimer les catégories du forum  
   - exécuter des commandes  

Tester une permission
---------------------
    if ($app->has(P::POST_FORUM)) { } // Utilisateur connecté
    // ou  
    if ($app->has(P::POST_FORUM), $id_user) { } // Utilisateur selon l'identifiant
    // ou  
    if ($user->has(P::POST_FORUM)) { } // Utilisateur de la classe UserInfo

Catégories du forum
===================
- Blabla      parler du quotidien, du groupe, etc.  
- Projet      projets en cours, terminés ou abandonnés (principalement les
                terminés/jouables)
- Aide        demande d'aide  
- Ressource   partage de liens, codes, fichiers utiles, etc.  
- Autre       graphisme, etc.  
- Exercices   exercices des membres  
- Important   sujets importants

À savoir
========
Pas de système de cache
-----------------------
Aucun système de cache n'a été mis en place car c'est principalement un forum, ce qui implique que le contenu peut évoluer à tout moment, et il est peu fréquenté.

Notifications
=============
Types de notification
---------------------
- 0 ou `SITE_MESSAGE` : message officiel
  - `id_ref` : identifiant du bannissement
- 1 ou `FORUM_REPLY` : ajout d'une réponse à un sujet du forum
  - `id_ref` : identifiant du sujet
- 2 ou `MAIL_NEW_OR_REPLY` : nouveau message privé ou ajout d'une réponse à un
  message privé
  - `id_ref` : identifiant du message privé
- 3 ou `SITE_BAN` : bannissement d'un utilisateur
  - `id_ref` : identifiant du bannissement

Mise en place
-------------
- Itération 1 : ajout des notifications dans le tableau prévu à cet effet  
- Itération 2 : regroupement des notifications communes (ex : 5 nouveaux messages
              sur « sujet »)
- Itération 3 : choix des sujets suivis
