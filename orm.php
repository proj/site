<?php

/*

Normalisation :
  Date & heure ajout = "date_add"
	Date & heure modification = "date_upd"
	Clé primaire = "id"
	Clé étrangère = "id_"[nom_table_parent]

*/

// AaaBbbCcc => aaa_bbb_ccc
function underscorify($name) {
	$matches = preg_split('/(?=[A-Z])/', $name, -1, PREG_SPLIT_NO_EMPTY);

	$matches = array_map(function($match) {
		return strtolower($match);
	}, $matches);

	return join('_', $matches);
}

class Model {
	protected $tableName;
	protected $attributes;
	protected $columns = [];

	public static function all() {
		$instance = new static();
		return $instance->newQuery()->get();
	}

	public static function find($id) {
		return new static();
	}

	public function __construct($attributes = []) {
		$this->attributes = $attributes;
	}

	public function belongsTo($related) {
		$instance = new $related();

		return '
			select *
			from ' . $this->getTableName() . '
			where id_' . $instance->getTableName() . ' = ' . $instance->{'id_' . $instance->getTableName()} . ';
		';
	}

	public function hasOne($related) {
		$instance = new $related();

		return '
			select *
			from ' . $instance->getTableName() . '
			where id_' . $this->getTableName() . ' = ' . $this->{'id_' . $this->getTableName()} . ';
		';
	}

	public function hasMany($related) {
		$instance = new $related();

		return '
			select t2.*
			from ' . $this->getTableName() . ' as t1
			inner join ' . $instance->getTableName() . ' as t2
				on t1.id = t2.id_' . $this->getTableName() . '
			where t1.id = ' . $this->id . ';
		';
	}

	public function belongsToMany($related) {

	}

	public function save() {

	}

	public function getTableName() {
		if ($this->tableName)
			return $this->tableName;
		else
			return underscorify(get_called_class());
	}

	public function getColumns() {
		$columns = $this->columns;
		$columns = array_merge($columns, [ 'id', 'date_add', 'date_upd' ]);

		return $columns;
	}

	public function getAttribute($key) {
		if ($this->isAttributeExists($key))
			return $this->attributes[$key];
		else
			return null;
	}

	public function setAttribute($key, $value) {
		if (!$this->isColumnFillable($key))
			return;

		$this->attributes[$key] = $value;
	}

	public function isAttributeExists($key) {
		return isset($this->attributes[$key]);
	}

	public function isColumnFillable($key) {
		return in_array($key, $this->columns);
	}

	public function __get($key) {
		return $this->getAttribute($key);
	}

	public function __set($key, $value) {
		$this->setAttribute($key, $value);
	}
}

class ModelCollection implements IteratorAggregate, ArrayAccess, Countable {
	private $elements = [];

	public function __construct() {
	}

	// foreach ($var as $v)
	public function getIterator() {
		foreach ($this->elements as $element)
			yield $element;
	}

	// $var[$key]
	public function offsetGet($key) {
		return $this->elements[$key];
	}

	// $var[$key] = $value
	public function offsetSet($key, $value) {
		if (is_null($key))
			$this->elements[] = $value;
		else
			$this->elements[$key] = $value;
	}

	// isset($var)
	public function offsetExists($key) {
		return array_key_exists($key, $this->elements);
	}

	// unset($var)
	public function offsetUnset($key) {
		unset($this->elements[$key]);
	}

	// count($var)
	public function count() {
		return count($this->elements);
	}
}

class ForumCategory extends Model {
	protected $columns = [ 'title', 'slug', 'description', 'position',
	                        'visible' ];

	public function topics() {
		return $this->hasMany('ForumTopic');
	}
}

class ForumTopic extends Model {
	protected $columns = [ 'title', 'slug', 'locked', 'pinned', 'archived' ];

	public function category() {
		return $this->belongsTo('ForumCategory');
	}

	public function messages() {
		return $this->hasMany('ForumMessage');
	}
}

class ForumMessage extends Model {
	protected $columns = 'content';

	public function topic() {
		return $this->belongsTo('ForumMessage');
	}

	public function author() {
		return $this->belongsTo('User');
	}
}

class User extends Model {
	protected $tableName = 'user_info';
	protected $columns = [ 'username', 'password', 'signature', 'site',
                           'chatHeartbeat', 'notes', 'dateRegister',
												   'dateLastActivity', 'permissions', 'deleted',
												   'sid', 'token' ];

	public function banishments() {
		return $this->hasMany('UserBanishment');
	}
}

class UserBanishment extends Model {
	protected $columns = [ 'dateBegin', 'dateEnd', 'reason', 'permissions',
                          'permissionsBefore' ];

	public function banned() {
		return $this->belongsTo('User');
	}

	public function banisher() {
		return $this->belongsTo('User');
	}
}

class Article extends Model {
	protected $tableName = 'article_info';
	protected $columns = [ 'title', 'slug', 'content', 'tags', 'status' ];

	public function author() {
		return $this->belongsTo('User');
	}

	public function categories() {
		return $this->belongsToMany('ArticleCategory');
	}
}

class ArticleCategory extends Model {
	protected $columns = [ 'title', 'slug', 'description' ];

	public function articles() {
		return $this->hasMany('Article');
	}
}

class ChatMessage extends Model {
	protected $columns = [ 'content' ];

	public function author() {
		return $this->belongsTo('User');
	}
}

class FluxMessage extends Model {
	protected $columns = [ 'content', 'visibility' ];

	public function author() {
		return $this->belongsTo('User');
	}

	public function comments() {
		return $this->hasMany('FluxMessageComment');
	}
}

class FluxMessageComment extends Model {
	protected $columns = [ 'content' ];

	public function author() {
		return $this->belongsTo('User');
	}
}

class MailMessage extends Model {
	protected $columns = [ 'content' ];

	public function topic() {
		return $this->belongsTo('MailTopic');
	}

	public function author() {
		return $this->belongsTo('User');
	}
}

class MailTopic extends Model {
	protected $columns = [ 'title', 'slug' ];

	public function messages() {
		return $this->hasMany('MailMessage');
	}

	public function participants() {
		return $this->hasMany('MailParticipant');
	}
}

class Notification extends Model {
	protected $columns = [ 'content' ];

	public function users() {
		return $this->hasMany('User');
	}
}

class Tutorial extends Model {
	protected $tableName = 'tutorial_info';
	protected $columns = [ 'title', 'slug', 'progression', 'content', 'tags',
                          'visibility' ];

	public function contributors() {
		return $this->belonsToMany('User')->withColumns('permissions');
	}
}

echo '<pre>';
$topic = ForumTopic::find(1);
var_dump($topic->category());
echo '</pre>';
