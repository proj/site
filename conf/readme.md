# FR
/conf/db.php contient des informations d'authentification privées pour la base de
données SQL. C'est pour cela ce fichier n'est pas inclus dans le repo et qu'un fichier
exemple /conf/db.php.sample décrit son contenu. Pour obtenir un site fonctionnel en local,
vous avez besoin de renommer /conf/db.php.sample en /conf/db.php et de le remplir avec les
bonnes informations de connexion.

Grunt est nécessaire afin de gérer la transformation de vos fichiers situés dans /web/ en jolis fichiers opti dans le chouette dossier qu'est /dist/.

Pour cela, vous aurez besoin de Node. Donc si vous ne l'avez pas déjà, allez sur http://nodejs.org/ et cliquez sur le gros bouton vert.

Ensuite, il est temps de dégainer votre terminal, ou ligne de commandes. Vous y inscrirez la ligne suivante:
"npm install -g grunt-cli"
(un "sudo", ou des droits d'administrateur sous Windows seront peut-être nécessaires.)

Ensuite, à la racine de ce projet, faites un petit:
"npm install"

Pour commencer à contribuer et modifier des fichiers, vous aurez juste besoin de faire
"grunt watch"

Et le tour est joué. :)

# EN
/conf/db.php contains private authentication informations for SQL database. Thus, this file
is not included in the repository, and a sample file /conf/db.php.sample describes its contents.
To get this site working, you need to copy conf/db.php.sample into /conf/db.php and fill it
with right credentials.
