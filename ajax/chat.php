<?php


session_start();

/*
	chat_info :
		id (int)
		uid (int)
		writing (bool)
		heartbeat (int)
		sound_enabled (bool)
		banned (bool)

	A faire :
	* petit son pour prévenir
	* utilisateurs qui écrivent
	* outils de modération
	* notification dans le titre de la page
	* BBCode & smileys
	* Interpreteur de commandes
	* Ajout du token CSRF
*/

require '../app/models/Db.php';
require '../core/Format.php';

$db_infos = require '../conf/db.php';

DB::init($db_infos['host'], $db_infos['dbname'], $db_infos['username'], $db_infos['passwd']);

/// Indique si un message commence par $str et retourne les options
function beginby($msg, $str) {
	return (substr($msg->content, 0, strlen($str)) == $str)
		? substr($msg->content, strlen($str))
		: null;
}

function follow($path) {
	header('Location: ' . $path);
	exit;
}

class Chat {
	/// Poste sur le chat à partir des données en POST
	public static function post() {
		$time = time();
		$id = isset($_SESSION['id']) ? $_SESSION['id'] : 0;
		$msg = htmlentities($_POST['msg']);

		if (empty($msg))
			exit();

		DB::insert('chat_message', [
			'date_add'  => $time,
			'id_author' => $id,
			'content'   => $msg
		]);
	}

	/// Récupère les messages & les utilisateurs connectés
	public static function get() {
		$lmid = is_numeric(@$_GET['new']) ? $_GET['new'] : 0;

		// On récupère les 90 dernier messages
		$messages = DB::select('
			msg.*, user.username username
			from chat_message msg
			inner join user_info user on user.id = msg.id_author
			where msg.id > :lmid
			order by msg.id DESC
			limit 90
		', [
			'lmid' => $lmid
		])->all();

		if (count($messages) > 0) {
			$lmid = $messages[0]->id;
		}

		$msg = '';

		$cur_date = '00/00/0000';

		foreach ($messages as $message) {
			$last_date = date('d/m/Y', $message->date_add);

			if ($cur_date != $last_date && $last_date != date('d/m/Y', time())) {
				$msg .= '<div class="chat-date">' . $last_date . '</div>';
				$cur_date = $last_date;
			}

			$msg .= static::format($message);
		}

		// On récupère la liste des connectés
		$logd = '';

		$connectes = DB::select('
			username
			from user_info
			where chat_heartbeat >= ' . (time() - 60) . '
		')->all();

		foreach ($connectes as $c) {
			$logd .= $c->username . ", ";
		}

		$logd = trim($logd, ', ');

		$json = [
			'messages' 	=> $msg,
			'connectes'	=> $logd,
			'lmid'		=> $lmid,
			'notif'		=> '',
			'mcount'	=> count($messages)
		];

		print json_encode($json);

		// Mise à jour du heartbeat
		static::heartbeat();
	}

	/// Met à jour le dernier timestamp d'update dans la db
	public static function heartbeat() {
		$now = time();

		DB::update('
			user_info
			set chat_heartbeat = :timestamp
			where id = :uid;
		', [
			'timestamp' => $now,
			'uid'       => $_SESSION['id']
		]);
	}

	/// Formate un message
	public static function format($msg) {
		// pas de commande, on renvoie un affichage "classique" du message
		if ($msg->content[0] != '/')
			return '<strong>[' . date('H:i', $msg->date_add) . '] ' . $msg->username . '</strong> : ' . $msg->content . '<br />';

		if ($opt = beginby($msg, '/me '))
			return '<strong>' . Format::username($msg->username) . '</strong>' . ' ' . $opt . '<br />';

		if ($opt = beginby($msg, '/afk '))
			// à faire

		if ($opt = beginby($msg, '/ban '))
			// à faire

		if ($opt = beginby($msg, '/clear '))
			// à faire

		return '';
	}
}

/// Pour avoir tous les messages, users connectés, ...
if (isset($_GET['new']) or !count($_GET))
	Chat::get();

/// Poste d'un nouveau message
if (isset($_GET['post']))
	Chat::post();
