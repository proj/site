<?php

/**
 * Classe principale pour gérer l'application
 */
class App {
	/**
	 * Chemin de la page actuellement ouverte
	 */
	public $path = null;

	/**
	 * Connexion à la base de données
	 */
	public $db = null;

	/**
	 * Liste des routes du site
	 */
	protected $routes = [];

	/**
	 * Fonction exécuté si la page n'est pas trouvée
	 */
	protected $func_error = null;

	/**
	 *
	 */
	protected $filters = [];

	/**
	 * Configurations du site
	 */
	protected $configs = [];

	/**
	 * Configurations par défaut de l'application
	 */
	protected $defaultConfig = [
		// Informations sur l'application
		'app.timezone' => 'Europe/Paris',
		'app.session'  => false,

		// Informations de connexion à la base de données
		'db.host'     => 'localhost',
		'db.dbname'   => 'hum',
		'db.username' => 'root',
		'db.passwd'   => '',
		'db.dsn'      => 'mysql',
		'db.connect'  => false
	];

	protected $shortcuts = [
		'{char}'     => '([a-zA-Z_])',      // caractère
		'{digit}'    => '([0-9])',          // chiffre
		'{string}'   => '([a-zA-Z_]+)',     // chaine
		'{number}'   => '([0-9]+)',         // nombre
		'{alphanum}' => '([a-zA-Z0-9_-]+)', // alphanumérique
		'{paginate}' => '(/[0-9]*)?',       // pagination
		'{*}'        => '([a-zA-Z0-9_-]+)'  // n'importe quel chaine
	];

	/**
	 * Créer l'application
	 *
	 * @param array $configs Configurations par défaut du site
	 */
	public function __construct($configs = []) {
		spl_autoload_register(function($className) {
			$file = realpath(__DIR__ . '/' . $className . '.php');

			if (file_exists($file))
				require $className . '.php';
		});

		$this->setConfigs(array_merge($this->defaultConfig, $configs));
		$this->setTimezone($this->configs['app.timezone']);

		// Si la connexion à la bdd est voulue (db.connect == true)
		// alors celle-ci est effectuée
		if ($this->configs['db.connect']) {
			$this->dbConnect(
				$this->configs['db.host'],
				$this->configs['db.dbname'],
				$this->configs['db.username'],
				$this->configs['db.passwd'],
				$this->configs['db.dsn']
			);
		}

		if ($this->configs['app.session'])
			session_start();

		Request::init();

		$this->path = Request::$server['PATH_INFO'];
	}

	public function getHost() {
		// à faire : http ou https selon la configuration du serveur
		return 'https://' . $_SERVER['HTTP_HOST'];
	}

	public function getUrl() {
		return $this->getHost() . $this->path;
	}

	/**
	 * Redéfinir toutes les configurations
	 *
	 * @param array Liste des configurations
	 *
	 * @return App
	 */
	public function setConfigs($configs) {
		$this->configs = $configs;

		return $this;
	}

	/**
	 * Récupérer toutes les configurations
	 *
	 * @return array Liste des configurations
	 */
	public function getConfigs() {
		return $this->configs;
	}

	/**
	 * Définir une configuration
	 *
	 * @param string $configName Nom de la configuration
	 * @param string $configValue Valeur de la configuration
	 *
	 * @return App
	 */
	public function setConfig($configName, $configValue) {
		$this->configs[$configName] = $configValue;

		return $this;
	}

	/**
	 * Récupérer une configuration
	 *
	 * @param string $configName Nom de la configuration
	 *
	 * @return mixed Valeur de la configuration
	 */
	public function getConfig($configName) {
		return isset($this->configs[$configName])
			? $this->configs[$configName]
			: false;
	}

	/**
	 * Connexion à la base de données
	 *
	 * @param string $host Hôte de la base
	 * @param string $dbname Nom de la base de données
	 * @param string $username Nom d'utilisateur
	 * @param string $passwd Mot de passe de l'utilisateur
	 * @param string $dsn Data Source Name - Nom du pilote PDO
	 * @param array $driverOptions Options spécifiques de connexion
	 *
	 * @return App
	 */
	public function dbConnect($host, $dbname, $username, $passwd = '',
		$dsn = 'mysql', $driverOptions = []
	) {
		$this->db = new Db($host, $dbname, $username, $passwd, $dsn, $driverOptions);

		return $this;
	}

	/**
	 * Définit le fuseau horaire de l'application
	 *
	 * @param string $timezone Nom du fuseau horaire
	 *        (cf. http://www.php.net/manual/fr/timezones.php)
	 *
	 * @return App
	 */
	public function setTimezone($timezone) {
		$this->configs['app.timezone'] = $timezone;

		Date::setTimezone($this->configs['app.timezone']);

		return $this;
	}

	/**
	 * Défini une route
	 *
	 * @param string $name Nom de la route, utile pour les redirections vers
	 *        celle-ci
	 * @param string $path Adresse pour accéder à la route
	 * @param Closure $callback Fonction appelée lorsque la route est trouvée
	 * @param string $method
	 *
	 * @return App
	 */
	public function route($name, $path, $callback, $method = 'GET') {
		$this->routes[$name] = [
			'path'     => $path,
			'callback' => $callback,
			'method'   => $method
		];

		return $this;
	}

	/**
	 * Définit une route
	 *
	 * @param string $name Nom de la route, utile pour les redirections vers
	 *        celle-ci
	 * @param string $path Adresse pour accéder à la route
	 * @param Closure $callback Fonction appelée lorsque la route est trouvée
	 *
	 * @return App
	 */
	public function get($name, $path, $callback) {
		return $this->route($name, $path, $callback, 'GET');
	}

	/**
	 * Définit une route
	 *
	 * @param string $name Nom de la route, utile pour les redirections vers
	 *        celle-ci
	 * @param string $path Adresse pour accéder à la route
	 * @param Closure $callback Fonction appelée lorsque la route est trouvée
	 *
	 * @return App
	 */
	public function post($name, $path, $callback) {
		return $this->route($name, $path, $callback, 'POST');
	}

	/**
	 * Définit une route de secours si aucune n'est trouvée
	 *
	 * @param Closure $callback Fonction appelée lorsque aucune route n'est
	*         trouvée
	 *
	 * @return App
	 */
	public function error($callback) {
		$this->func_error = $callback;

		return $this;
	}

	/**
	 * Envoie un en-tête HTTP
	 *
	 * @param string $value Spécifier l'en-tête HTTP
	 *
	 * @return App
	 */
	public function sendHeader($value) {
		Request::sendHeader($value);

		return $this;
	}

	/**
	 * Recherche la route demandée exécute son callback
	 *
	 * @return App
	 */
	public function run() {
		$found = false;

		$method = Request::getMethod();

		foreach ($this->routes as $k => $v) {
			$path = strtr($v['path'], $this->shortcuts);

			if (preg_match('#^/?' . $path . '/?$#U', $this->path, $matches)
				&& $v['method'] == $method
			) {
				array_splice($matches, 0, 1);

				$matches = array_map(function($match) {
					return trim($match, '/');
				}, $matches);

				array_unshift($matches, $this);

				call_user_func_array($v['callback'], $matches);

				$found = true;

				break;
			}
		}

		if (!$found && !is_null($this->func_error)) {
			$this->sendHeader('404');
			call_user_func($this->func_error, $this, '404');
		}

		return $this;
	}

	/**
	 * Génère un lien vers une route
	 *
	 * @param string $route_name Nom de la route
	 * @param mixed ...$args Arguments passés à la route
	 *
	 * @return string Lien vers la route
	 */
	public function genLink($routeName) {
		if ($this->routes[$routeName]['path'] == '/')
			return '/';

		// Récupérer la liste des arguments passés à la fonction
		// excepté $route_name
		$args = func_get_args();
		array_splice($args, 0, 1);

		$path = strtr($this->routes[$routeName]['path'], $this->shortcuts);
		$path = str_replace('(/[0-9]*)?', '/([0-9]*)?', $path);

		// Remplacer les structures (...) des url par les paramètres passés à la
		// fonction
		$link = preg_replace_callback('#\(.+\)#U', function($matches) use($args) {
			static $i = 0;

			if (isset($args[$i]))
				return $args[$i++];
			else
				return '';
		}, str_replace('?', '', $path));

		// Supprimer le "/" final
		$link = rtrim($link, '/');

		return '/' . $link;
	}

	/**
	 * Redirige vers le lien généré
	 *
	 * @param string $routeName Nom de la route
	 * @param mixed ...$args Arguments passés à la route
	 *
	 * @return void
	 */
	public function follow($routeName) {
		header('Location: ' . call_user_func_array([$this, 'genLink'],
			func_get_args()));

		exit;
	}

	/**
	 * Redirection vers la chaîne spécifiée
	 *
	 * @param string $str Lien vers lequel rediriger
	 *
	 * @return void
	 */
	public function followStr($str) {
		header('Location: ' . $str);

		exit;
	}

	/**
	 * Créer une vue en lui passant l'application
	 */
	public function view($file) {
		$view = new View($file);
		$view->app = $this;

		return $view;
	}

	/**
	 * Exécuter une route
	 */
	public function exec($routeName) {
		$args = func_get_args();
		array_splice($args, 0, 1);
		array_unshift($args, $this);

		call_user_func_array($this->routes[$routeName]['callback'], $args);
	}

	/**
	 *
	 */
	public function addFilter($filterName, $filterFunc) {
		$this->filters[$filterName] = [
			'func' => $filterFunc
		];
	}

	public function filter($filterName) {
		$args = func_get_args();
		array_slice($args, 0, 1);
		array_unshift($args, $this);

		call_user_func_array($this->filters[$filterName]['func'], $args);
	}
}
