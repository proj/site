<?php

abstract class ContributorPermission {
	const CAN_CHANGE_INFORMATIONS = 1;
	const CAN_CHANGE_VISIBILITY   = 2;
	const CAN_MANAGE_CONTRIBUTORS = 4;
	const IS_CREATOR              = 8;

	public static function toStr($name) {
		switch ($name) {
			case static::CAN_CHANGE_INFORMATIONS:
				return 'Peut changer les informations';
				break;

			case static::CAN_CHANGE_VISIBILITY:
				return 'Peut changer la visibilité';
				break;

			case static::CAN_MANAGE_CONTRIBUTORS:
				return 'Peut gérer les contributeurs';
				break;

			case static::IS_CREATOR:
				return 'Est le créateur';
				break;

			default:
				return '-';
		}
	}
}
