A faire :
- Une fonction comme $app->route pour des bouts de code n'ayant pas besoin de route (donc pas d'URL pour y accéder).
  Utile pour partager des bouts de code entre plusieurs routes.
- Générer une documentation (et donc documenter les classes)
