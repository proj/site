<?php

/**
 * 
 */
class Request {
	const HEAD    = 'HEAD';
	const GET     = 'GET';
	const POST    = 'POST';
	const PUT     = 'PUT';
	const PATCH   = 'PATCH';
	const DELETE  = 'DELETE';
	const OPTIONS = 'OPTIONS';

	public static $server = [];

	public static function init() {
		$defaults = [
			'REQUEST_METHOD' => 'GET',
			'SCRIPT_NAME'    => '',
			'PATH_INFO'      => '/',
			'QUERY_STRING'   => '',
			'SERVER_NAME'    => 'localhost',
			'SERVER_PORT'    => 80,
			'REMOTE_ADDR'    => '127.0.0.1'
		];

		static::$server = array_merge($defaults, $_SERVER);
	}

	public static function getMethod() {
		return static::$server['REQUEST_METHOD'];
	}

	public static function isGet() {
		return static::getMethod() == 'GET';
	}

	public static function isPost() {
		return static::getMethod() == 'POST';
	}

	public static function sendHeader($value) {
		// à faire : créer des constantes ? ::OK ::FOUND, etc.
		$values = [
			'ok'                    => 'HTTP/1.0 200 OK',
			'moved permanently'     => 'HTTP/1.0 301 Moved Permanently',
			'found'                 => 'HTTP/1.0 302 Found',
			'forbidden'             => 'HTTP/1.0 403 Forbidden',
			'not found'             => 'HTTP/1.0 404 Not Found',
			'internal server_error' => 'HTTP/1.0 500 Internal Server Error',

			'200' => 'HTTP/1.0 200 OK',
			'301' => 'HTTP/1.0 301 Moved Permanently',
			'302' => 'HTTP/1.0 302 Found',
			'403' => 'HTTP/1.0 403 Forbidden',
			'404' => 'HTTP/1.0 404 Not Found',
			'500' => 'HTTP/1.0 500 Internal Server Error'
		];

		header(isset($values[$value]) ? $values[$value] : $value);
	}
}
