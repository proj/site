<?php

abstract class P {
	const POST_FLUX           = 1;   // poster des messages sur le flux
	const POST_FORUM          = 2;   // créer un sujet, poster ou modifier une réponse
	const POST_CHAT           = 4;   // poster sur le chat
	const POST_TUTORIAL       = 8;   // créer de nouveaux tutoriels, publier des tutoriels
	const POST_MAIL           = 16;  // envoyer des messages privés
	const MODERATOR           = 32;  // verrouiller, éplinger ou renommer un sujet ou modifier un message
	const ADMINISTRATOR       = 64;  // supprimer un sujet
	const SUPER_ADMINISTRATOR = 128; // créer et supprimer des membres

	public static function user() {
		return
			static::POST_FORUM    |
			static::POST_CHAT     |
			static::POST_TUTORIAL |
			static::POST_MAIL     |
			static::POST_FLUX;
	}

	public static function moderator() {
		return
			static::POST_FORUM    |
			static::POST_CHAT     |
			static::POST_TUTORIAL |
			static::POST_MAIL     |
			static::POST_FLUX     |
			static::MODERATOR;
	}

	public static function administrator() {
		return
			static::POST_FORUM    |
			static::POST_CHAT     |
			static::POST_TUTORIAL |
			static::POST_MAIL     |
			static::POST_FLUX     |
			static::MODERATOR     |
			static::ADMINISTRATOR;
	}

	public static function superAdministrator() {
		return
			static::POST_FORUM    |
			static::POST_CHAT     |
			static::POST_TUTORIAL |
			static::POST_MAIL     |
			static::POST_FLUX     |
			static::MODERATOR     |
			static::ADMINISTRATOR |
			static::SUPER_ADMINISTRATOR;
	}

	public static function toStr($name) {
		switch ($name) {
			case 'POST_FLUX':
			case static::POST_FLUX:
				return 'Poster sur le flux';
				break;

			case 'POST_FORUM':
			case static::POST_FORUM:
				return 'Utiliser le forum';
				break;

			case 'POST_CHAT':
			case static::POST_CHAT:
				return 'Poster sur le chat';
				break;

			case 'POST_TUTORIAL':
			case static::POST_TUTORIAL:
				return 'Poster des tutoriels';
				break;

			case 'POST_MAIL':
			case static::POST_MAIL:
				return 'Envoyer des mp';
				break;

			case 'MODERATOR':
			case static::MODERATOR:
				return 'Modérateur';
				break;

			case 'ADMINISTRATOR':
			case static::ADMINISTRATOR:
				return 'Administrateur';
				break;

			case 'SUPER_ADMINISTRATOR':
			case static::SUPER_ADMINISTRATOR:
				return 'Super administrateur';
				break;

			default:
				return '-';
		}
	}
}
