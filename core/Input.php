<?php

class Input {
	public static function get($name, $default = '') {
		return isset($_POST[$name]) ? $_POST[$name] : $default;
	}
	
	public static function has($name) {
		return isset($_POST[$name]);
	}
}
