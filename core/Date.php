<?php

/**
 * Gestion des dates
 */
class Date {
	/**
	 * @var string Formatage par défaut de la date
	 *
	 * @static
	 */
	const DEFAULT_FORMAT = '{HH}:{MI} {DD}/{MO}/{YYYY}';

	// jours
	public static $days = [
		'monday'    => 'Lundi',
		'tuesday'   => 'Mardi',
		'wednesday' => 'Mercredi',
		'thursday'  => 'Jeudi',
		'friday'    => 'Vendredi',
		'saturday'  => 'Samedi',
		'sunday'    => 'Dimanche'
	];

	// mois
	public static $months = [
		'january'   => 'Janvier',
		'february'  => 'Février',
		'march'     => 'Mars',
		'april'     => 'Avril',
		'may'       => 'Mai',
		'june'      => 'Juin',
		'july'      => 'Juillet',
		'august'    => 'Août',
		'september' => 'Septembre',
		'october'   => 'Octobre',
		'november'  => 'Novembre',
		'december'  => 'Décembre'
	];

	/**
	 * Formater une date suivant en lui donnant un format
	 *
	 * @param mixed $timestamp Date à formater
	 * @param string $format Format de la date
	 *
	 * @return string Date formatée
	 *
	 * @static
	 */
	public static function format($timestamp, $format = self::DEFAULT_FORMAT) {
		$day   = strtolower(date('l', $timestamp));
		$month = strtolower(date('F', $timestamp));

		// à faire : à compléter pour ajouter plus d'informations (c.f php.net/date)
		$remplacements = [
			'{D}'     => '%e',
			'{DD}'    => '%d',
			'{MO}'    => '%m',
			'{YY}'    => '%y',
			'{YYYY}'  => '%Y',
			'{H}'     => '%k',
			'{HH}'    => '%H',
			'{H12}'   => '%l',
			'{HH12}'  => '%I',
			'{MI}'    => '%M',
			'{SS}'    => '%S',
			'{ampm}'  => '%P',
			'{AMPM}'  => '%p',
			'{DD_tr}' => htmlentities(strtolower(static::$days[$day])),
			'{MO_tr}' => htmlentities(strtolower(static::$months[$month]))
		];

		$real_format = str_replace(array_keys($remplacements), array_values($remplacements), $format);

		return strftime($real_format, $timestamp);
	}

	/**
	 * @param int $date
	 *
	 * @return string
	 */
	public static function relative($date) {
		$now = new DateTime();
		$now_trunc_day = new DateTime();
		$now_trunc_day->setTimestamp($now->getTimestamp())->setTime(0, 0, 0);

		$dateT = new DateTime();
		$dateT->setTimestamp($date);
		$dateT_trunc_day = new DateTime();
		$dateT_trunc_day->setTimestamp($date)->setTime(0, 0, 0);

		$diff = $dateT->diff($now);
		$diff_trunc_day = $dateT_trunc_day->diff($now_trunc_day);

		$h = $dateT->format('H');
		$i = $dateT->format('i');

		$addTime = true;

		switch ($diff_trunc_day->d) {
			case 0: // aujourd'hui
				if ($diff->h == 0) {
					$addTime = false;

					if ($diff->i == 0)  {
						if ($diff->s == 0) {
							$str = 'a l\'instant';
						} else {
							$str = 'il y a ' . $diff->s . ' seconde' . ($diff->s > 1 ? 's' : '');
						}
					} else {
						$str = 'il y a ' . $diff->i . ' minute' . ($diff->i > 1 ? 's' : '');
					}
				} else {
					$str = 'aujourd\'hui';
				}
				break;

			case 1:  $str = 'hier';       break;
			case 2:  $str = 'avant-hier'; break;
			default: $str = static::format($dateT->getTimestamp(), '{DD_tr} {DD} {MO_tr} {YYYY}');
		}

		if ($addTime)
			$str .= ' à ' . $h . ' h ' . $i;

		return $str;
	}
	
	/**
	 * Défini le fuseau horaire qu'utilisera l'application
	 *
	 * @param string $timezone_identifier Nom du fuseau horaire (c.f http://www.php.net/manual/fr/timezones.php)
	 *
	 * @return true si le nom du fuseau horaire est invalide, sinon false
	 *
	 * @static
	 */
	public static function setTimezone($timezone_identifier = 'Europe/Paris') {
		return date_default_timezone_set($timezone_identifier);
	}
}
