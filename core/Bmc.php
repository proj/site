<?php

class Bmc extends App {
	public $user = false;

	public function __construct($db_infos) {
		parent::__construct();

		DB::init(
			$db_infos['host'],
			$db_infos['dbname'],
			$db_infos['username'],
			$db_infos['passwd'],
			$db_infos['dsn']
		);

		if (UserInfo::isLogged()) {
			$this->user = UserInfo::getById($_SESSION['id']);

			$this->user->dateLastActivity = time();
			$this->user->save();
		} else {
			$this->user = new UserInfo();
			$this->autoLogin();
		}
	}

	public function view($file) {
		$view = parent::view($file);
		$view->user = $this->user;

		return $view;
	}

	public function has($permission, $an_user = null) {
		if ($an_user == null)
			return $this->user->permissions & $permission;
		else
			return $an_user->permissions & $permission;
	}

	public function pagination($file, $route_name, $p, $slug = false) {
		$pagination = $this->view($file);
		$pagination->route_name = $route_name;
		$pagination->p          = $p;
		$pagination->slug       = $slug;

		return $pagination;
	}

	public function viewTpl() {
		$tpl = $this->view('app/views/tpl.php');

		if (UserInfo::isLogged())
			$tpl->nbNotifs  = Notif::getNbNotifs($this->user);

		$tpl->curModule = '';

		return $tpl;
	}

	public function autoLogin() {
		if ($this->user && $this->user->id != 0)
			return;

		if (empty($_COOKIE['bmc-username']) || empty($_COOKIE['bmc-sid']) || empty($_COOKIE['bmc-token']))
			return;

		$test_user = UserInfo::getByUsername($_COOKIE['bmc-username']);

		if (!$test_user)
			return;

		if ($test_user->sid != $_COOKIE['bmc-sid'] || $test_user->token != $_COOKIE['bmc-token'])
			return;

		$token = Secure::randStr(32);

		setcookie('bmc-token', $token, time() + 3600);

		$test_user->token = sha1($token);
		$test_user->save();

		$_SESSION['id'] = $test_user->id;
		$this->follow('site.home'); // à faire : rediriger sur la même page
	}
}
