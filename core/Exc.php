<?php

/**
 * Classe d'exception personnalisée pour formater les erreurs
 */
class Exc extends \Exception {
	/**
	 * Créer une exception
	 *
	 * @param string $message Message de l'exception
	 * @param integer $code Code de l'exception
	 */
	public function __construct($message = '', $code = 0) {
		$this->message = $message;
		$this->code    = $code;
	}

	/**
	 * "Lancer" l'exception
	 *
	 * @return void
	 */
	public function launch() {
		if (Application::$error_code_func) {
			call_user_func_array(Application::$error_code_func, [$this]);
			exit;
		} else {
			echo $this;
			exit;
		}
	}
	
	/**
	 * Renvoie l'exception formatée
	 *
	 * @return string
	 */
	public function __toString() {
		return '<strong>' . htmlentities('L\'erreur suivante a été générée. Contactez un administrateur si celle-ci apparaît encore') . '</strong> :<br />' .
			htmlentities($this->getMessage()) . ' (code ' . $this->getCode() . ')<br />' .
			nl2br($this->getTraceAsString());
	}
}
