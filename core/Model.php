<?php

abstract class Model {
	protected $_isNew;
	protected $_pk;
	protected $_slugField;

	public static function getList() {
		$list = [];

		$self = new static();

		$fields = array_keys($self->getFields());

		$values = DB::select(
			join(', ', $fields) .
			' from ' . $self->getTableName()
		)->all(DB::ARR);

		foreach ($values as $value) {
			$obj = static::getById($value[$self->_pk]);
			array_push($list, $obj);
		}

		return $list;
	}

	/**
	 * @param $id
	 * @return static
	 */
	public static function getById($id) {
		$self = new static();

		$self->_isNew = false;

		if ($self->hydrateById($id))
			return $self;
		else
			return null;
	}

	/**
	 * @param $slug
	 * @return static
	 */
	public static function getBySlug($slug) {
		$self = new static();

		$self->_isNew = false;

		$fields = array_keys($self->getFields());

		$values = DB::select(
			join(', ', $fields) .
			' from ' . $self->getTableName() .
			' where ' . $self->_slugField . ' = :' . $self->_slugField . ';
		', [
			$self->_slugField => $slug
		])->one(DB::ARR);

		if (!$values)
			return null;

		foreach ($values as $key => $value)
			$self->{Format::camelcasify($key)} = $value;

		return $self;
	}

	public function __construct() {
		$this->_isNew     = true;
		$this->_pk        = 'id';
		$this->_slugField = 'slug';
	}

	public function save($refresh = false) {
		if ($this->_isNew)
			return $this->insert();
		else
			return $this->update($refresh);
	}

	public function remove() {
		return DB::delete('
			' . $this->getTableName() . '
			where ' . $this->_pk . ' = :' . $this->_pk . '
		', [
			$this->_pk => $this->{$this->_pk}
		]);
	}

	protected function hydrateById($id) {
		$fields = array_keys($this->getFields());

		$values = DB::select(
			join(', ', $fields) .
			' from ' . $this->getTableName() .
			' where ' . $this->_pk . ' = :' . $this->_pk
		, [
			$this->_pk => $id
		])->one(DB::ARR);

		if (!$values)
			return false;

		foreach ($values as $key => $value)
			$this->{Format::camelcasify($key)} = $value;

		return true;
	}

	protected function insert() {
		$values = $this->getFields();

		unset($values[$this->_pk]);

		$id = DB::insert($this->getTableName(), $values);

		$this->_isNew = false;

		$this->hydrateById($id);

		return $id;
	}

	protected function update($refresh = false) {
		$keys   = [];
		$values = [];

		// à faire : ne pas modifier dans la BDD les champs non changés
		foreach ($this->getFields() as $key => $value) {
			$k = Format::underscorify($key);

			if ($k != $this->_pk) {
				array_push($keys, $k . ' = :' . $k);

				$values[$k] = $value;
			}
		}

		$values[$this->_pk] = $this->{$this->_pk};

		$query = $this->getTableName() .
			' set ' . implode(', ', $keys) .
			' where ' . $this->_pk . ' = :' . $this->_pk . ';';

		$success = DB::update($query, $values);

		if ($refresh)
			$this->hydrateById($this->{$this->_pk});

		return $success;
	}

	protected function getFields() {
		$attributes = get_class_vars(get_class($this));

		$fields = [];

		foreach ($attributes as $key => $value)
			if ($key[0] != '_')
				$fields[Format::underscorify($key)] = $this->{$key};

		return $fields;
	}

	protected function getTableName() {
		return Format::underscorify(get_called_class());
	}
}
