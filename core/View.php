<?php

class View {
	protected $vars = [];
	protected $file = '';

	public function __construct($file) {
		try {
			if (file_exists($file)) {
				$this->file = $file;
			} else {
				throw new Exc('Le fichier de vue "' . $file . '" n\'existe pas');
			}
		} catch(Exc $e) {
			exit($e);
		}
	}

	public function __set($key, $value) {
		if ($value instanceof View)
			$this->vars[$key] = (string) $value;
		else
			$this->vars[$key] = $value;
	}

	public function __toString() {
		ob_start();
		extract($this->vars);

		require $this->file;

		$_C = ob_get_contents();
		ob_end_clean();

		return $_C;
	}
}
