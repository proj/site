<?php

/**
 * Fonctions de sécurisation
 */
class Secure {
	/**
	 * Génère un grain de sel
	 *
	 * @return string Grain de sel généré
	 *
	 * @static
	 */
	public static function genSalt() {
		return substr(str_replace('+', '.', base64_encode(sha1(microtime(true), true))), 0, 22);
	}

	/**
	 * Génère un hash à partir d'un mot de passe et d'un grain de sel
	 *
	 * @param string $pass Mot de passe
	 * @param string $salt Grain de sel
	 *
	 * @return string Hash généré
	 *
	 * @static
	 */
	public static function genHash($pass, $salt = false) {
		if (!$salt || strlen($salt) != 22)
			$salt = static::genSalt();

		return crypt($pass, '$2a$12$' . $salt);
	}

	/**
	 * Test un mot de passe à partir d'un hash
	 *
	 * @param string $pass Mot de passe à tester
	 * @param string $hash Hash à tester
	 *
	 * @return bool true si le mot de passe correspond au hash, sinon false
	 *
	 * @static
	 */
	public static function testPassword($pass, $hash) {
		return $hash == crypt($pass, $hash);
	}

	/**
	 * Sécurise une chaîne HTML (alias de htmlentities)
	 *
	 * @param string $text Chaîne HTML
	 *
	 * @return string Chaîne HTML sécurisée
	 *
	 * @static
	 */
	public static function html($text) {
		return htmlentities($text);
	}

	/**
	 * Echappe une chaîne de caractères avec des antislashs (alias de addslashes)
	 *
	 * @param string $text Chaîne de caractères à échapper
	 *
	 * @return string Chaîne de caractères échapée
	 *
	 * @static
	 */
	public static function slash($text) {
		return addslashes($text);
	}

	/**
	 * Supprime les antislashs d'échappement d'une chaîne (alias de stripslashes)
	 *
	 * @param string $text Chaîne de caractères
	 *
	 * @return string Chaîne de caractère sans antislashs d'échappement
	 *
	 * @static
	 */
	public static function unslash($text) {
		return stripslashes($text);
	}

	/**
	 * Supprime les balises du texte HTML (alias de strip_tags)
	 *
	 * @param string $text Chaîne HTML
	 * @param string $tags Balises à effacer
	 *
	 * @return string La chaîne sans les balises HTML
	 *
	 * @static
	 */
	public static function removeTags($text, $allowed_tags = null) {
		return strip_tags($text, $allowed_tags);
	}

	/**
	 * @param int $length
	 *
	 * @return string
	 *
	 * @static
	 */
	public static function randStr($length = 32) {
		$chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$chars_len = strlen($chars);
		$str = '';

		for ($i = 0 ; $i < $length ; $i++)
			$str .= $chars[rand(0, $chars_len - 1)];

		return $str;
	}
}
