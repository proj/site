﻿<?php

/// Classe pour générer des tokens contre le CSRF
class Token {
	/// Génère un token
	public static function gen() {
		$_SESSION['csrf_token'] = substr(md5(time() . mt_rand()), 0, 8);
	}

	/// Retourne le token de Session, et le crée si n'existe pas
	public static function get() {
		if (!isset($_SESSION['csrf_token']))
			static::gen();

		return $_SESSION['csrf_token'];
	}

	/// Génère directement pour requetes
	public static function url($first = false) {
		return (($first) ? '' : '&') . 'token=' . static::get();
	}

	/// Génère directement pour les form
	public static function input() {
		return '<input type="hidden" id="token" name="token" value="' . static::get() . '">';
	}

	/// Compare avec $_POST/$_GET["token"]
	public static function valid($post = true) {
		$var = $post ? $_POST : $_GET;

		return $_SESSION['csrf_token'] == $var['token'];
	}
}
