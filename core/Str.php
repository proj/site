<?php

/**
 * Étend les fonctionnalités natives sur la manipulation des chaînes
 */
class Str {
	/**
	 * Met la chaîne passé en paramètre en majuscule (alias de strtoupper)
	 *
	 * @param string $string
	 *
	 * @return string
	 *
	 * @static
	 */
	public static function upper($string) {
		return strtoupper($string);
	}

	/**
	 * Met la chaîne passé en paramètre en miniscule (alias de strtolower)
	 *
	 * @param string $string
	 *
	 * @return string
	 *
	 * @static
	 */
	public static function lower($string) {
		return strtolower($string);
	}

	/**
	 * Met la première lettre de la chaîne passé en paramètre en majuscule (alias de ucfirst)
	 *
	 * @param string $string
	 *
	 * @return string
	 *
	 * @static
	 */
	public static function firstUpper($string) {
		return ucfirst($string);
	}

	/**
	 * Met la première lettre de la chaîne passé en paramètre en miniscule (alias de lcfirst)
	 *
	 * @param string $string
	 *
	 * @return string
	 *
	 * @static
	 */
	public static function firstLower($string) {
		return lcfirst($string);
	}
}
