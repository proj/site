<?php

/*

Usage:

	$bbmark = new BBMarkDown();
	$text = $bbmark->format();

Todo:

	[OK] Explode ``format`` function into sub-functions;
	[OK] Clean code of exploded functions;
	[  ] Rename exploded functions.

*/

/*

Note : tout n'est pas implémenté.

Titre niveau 1
==============

Titre niveau 2
--------------

# Titre niveau 1
## Titre niveau 2
### Titre niveau 3
#### Titre niveau 4
##### Titre niveau 5
###### Titre niveau 6

**gras**
//italique//
__souligné__
--barré--
[Mon lien](http://bitmycode.com/)

``code en ligne``

```langage (facultatif, à remplacer par le langage souhaité)
code
multiligne
```

[[bloc **sans** formatage]]

[[[
bloc
multiligne
**sans**
formatage
]]]

??spoil en ligne??

???
spoil
multiligne
???

> Citation
> sur plusieurs
> lignes

- Liste
- à
- puces

- Liste
  - à (2 espace = une nouvelle imbrication)
  - puces
    - imbriquée
- étrange

1. Liste
2. numérotée
3. ...

----- (ligne séparatrice)

%%%attention|erreur|information|question (au choix)
message
multiligne
mise en forme
%%%

*/

class BBMarkDown {
	public $text;

	public function __construct($text) {
		$this->text = $text;
	}

	protected function cleanText($text) {
		$text = str_replace(["\r\n", "\r"], "\n", $text);
		$text = trim($text, "\n");

		return $text;
	}

	protected function splitLines($text) {
		return explode("\n", $text);
	}

	protected function splitBlocks($lines) {
		$blocksOutput = [];
		$previousLine = '';
		$codeBlock = false;

		foreach ($lines as $line) {
			if ($codeBlock && !preg_match('/^```\s*$/', $line)) {
				$currentBlock = [
					'type' => 'code',
					'text' => $line
				];
			} else {
				// Header 1
				// ========
				if (preg_match('/^=+\s*$/', $line)) {
					array_pop($blocksOutput);

					$currentBlock = [
						'type' => 'h1',
						'text' => $previousLine
					];
				// Header 2
				// --------
				} else if (preg_match('/^-+\s*$/', $line)) {
					array_pop($blocksOutput);

					$currentBlock = [
						'type' => 'h2',
						'text' => $previousLine
					];
				// ### Header 1, 2, 3, 4 et 5
				} else if (preg_match('/^#{1,6}.+$/', $line)) {
					$txt = trim($line, '# ');
					$level = 0;

					while ($line[$level] == '#')
						$level++;

					$currentBlock = [
						'type' => 'h' . $level,
						'text' => $txt
					];
				// > Citation
				} else if (preg_match('/^>+.*$/', $line)) {
					$txt = preg_replace('/^>(.*)$/', '$1', $line);

					$currentBlock = [
						'type' => 'cite',
						'text' => $txt
					];
				// * Unordered list
				} else if (preg_match('/^\*(?!\*)+.+$/', $line)) {
					$txt = preg_replace('/^\*(?!\*)+(.+)$/', '$1', $line);

					$currentBlock = [
						'type' => 'ul',
						'text' => $txt
					];
				// 1. Ordered list
				} else if (preg_match('/^[0-9]+\..+$/', $line)) {
					$txt = preg_replace('/^[0-9]+\.(.+)$/', '$1', $line);

					$currentBlock = [
						'type' => 'ol',
						'text' => $txt
					];
				// ``` code
				} else if (preg_match('/^```/', $line)) {
					preg_match('/^```(.+)/', $line, $m);

					$language = '';

					if (isset($m[1]))
						$language = $m[1];

					$currentBlock = [
						'type'     => 'code',
						'text'     => $line,
						'language' => $language
					];

					$codeBlock = !$codeBlock;
				// Empty line
				} else if (preg_match('/^\s*$/', $line)) {
					$currentBlock = [
						'type' => 'stop',
						'text' => $line
					];
				// The remaining
				} else {
					$currentBlock = [
						'type' => 'p',
						'text' => $line
					];
				}
			}

			$blocksOutput[] = $currentBlock;

			$previousLine = $line;
		}

		return $blocksOutput;
	}

	protected function concatenateBlocks($blocks) {
		$blocksOutput = [];
		$previousBlock = null;
		$currentBlock = '';

		foreach ($blocks as $block) {
			if ($block['type'] == $previousBlock['type']) {
				array_pop($blocksOutput);
				$currentBlock['texts'][] = $block['text'];
			} else {
				if ($block['type'] == 'p') {
					$currentBlock = [
						'type'  => 'p',
						'texts' => [ $block['text'] ]
					];
				} else if ($block['type'] == 'cite') {
					$currentBlock = [
						'type'  => 'cite',
						'texts' => [ $block['text'] ]
					];
				} else if ($block['type'] == 'ul') {
					$currentBlock = [
						'type'  => 'ul',
						'texts' => [ $block['text'] ]
					];
				} else if ($block['type'] == 'ol') {
					$currentBlock = [
						'type'  => 'ol',
						'texts' => [ $block['text'] ]
					];
				} else if ($block['type'] == 'code') {
					$currentBlock = [
						'type'     => 'code',
						'texts'    => [ $block['text'] ],
						'language' => $block['language']
					];
				} else {
					$currentBlock = $block;
				}
			}

			$blocksOutput[] = $currentBlock;

			$previousBlock = $block;
		}

		return $blocksOutput;
	}

	function preFormatHtml($blocks) {
		$blocksOutput = [];
		$currentBlock = [];

		foreach ($blocks as $block) {
			if ($block['type'] == 'p') {
				$currentBlock['type'] = 'p';
				$currentBlock['text'] = '';

				foreach ($block['texts'] as $text)
					$currentBlock['text'] .= $text . "\n";

				$currentBlock['text'] = substr($currentBlock['text'], 0, -1);
			} else if ($block['type'] == 'code') {
				$currentBlock['type'] = 'code';
				$currentBlock['text'] = '';
				$currentBlock['language'] = $block['language'];

				foreach ($block['texts'] as $text)
					if (!preg_match('/^```(.*)/', $text))
						$currentBlock['text'] .= $text . "\n";
			} else {
				$currentBlock = $block;
			}

			$blocksOutput[] = $currentBlock;
			$currentBlock = [];
		}

		return $blocksOutput;
	}

	public function formatInlineTags($blocks) {
		foreach ($blocks as &$block) {
			if (in_array($block['type'],  [ 'p', 'cite', 'ul', 'ol' ])) {
				$replacements = [
					'~\*\*(.+)\*\*~sU'           => '<strong>$1</strong>',
					'~(?<!http:)\/\/(.+)\/\/~sU' => '<em>$1</em>',
					'~__(.+)__~sU'               => '<ins>$1</ins>',
					'~--(.+)--~sU'               => '<del>$1</del>',
					'~\[(.+)\]\((.+)\)~U'        => '<a href="$2">$1</a>',
					'~``(.+)``~U'                => '<code>$1</code>'
				];

				if (isset($block['text'])) {
					$block['text'] = htmlentities($block['text']);
					$block['text'] = preg_replace(array_keys($replacements), array_values($replacements), $block['text']);
					$block['text'] = str_replace("\n", '<br />', $block['text']);
				} else if (isset($block['texts'])) {
					foreach ($block['texts'] as &$text) {
						$text = htmlentities($text);
						$text = preg_replace(array_keys($replacements), array_values($replacements), $text);
						$text = str_replace("\n", '<br />', $text);
					}

					unset($text);

					$block['text'] = '';

					if (in_array($block['type'],  [ 'ul', 'ol' ])) {
						foreach ($block['texts'] as $text)
							$block['text'] .= '<li>' . $text . '</li>';
					} else if ($block['type'] == 'cite') {
						$block['text'] = join($block['texts'], '<br />');
					}
				}
			}
		}

		return $blocks;
	}

	protected function formatHtml($blocks) {
		$ret = '';

		foreach ($blocks as $block) {
			switch ($block['type']) {
				case 'p':
					$ret .= '<p>' . $block['text'] . '</p>';
					break;

				case 'h1':
					$ret .= '<h1>' . $block['text'] . '</h1>';
					break;

				case 'h2':
					$ret .= '<h2>' . $block['text'] . '</h2>';
					break;

				case 'h3':
					$ret .= '<h3>' . $block['text'] . '</h3>';
					break;

				case 'h4':
					$ret .= '<h4>' . $block['text'] . '</h4>';
					break;

				case 'h5':
					$ret .= '<h5>' . $block['text'] . '</h5>';
					break;

				case 'h6':
					$ret .= '<h6>' . $block['text'] . '</h6>';
					break;

				case 'cite':
					$ret .= '<blockquote><p>' . $block['text'] . '</p></blockquote>';
					break;

				case 'ul':
					$ret .= '<ul>' . $block['text'] . '</ul>';
					break;

				case 'ol':
					$ret .= '<ol>' . $block['text'] . '</ol>';
					break;

				case 'code':
					$ret .= '<pre><code' . (!empty($block['language']) ? ' class="' . $block['language'] . '"' : '') . '>';
					$ret .= htmlentities($block['text']);
					$ret .= '</code></pre>';
			}
		}

		return $ret;
	}

	public function format() {
		$text   = $this->cleanText($this->text);
		$lines  = $this->splitLines($text);
		$blocks = $this->splitBlocks($lines);
		$blocks = $this->concatenateBlocks($blocks);
		$blocks = $this->preFormatHtml($blocks);
		$blocks = $this->formatInlineTags($blocks);
		$html   = $this->formatHtml($blocks);

		return $html;
	}

	public static function staticFormat($text) {
		$formatter = new static($text);
		return $formatter->format();
	}
}
