<?php

/* Classe de formatage
---------------------- */
class Format {
	/* Autolinkage des liens
	------------------------ */
	public static function autolink($text) {
		return preg_replace('#https?://[a-zA-Z0-9?;&=./\#!|_-]+#', '<a href="$0" target="_blank">$0</a>', $text);
	}

	/* Coloration du pseudo
	----------------------- */
	public static function username($username) {
		srand(hexdec(substr(md5($username), 0, 6)));

		$red   = rand(0, 200);
		$blue  = rand(0, 225) - $red;
		$blue  = $blue < 0 ? 0 : $blue;
		$green = rand(0, 255) - $red - $blue;
		$green = $green < 0 ? 0 : $green;

		srand();

		return '<span style="color: rgb(' . $red . ',' . $green . ',' . $blue . ');">' . $username . '</span>';
	}

	/**
	 * Transforme une chaîne en lien acceptable
	 *
	 * @param string $txt Chaîne à transformer
	 *
	 * @return string Lien acceptable généré
	 */
	public static function genSlug($txt) {
		// on remplace les apostrophes et les espaces par des tirets
		$txt = str_replace(['\'', ' '], '-', $txt);

		// on retire les accents des caractères spéciaux
		$txt = static::stripAccents($txt);

		// on ne garde que les caractères alphanumérique et les tirets
		$txt = preg_replace('~[^a-zA-Z0-9-]~', '', $txt);

		// on met en minuscule
		$txt = strtolower($txt);

		// on remplace les groupe de tirets par un seul
		$txt = preg_replace('~-+~', '-', $txt);

		// on retire les tirets initiaux et finaux s'il y en a
		$txt = trim($txt, '-');

		return $txt;
	}

	/**
	 * Supprime les accents d'une chaîne
	 *
	 * @param string $txt Chaîne dont on veut supprimer les accents
	 *
	 * @return string Chaîne avec les accents effacés
	 */
	public static function stripAccents($txt) {
		return strtr(utf8_decode($txt), utf8_decode(
			'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ'),
			'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
	}

	/* Génère un slug
	----------------- */
	public static function genSlugTable($title, $func) {
		$slug = '';

		$i = 1;

		do {
			if ($i == 1)
				$slug = static::genSlug($title);
			else
				$slug = static::genSlug($title . '-' . $i);

			$i++;
		} while (call_user_func($func, $slug));

		return $slug;
	}

	/* Génère une liste d'étiquettes (tags)
	--------------------------------------- */
	public static function getListTags($txt) {
		$tags = explode(',', $txt);

		// On supprime les espaces inutiles
		// [ '  aaa  ', '   bbb   '] -> [ 'aaa', 'bbb' ]
		$tags = array_map('trim', $tags);

		// On évite les caractères exotiques
		// Hey, ça va ? -> hey-ca-va
		$tags = array_map([ 'self', 'genSlug' ], $tags);

		// Suppression des doublons
		// [ 'a', 'b', 'a' ] -> [ 'a', 'b' ]
		$tags = array_unique($tags);

		// Suppression des chaînes vides
		// [ '', 'a', 'b' ] -> [ 'a', 'b' ]
		$tags = array_filter($tags, function($el) {
			return !empty($el);
		});

		return $tags;
	}

	// AaaBbbCcc => aaa_bbb_ccc
	public static function underscorify($name) {
		$matches = preg_split('/(?=[A-Z])/', $name, -1, PREG_SPLIT_NO_EMPTY);

		$matches = array_map(function($match) {
			return strtolower($match);
		}, $matches);

		return join('_', $matches);
	}

	// aaa_bbb_ccc => AaaBbbCcc
	public static function camelcasify($name, $pascal_case = false) {
		// On met tout en minuscule
		$name = strtolower($name);

		// On remplace les tirets-bas par des espaces
		$name = str_replace('_', ' ', $name);

		// A chaque "mot" est ajouté une majuscule au premier caractère
		$name = ucwords($name);

		// Suppression des espaces
		$name = str_replace(' ', '', $name);

		// Suppression de la majuscule sur le premier caractère
		if (!$pascal_case)
			$name = lcfirst($name);

		return $name;
	}
}
