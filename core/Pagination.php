<?php

/**
 * Calculs les valeurs utiles pour une pagination
 */
class Pagination {
	/**
	 * @var int Numéro de la première page
	 */
	public $first;

	/**
	 * @var int Numéro de la dernière page
	 */
	public $last;

	/**
	 * @var int Numéro de la page précédente
	 */
	public $prev;

	/**
	 * @var int Numéro de la page suivante
	 */
	public $next;

	/**
	 * @var int Nombre total de message
	 */
	public $nb_total;

	/**
	 * @var int Nombre de messages par page
	 */
	public $nb_by_page;

	/**
	 * @var int Numéro de la page actuelle
	 */
	public $actual;

	/**
	 * @var int Nombre de pages
	 */
	public $nb_pages;

	/**
	 * Créer un nouvel objet de la classe Pagination
	 *
	 * @param integer $nb_total Nombre total de message
	 * @param integer $nb_by_page Nombre de message par page
	 * @param integer $actual Page actuellement ouverte
	 */
	public function __construct($nb_total, $nb_by_page, $actual = 1, $first_page = 1) {
		$this->first      = $first_page;
		$this->last       = ceil($nb_total / $nb_by_page);
		$this->actual     = max($this->first, min($this->last, $actual));
		$this->prev       = max($this->first, min($this->last, $this->actual - 1));
		$this->next       = max($this->first, min($this->last, $this->actual + 1));
		$this->nb_total   = $nb_total;
		$this->nb_by_page = $nb_by_page;
		$this->nb_pages   = $this->last - $this->first; /* A REVOIR */
	}
}
