<?php

/**
 * Gestion des messages d'erreurs et de succès
 */
class Flash {
	/**
	 * Création des tableaux d'erreurs et de succès
	 */
	public static function init() {
		if (!isset($_SESSION['errors']))
			$_SESSION['errors']  = [];

		if (!isset($_SESSION['success']))
			$_SESSION['success'] = [];
	}

	/**
	 * Suppression des erreurs et des succès
	 */
	public static function clean() {
		$_SESSION['errors']  = [];
		$_SESSION['success'] = [];
	}

	/**
	 * Ajouter un message d'erreur à la pile des erreurs
	 *
	 * @param string $error Message d'erreur à ajouter à la pile des erreurs
	 *
	 * @return void
	 */
	public static function pushError($error) {
		array_push($_SESSION['errors'], $error);
	}

	/**
	 * Ajouter un message de succès à la pile des succès
	 *
	 * @param string $success Message de succès à ajouter à la pile des succès
	 *
	 * @return void
	 */
	public static function pushSuccess($success) {
		array_push($_SESSION['success'], $success);
	}

	/**
	 * La pile des erreurs est-elle remplie (1 élément ou plus) ?
	 *
	 * @return bool true si la pile des erreurs contient des erreurs, false sinon
	 */
	public static function hasErrors() {
		return count($_SESSION['errors']) > 0;
	}

	/**
	 * La pile des erreurs est-elle vide ?
	 *
	 * @return bool true si la pile des erreurs est vide, false sinon
	 */
	public static function hasNoErrors() {
		return !static::hasErrors();
	}

	/**
	 * La pile des succès est-elle remplie (1 élément ou plus) ?
	 *
	 * @return bool true si la pile des succès contient des succès, false sinon
	 */
	public static function hasSuccess() {
		return count($_SESSION['success']) > 0;
	}

	/**
	 * Récupération de la liste des erreurs
	 *
	 * @return array La liste des erreurs
	 */
	public static function getErrors() {
		return $_SESSION['errors'];
	}

	/**
	 * Récupération de la liste des succès
	 *
	 * @return array La liste des succès
	 */
	public static function getSuccess() {
		return $_SESSION['success'];
	}
}
