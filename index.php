<?php

if (!function_exists('utf8_decode')) {
	function utf8_decode($str) {
		return $str;
	}
}

session_start();

define('TOPICS_BY_PAGE', 18);
define('MESSAGES_BY_PAGE', 12);

require 'core/Permissions.php';
require 'core/Format.php';
require 'core/Flash.php';
require 'core/BBMarkDown.php';
require 'core/App.php';
require 'core/Model.php';
require 'core/Bmc.php';

require 'app/models/Db.php';
require 'app/models/models.php';
require 'app/models/User.php';
require 'app/models/Category.php';
require 'app/models/Topic.php';
require 'app/models/Mail.php';
require 'app/models/Tuto.php';
require 'app/models/Notif.php';

// Création de l'application avec ses configurations
$app = new Bmc(require 'conf/db.php');

// Création des filtres
$app->addFilter('user.connected', function($app) {
	if (!UserInfo::isLogged())
		$app->follow('site.login');
});

// Initialisation des "flashs"
Flash::init();

// Inclusion des routes
foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator('app/modules')) as $filename)
	if (is_file($filename) && basename($filename) != '.htaccess' && basename($filename->getPath()) != 'views')
		require $filename;

// Exécution de la route trouvée
$app->run();

// Nettoyage des "flashs"
Flash::clean();
