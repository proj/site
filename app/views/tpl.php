<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />

		<?php if (isset($overrideTitle) && $overrideTitle): ?>
			<title><?=$title?></title>
		<?php else: ?>
			<title><?=isset($title) ? $title . ' - ' : ''?>Bit My Code</title>
		<?php endif; ?>

		<?php if (isset($description)): ?>
			<meta name="description" content="<?=$description?>" />
		<?php endif; ?>

		<link
			rel="stylesheet"
			type="text/css"
			href="/web/css/style.css" />

		<link
			rel="stylesheet"
			type="text/css"
			href="/web/js/highlight/styles/tomorrow.css" />

		<link rel="icon" type="image/png" href="/web/img/icons/favicon.png" />

		<meta name="viewport" content="initial-scale=1.0, user-scalable=yes" />

		<?php if (isset($overrideTitle) && $overrideTitle): ?>
			<meta property="og:title" content="<?=$title?>" />
		<?php else: ?>
			<meta
				property="og:title"
				content="<?=isset($title) ? $title . ' - ' : ''?>Bit My Code" />
		<?php endif; ?>

		<meta property="og:type" content="website" />
		<meta property="og:url" content="<?=$app->getUrl()?>" />

		<meta
			property="og:image"
			content="<?=$app->getHost()?>/web/img/balloon.png" />
	</head>

	<body>
		<div class="site-container">
			<div class="site-left">
				<?php if (UserInfo::isLogged()): ?>
					<div class="profile-card">
						<div class="profile-avatar">
							<a
								href="<?=$app->genLink('user.edit')?>"
								class="profile-edit"
								title="Modifier mon profil"
							>
								<img src="<?=$user->getAvatarUrl()?>" alt="" />
							</a>

							<a
								href="<?=$app->genLink('user.notifs')?>"
								class="profile-notifications"
								title="Notifications"
							>
								<?=$nbNotifs?>
							</a>
						</div>

						<div class="profile-username">
							<a
								href="<?=$app->genLink('user.edit')?>"
								class="profile-edit"
								title="Modifier mon profil"
							>
								<?=$app->user->username?><!--
							--></a>

							<a
								href="<?=$app->genLink('site.logout')?>"
								class="profile-logout"
								title="Déconnexion"
							>
								<i class="icon icon-logout"></i>
							</a>
						</div>
					</div>
				<?php else: ?>
					<div class="profile-card">
						<?php if ($curModule == 'home'): ?>
							<h1>
						<?php endif; ?>

						<a href="<?=$app->genLink('site.home')?>">
							<img
								src="/web/img/logo.png"
								alt="Bit My Code - Communauté de
									développement informatique"
								class="left-logo" />
						</a>

						<?php if ($curModule == 'home'): ?>
							</h1>
						<?php endif; ?>
					</div>
				<?php endif; ?>

				<nav>
					<ul>
						<li<?=$curModule == 'home' ? ' class="active"' : ''?>>
							<a href="<?=$app->genLink('site.home')?>">
								<i class="icon icon-home"></i>
								Accueil
							</a>
						</li>

						<li<?=$curModule == 'forum' ? ' class="active"' : ''?>>
							<a href="<?=$app->genLink('forum.home')?>">
								<i class="icon icon-forum"></i>
								Forum
							</a>
						</li>

						<?php if (UserInfo::isLogged()): ?>
							<li<?=$curModule == 'mail' ? ' class="active"' : ''?>>
								<a href="<?=$app->genLink('mail.home')?>">
									<i class="icon icon-mail"></i>
									Messagerie
								</a>
							</li>

							<li<?=$curModule == 'chat' ? ' class="active"' : ''?>>
								<a href="<?=$app->genLink('chat.home')?>">
									<i class="icon icon-chat"></i>
									Chat
								</a>
							</li>
						<?php endif; ?>

						<li<?=$curModule == 'tuto' ? ' class="active"' : ''?>>
							<a href="<?=$app->genLink('tuto.home')?>">
								<i class="icon icon-tuto"></i>
								Tutoriels
							</a>
						</li>

						<li<?=$curModule == 'annex' ? ' class="active"' : ''?>>
							<a href="<?=$app->genLink('site.annex')?>">
								<i class="icon icon-annex"></i>
								Pages annexes
							</a>
						</li>

						<?php if (UserInfo::isLogged()): ?>
							<?php if ($user->has(P::MODERATOR)): ?>
								<li<?=$curModule == 'admin' ? ' class="active"' : ''?>>
									<a href="<?=$app->genLink('admin.home')?>">
										<i class="icon icon-admin"></i>
										Administration
									</a>
								</li>
							<?php endif; ?>
						<?php else: ?>
							<li<?=$curModule == 'login' ? ' class="active"' : ''?>>
								<a href="<?=$app->genLink('site.login')?>">
									<i class="icon icon-login"></i>
									Connexion
								</a>
							</li>

							<li<?=$curModule == 'register' ? ' class="active"' : ''?>>
								<a href="<?=$app->genLink('site.register')?>">
									<i class="icon icon-register"></i>
									Inscription
								</a>
							</li>
						<?php endif; ?>
					</ul>
				</nav>
			</div>

			<div class="site-right">
				<header>
					<?php if (!empty($breadcrumb)): ?>
						<ul class="breadcrumbs">
							<?php foreach ($breadcrumb as $name => $link): ?>
								<?php if ($link): ?>
									<li>
										<a href="<?=$link?>">
											<?=$name?>
										</a>
									</li>
								<?php else: ?>
									<li>
										<?=$name?>
									</li>
								<?php endif; ?>
							<?php endforeach; ?>
						</ul>
					<?php endif; ?>
				</header>

				<section>
					<noscript>
						Votre navigateur ne supporte pas JavaScript, il se peut que
						certaines fonctionnalités ne fonctionnent pas correctement. Pensez
						à l'activer ou à utiliser un navigateur plus récent.
					</noscript>

					<?php if (Flash::hasSuccess()): ?>
						<ul class="success">
							<?php foreach (Flash::getSuccess() as $success): ?>
								<li><?=$success?></li>
							<?php endforeach; ?>
						</ul>
					<?php endif; ?>

					<?php if (Flash::hasErrors()): ?>
						<ul class="errors">
							<?php foreach (Flash::getErrors() as $error): ?>
								<li><?=$error?></li>
							<?php endforeach; ?>
						</ul>
					<?php endif; ?>

					<?php echo $content; ?>
				</section>

				<footer>
					<p>
						Bit My Code - 2013 - 2020
						| <a href="<?=$app->genLink('site.legal')?>">Mentions légales</a>
					</p>
				</footer>
			</div>
		</div>

		<script src="/web/js/main.min.js"></script>
		<script src="/web/js/highlight/highlight.pack.js"></script>
		<script>hljs.initHighlightingOnLoad();</script>
	</body>
</html>
