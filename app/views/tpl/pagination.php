<div class="pagination">
	Pages

	<?php for ($i = 1 ; $i <= $p->last ; $i++): ?>
		<?php $active = $i == $p->actual ? ' class="active"' : ''; ?>

		<?php if ($slug): ?>
			<a href="<?=$app->genLink($route_name, $slug, $i)?>"<?=$active?>><?=$i?></a>
		<?php else: ?>
			<a href="<?=$app->genLink($route_name, $i)?>"<?=$active?>><?=$i?></a>
		<?php endif; ?>
	<?php endfor; ?>
</div>
