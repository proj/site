<?php

/* Gestion des sujets
--------------------- */
class Mail {
	public static function oneTopic($slug_topic, $user = null) {
		$fields = [
			'slug' => $slug_topic
		];

		if ($user)
			$fields['id_user'] = $user->id;

		$topic = DB::select('
			ft.id, ft.title, ft.slug
			from mail_topic ft
			inner join mail_participant fp on fp.id_topic = ft.id
			where ft.slug = :slug
			' . ($user ? 'and fp.id_user = :id_user' : '') . ';
		', $fields)->one();

		if ($topic)
			$topic->id = (int) $topic->id;

		return $topic;
	}

	public static function nbTopics($user) {
		$fields = [];

		if ($user)
			$fields['id_user'] = $user->id;

		return DB::select('
			ft.id, ft.title, ft.slug,
			ui.username author_begin, fm.date_add date_begin,
			lui.username author_last, lfm.date_add date_last,
			sqrt(count(*)) nb_messages
			from mail_topic ft
			inner join mail_message fm on fm.id_topic = ft.id
			inner join user_info ui on ui.id = fm.id_author
			inner join (select * from mail_message order by date_add desc) lfm on lfm.id_topic = ft.id
			inner join user_info lui on lui.id = lfm.id_author
			inner join mail_participant fp on fp.id_topic = ft.id
			' . ($user ? 'where fp.id_user = :id_user' : '') . '
			group by ft.id
			order by date_last desc;
		', $fields)->count();
	}

	/**
	 * @param null $user
	 * @param null $p
	 * @return mixed
	 */
	public static function listTopics($user = null, $p = null) {
		if ($p != null) {
			$begin = ($p->actual - 1) * $p->nb_by_page;
			$size  = $p->nb_by_page;
		} else {
			$begin = 0;
			$size  = TOPICS_BY_PAGE;
		}

		return DB::select('
			ft.id, ft.title, ft.slug,
			ui.username author_begin, fm.date_add date_begin,
			lui.username author_last, lfm.date_add date_last,
			count(cfm.id) nb_messages,
			fr.date_last_read, fr.participate, fr.creator
			from mail_topic ft
			inner join (
				select fm.* from (
					select fm.id_topic, fm.id_author, fm.date_add
					from mail_message fm
					order by fm.date_add asc
				) fm
				group by fm.id_topic
			) fm on fm.id_topic = ft.id
			inner join (
				select fm.* from (
					select fm.id_topic, fm.id_author, fm.date_add
					from mail_message fm
					order by fm.date_add desc
				) fm
				group by fm.id_topic
			) lfm on lfm.id_topic = ft.id
			inner join mail_message cfm on cfm.id_topic = ft.id
			inner join user_info ui on ui.id = fm.id_author
			inner join user_info lui on lui.id = lfm.id_author
			inner join mail_participant fp on fp.id_topic = ft.id
			left join mail_read fr on fr.id_topic = ft.id and fr.id_user = :id_user
			where (
				select id
				from user_info lui
				where lui.id = :id_user
				and (lui.permissions & :flag_admin) = :flag_admin
			) is not null
			and fp.id_user = :id_user
			group by ft.id
			order by date_last desc
			limit ' . $begin . ', ' . $size . ';
		', [
			'id_user'    => $user->id,
			'flag_admin' => P::ADMINISTRATOR
		])->all();
	}

	public static function addTopic($user, $data) {
		DB::begin();

		$slugTopic = static::genSlug($data['title']);

		$idTopic = DB::insert('mail_topic', [
			'title' => $data['title'],
			'slug'  => $slugTopic
		]);

		$idMessage = DB::insert('mail_message', [
			'id_topic'  => $idTopic,
			'id_author' => $user->id,
			'content'   => $data['content'],
			'date_add'  => time()
		]);

		$idRead = DB::insert('mail_read', [
			'id_topic'       => $idTopic,
			'id_user'        => $user->id,
			'date_last_read' => time(),
			'participate'    => (int) true,
			'creator'        => (int) true
		]);

		$idParticipants = [];

		foreach ($data['participants'] as $participant) {
			array_push($idParticipants, DB::insert('mail_participant', [
				'id_topic' => $idTopic,
				'id_user'  => $participant->id,
				'deleted'  => (int) false
			]));
		}

		$success  = (bool) $idTopic;
		$success &= (bool) $idMessage;
		$success &= (bool) $idRead;
		$success &= count($idParticipants) == count($data['participants']);

		if ($success && DB::send())
			return $slugTopic;
		else
			return '';
	}

	public static function removeTopic($topic, $user) {
		return false;
	}

	public static function nbMessagesTopic($topic) {
		return DB::select('
			fm.*, ui.username
			from mail_message fm
			inner join user_info ui on ui.id = fm.id_author
			where id_topic = :id_topic
			order by fm.id;
		', [
			'id_topic' => $topic->id
		])->count();
	}

	/* Récupérer les messages d'un sujet
	------------------------------------ */
	public static function listMessagesTopic($topic, $p) {
		$begin = ($p->actual - 1) * $p->nb_by_page;
		$size  = $p->nb_by_page;

		// à faire : fm.* -> fm.id, ...
		return DB::select('
			fm.*, ui.id user_id, ui.username, ui.signature
			from mail_message fm
			inner join user_info ui on ui.id = fm.id_author
			where id_topic = :id_topic
			order by fm.id
			limit ' . $begin . ', ' . $size . ';
		', [
			'id_topic' => $topic->id
		])->all();
	}

	/* Ajouter une réponse à un sujet
	--------------------------------- */
	public static function addReply($topic, $user, $data) {
		DB::begin();

		$insert_reply = DB::insert('mail_message', [
			'id_topic'  => $topic->id,
			'id_author' => $user->id,
			'content'   => $data['content'],
			'date_add'  => time()
		]);

		$update_participate = DB::update('
			mail_read
			set participate = :participate
			where id_topic = :id_topic
			and id_user = :id_user
		', [
			'participate' => (int) true,
			'id_topic'    => $topic->id,
			'id_user'     => $user->id
		]);

		if ($insert_reply && $update_participate && DB::send())
			return $insert_reply;
		else
			return false;
	}

	public static function oneMessage($message_id, $user) {
		$message = DB::select('
			fm.id, fm.id_topic, fm.id_author, fm.content, fm.date_add,
			ft.slug
			from mail_message fm
			inner join mail_topic ft on ft.id = fm.id_topic
			where fm.id = :id;
		', [
			'id' => $message_id
		])->one();

		if ($user->has(P::MODERATOR) || $message->id_author == $user->id)
			return $message;
		else
			return false;
	}

	public static function participantsTopic($topic) {
		return DB::select('
			ui.username
			from mail_participant fp
			inner join user_info ui on ui.id = fp.id_user
			where fp.id_topic = :id_topic
		', [
			'id_topic' => $topic->id
		])->all();
	}
	
	/* Génère un slug pour les sujets
	--------------------------------- */
	public static function genSlug($title) {
		return Format::genSlugTable($title, __CLASS__ . '::oneTopic');
	}
}
