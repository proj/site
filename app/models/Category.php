<?php

/* Gestion des catégories
------------------------- */
class Category {
	/* Récupère une catégorie avec son titre ou son slug
	---------------------------------------------------- */
	public static function oneCategory($slug_category) {
		$category = ForumCategory::getBySlug($slug_category);

		if ($category->id)
			$category->id = (int) $category->id;

		return $category;
	}

	/* Liste des membres
	-------------------- */
	public static function listCategories() {
		return DB::select('
			id, title, description, slug, position, visible
			from forum_category
			where visible = 1
			order by position;
		')->all();
	}

	/* Création d'une nouvelle catégorie
	------------------------------------ */
	public static function addCategory($data) {
		$slug_category = self::genSlug($data['title']);

		DB::insert('forum_category', [
			'title'       => $data['title'],
			'description' => $data['description'],
			'slug'        => $slug_category,
			'position'    => 0,
			'visible'     => (int) true
		]);

		return $slug_category;
	}

	/* Mise à jour d'un catégorie
	----------------------------- */
	public static function updateCategory($category, $data) {
		$category = ForumCategory::getById($category->id);

		$category->title       = $data['title'];
		$category->description = $data['description'];
		$category->slug        = static::genSlug($data['title']);
		$category->position    = $data['position'];
		$category->visible     = $data['visible'];

		return $category->save();
	}

	/* Supprimer une catégorie
	-------------------------- */
	public static function removeCategory($category) {
		return DB::delete('
			forum_category
			where id = :id;
		', [
			'id' => $category->id
		]);
	}

	/* Génère un slug pour les catégories
	------------------------------------- */
	public static function genSlug($title) {
		return Format::genSlugTable($title, __CLASS__ . '::oneCategory');
	}
}
