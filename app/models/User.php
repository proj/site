<?php

class User {
	public static function cleanUsername($username) {
		return preg_replace('/[^a-zA-Z0-9_-]/', '', $username);
	}

	public static function listUsers() {
		$users = DB::select('
			id, username, passwd, signature, site, permissions, chat_heartbeat,
			date_register, date_last_activity, deleted
			from user_info
			where deleted = 0
			order by username;
		')->all();

		$usersArray = [];

		foreach ($users as $user) {
			$an_user = new UserInfo();

			foreach ($user as $key => $value)
				$an_user->{$key} = $value;

			array_push($usersArray, $an_user);
		}

		return $usersArray;
	}
}
