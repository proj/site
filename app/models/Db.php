<?php

class DB {
	const ARR = false;
	const OBJ = true;

	protected static $connection;

	public static $nbQueries = 0;
	private static $lastInsertSuccess = false;

	public static function init($host, $dbname, $username, $passwd, $driver = 'mysql', $driver_options = []) {
		try {
			$pdo_options = array_merge([
				PDO::ATTR_ERRMODE          => PDO::ERRMODE_EXCEPTION,
				PDO::ATTR_EMULATE_PREPARES => false
			], $driver_options);

			static::$connection = new PDO($driver . ':host=' . $host . ';dbname=' . $dbname, $username, $passwd, $pdo_options);
		} catch (Exception $e) {
			exit('<strong>Problème avec la base de données :</strong><br />' . $e->getMessage());
		}
	}

	public static function insert($table, $values = []) {
		static::$nbQueries++;

		$req = static::$connection->prepare('insert into ' . $table .
			'(' . implode(', ', array_keys($values)) . ') ' .
			'VALUES(:' . implode(', :', array_keys($values)) . ');');

		static::$lastInsertSuccess = $req->execute($values);

		return static::lastInsertId();
	}

	public static function isLastInsertSuccess() {
		return static::$lastInsertSuccess;
	}

	public static function update($query, $values = []) {
		static::$nbQueries++;

		return static::$connection->prepare('update ' . $query)->execute($values);
	}

	public static function delete($query, $values = []) {
		static::$nbQueries++;

		return static::$connection->prepare('delete from ' . $query)->execute($values);
	}

	public static function select($query, $values = []) {
		return new DBStatement($query, $values, static::$connection);
	}

	public static function query($query) {
		static::$nbQueries++;

		return static::$connection->prepare($query);
	}

	public static function lastInsertId($name = null) {
		return static::$connection->lastInsertId($name);
	}

	public static function begin() {
		return static::$connection->beginTransaction();
	}

	public static function send() {
		return static::$connection->commit();
	}

	public static function back() {
		return static::$connection->rollBack();
	}
}

class DBStatement {
	protected $query;
	protected $values;
	protected $connection;

	public function __construct($query, $values, $connection) {
		$this->query      = $query;
		$this->values     = $values;
		$this->connection = $connection;
	}

	public function count() {
		return $this->select()->rowCount();
	}

	public function one($obj = DB::OBJ) {
		return $this->select()->fetch(($obj) ? \PDO::FETCH_OBJ : \PDO::FETCH_ASSOC);
	}

	public function all($obj = DB::OBJ) {
		return $this->select()->fetchAll(($obj) ? \PDO::FETCH_CLASS : \PDO::FETCH_ASSOC);
	}

	protected function select() {
		DB::$nbQueries++;

		$return = $this->connection->prepare('SELECT ' . $this->query);
		$return->execute($this->values);

		//echo '<pre>';
		//var_dump('SELECT ' . $this->query);
		//echo '</pre>';

		return $return;
	}

	public function add($query, $values = []) {
		$this->query  .= ' ' . $query;
		$this->values  = array_merge($this->values, $values);

		return $this;
	}
}
