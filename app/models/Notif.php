<?php

class Notif {
	const SITE_MESSAGE      = 0;
	const FORUM_REPLY       = 1;
	const MAIL_NEW_OR_REPLY = 2;
	const SITE_BAN          = 3;

	public static function push($type, $id_ref, $message = '', $date = null) {
		if ($date == null)
			$date = time();

		$notif = new NotifInfo();
		$notif->numType = $type;
		$notif->idRef   = $id_ref;
		$notif->message = $message;
		$notif->dateAdd = $date;

		$notif->save();

		return $notif;
	}

	public static function getAllNotifs($user) {
		return static::getNotifs($user)->all();
	}

	public static function getNbNotifs($user) {
		return static::getNotifs($user)->count();
	}

	protected static function getNotifs($user) {
		return DB::select('
			ft.id, ft.title, ft.slug, ni.id_ref
			from notif_info ni
			inner join forum_message fm on fm.id = ni.id_ref
			inner join forum_topic ft on ft.id = fm.id_topic
			where fm.id_author <> :id_user
			order by ni.date_add desc;
		', [
			'id_user' => $user->id
		]);
	}
}
