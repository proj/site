<?php

class Tuto {
	/// Permissions
	const CAN_CHANGE_INFORMATIONS = 1; // peut changer l'avancement et les étiquettes
	const CAN_CHANGE_VISIBILITY   = 2; // peut changer la visibilité du tutoriel
	const CAN_MANAGE_CONTRIBUTORS = 4; // peut inviter, supprimer et changer les permessions des contributeurs
	const IS_CREATOR              = 8; // peut changer le titre du tutoriel et le supprimer

	/// Visibilité
	const VISIBILITY_CLOSED = 0; // accessible seulement aux contributeurs
	const VISIBILITY_SHARED = 1; // accessible par le lien partagé (introuvable sinon)
	const VISIBILITY_BETA   = 2; // bêta : classé dans une catégorie spécifique
	const VISIBILITY_PUBLIC = 3; // totalement public, visible par tous

	public static function listTutos() {
		return DB::select('
			ti.id, ti.title, ti.slug, ti.progression, ti.visibility, ti.date_add,
			ti.date_upd, ti.content, tc.permissions, ui.username
			from tutorial_info ti
			inner join tutorial_contributor tc on tc.id_tutorial = ti.id
			inner join user_info ui on ui.id = tc.id_user
			where visibility = :visibility
			group by ti.id
			order by date_upd desc;
		', [
			'visibility' => Tuto::VISIBILITY_PUBLIC
		])->all();
	}

	public static function userTutos($user) {
		return DB::select('
			ti.id, ti.title, ti.slug, ti.progression, ti.visibility, ti.date_add,
			ti.date_upd, ti.content, tc.permissions, ui.username
			from tutorial_info ti
			inner join tutorial_contributor tc on tc.id_tutorial = ti.id
			inner join user_info ui on ui.id = tc.id_user
			where ui.id = :id_user
			order by date_upd desc;
		', [
			'id_user' => $user->id
		])->all();
	}

	public static function addTuto($user, $data) {
		Db::begin();

		$slugTuto = static::genSlug($data['title']);
		$date     = time();

		$tuto = new TutorialInfo();
		$tuto->title       = $data['title'];
		$tuto->slug        = $slugTuto;
		$tuto->progression = 0;
		$tuto->visibility  = static::VISIBILITY_CLOSED;
		$tuto->dateAdd     = $date;
		$tuto->dateUpd     = $date;
		$tuto->content     = '';
		$tuto->tags        = '';
		$tuto->save();

		$author = new TutorialContributor();
		$author->idTutorial  = $tuto->id;
		$author->idUser      = $user->id;
		$author->permissions = static::creator();
		$author->save();

		$success  = (bool) $tuto->id;
		$success &= (bool) $author->idTutorial;
		$success &= (bool) $author->idUser;

		if ($success && Db::send())
			return $slugTuto;
		else
			return '';
	}

	public static function creator() {
		return
			static::CAN_CHANGE_INFORMATIONS |
			static::CAN_CHANGE_VISIBILITY   |
			static::CAN_MANAGE_CONTRIBUTORS |
			static::IS_CREATOR;
	}

	public static function remove($tuto) {
		return DB::delete('
			tutorial_info
			where id = :id;
		', [
			'id' => $tuto->id
		]);
	}

	public static function toStr($name) {
		switch ($name) {
			case 'VISIBILITY_CLOSED':
			case static::VISIBILITY_CLOSED:
				return 'Fermée';
				break;

			case 'VISIBILITY_SHARED':
			case static::VISIBILITY_SHARED:
				return 'Partagée';
				break;

			case 'VISIBILITY_BETA':
			case static::VISIBILITY_BETA:
				return 'Bêta';
				break;

			case 'VISIBILITY_PUBLIC':
			case static::VISIBILITY_PUBLIC:
				return 'Publique';
				break;

			default:
				return '-';
		}
	}

	/// Génère un slug pour les sujets
	public static function genSlug($title) {
		return Format::genSlugTable($title, function($slug) {
			return isset(TutorialInfo::getBySlug($slug)->id);
		});
	}
}
