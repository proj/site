<?php

/* Gestion des sujets
--------------------- */
class Topic {
	public static function oneTopic($slug_topic, $user = null) {
		$topic = DB::select('
			ft.*,
			fc.title category_title, fc.slug category_slug
			from forum_topic ft
			inner join forum_category fc on fc.id = ft.id_category
			where ft.slug = :slug
				and (ft.archived = 0 or (select id from user_info lui where lui.id = :id_user and (lui.permissions & :flag_admin) = :flag_admin) is not null);
		', [
			'slug'       => $slug_topic,
			'id_user'    => $user ? $user->id : -1,
			'flag_admin' => P::ADMINISTRATOR
		])->one();

		if ($topic)
			$topic->id = (int) $topic->id;

		return $topic;
	}

	public static function nbTopics($user = null) {
		return DB::select('
			ft.id
			from forum_topic ft
			where ft.archived = 0 or (select id from user_info lui where lui.id = :id_user and (lui.permissions & :flag_admin) = :flag_admin) is not null
		', [
			'id_user'    => $user ? $user->id : -1,
			'flag_admin' => P::ADMINISTRATOR
		])->count();
	}

	public static function listTopics($user = null, $p = null) {
		if ($p != null) {
			$begin = ($p->actual - 1) * $p->nb_by_page;
			$size  = $p->nb_by_page;
		} else {
			$begin = 0;
			$size  = TOPICS_BY_PAGE;
		}

		return DB::select('
			ft.*,
			ui.username author_begin, fm.date_add date_begin,
			lui.username author_last, lfm.date_add date_last,
			fc.title title_category, COUNT(cfm.id) nb_messages,
			fr.date_last_read, fr.participate, fr.creator
			from forum_topic ft
			inner join (
				select fm.* from (
					select fm.id_topic, fm.id_author, fm.date_add
					from forum_message fm
					order by fm.date_add asc
					limit 0, 18446744073709551615
				) fm
				group by fm.id_topic
			) fm on fm.id_topic = ft.id
			inner join (
				select fm.* from (
					select fm.id_topic, fm.id_author, fm.date_add
					from forum_message fm
					order by fm.date_add desc
					limit 0, 18446744073709551615
				) fm
				group by fm.id_topic
			) lfm on lfm.id_topic = ft.id
			inner join forum_message cfm on cfm.id_topic = ft.id
			inner join user_info ui on ui.id = fm.id_author
			inner join user_info lui on lui.id = lfm.id_author
			inner join forum_category fc on fc.id = ft.id_category
			left join forum_read fr on fr.id_topic = ft.id and fr.id_user = :id_user
			where ft.archived = 0 or (select id from user_info lui where lui.id = :id_user and (lui.permissions & :flag_admin) = :flag_admin) is not null
			group by ft.id
			order by pinned desc, date_last desc
			limit ' . $begin . ', ' . $size . ';
		', [
			'id_user'    => $user ? $user->id : -1,
			'flag_admin' => P::ADMINISTRATOR
		])->all();
	}

	public static function nbTopicsCategory($category, $user = null) {
		return DB::select('
			ft.id
			from forum_topic ft
			inner join forum_category fc on fc.id = ft.id_category
			where id_category = :id_category
				and (ft.archived = 0 or (select id from user_info lui where lui.id = :id_user and (lui.permissions & :flag_admin) = :flag_admin) is not null)
		', [
			'id_category' => $category->id,
			'id_user'     => $user ? $user->id : -1,
			'flag_admin'  => P::ADMINISTRATOR
		])->count();
	}

	public static function listTopicsCategory($user, $category, $p) {
		$begin = ($p->actual - 1) * $p->nb_by_page;
		$size  = $p->nb_by_page;

		return DB::select('
			ft.*,
			ui.username author_begin, fm.date_add date_begin,
			lui.username author_last, lfm.date_add date_last,
			fc.title title_category, COUNT(cfm.id) nb_messages,
			fr.date_last_read, fr.participate, fr.creator
			from forum_topic ft
			inner join (
				select fm.* from (
					select fm.id_topic, fm.id_author, fm.date_add
					from forum_message fm
					order by fm.date_add asc
					limit 0, 18446744073709551615
				) fm
				group by fm.id_topic
			) fm on fm.id_topic = ft.id
			inner join (
				select fm.* from (
					select fm.id_topic, fm.id_author, fm.date_add
					from forum_message fm
					order by fm.date_add desc
					limit 0, 18446744073709551615
				) fm
				group by fm.id_topic
			) lfm on lfm.id_topic = ft.id
			inner join forum_message cfm on cfm.id_topic = ft.id
			inner join user_info ui on ui.id = fm.id_author
			inner join user_info lui on lui.id = lfm.id_author
			inner join forum_category fc on fc.id = ft.id_category
			left join forum_read fr on fr.id_topic = ft.id and fr.id_user = :id_user
			where id_category = :id_category
				and (ft.archived = 0 or (select id from user_info lui where lui.id = :id_user and (lui.permissions & :flag_admin) = :flag_admin) is not null)
			group by ft.id
			order by pinned desc, date_last desc
			limit ' . $begin . ', ' . $size . ';
		', [
			'id_user'     => $user->id,
			'id_category' => $category->id,
			'flag_admin'  => P::ADMINISTRATOR
		])->all();
	}

	public static function listTopicsCategoryFirstUnreadMessage($user = null, $p = null, $category = null) {
		if ($p != null) {
			$begin = ($p->actual - 1) * $p->nb_by_page;
			$size  = $p->nb_by_page;
		} else {
			$begin = 0;
			$size  = TOPICS_BY_PAGE;
		}

		return DB::select('
			fm.*, (((COUNT(cfm.id) - 1) div :nb_msg_by_page) + 1) page
			from forum_topic ft
			inner join (
				select fm.*
				from (
					select fm.*
					from forum_message fm
					inner join forum_topic ft on ft.id = fm.id_topic
					left join forum_read fr on fr.id_topic = ft.id and fr.id_user = :id_user
					left join (select fm.*, max(fm.date_add) max_date_message from forum_message fm group by fm.id_topic) lfm on lfm.id_topic = ft.id
					where (:id_category < 0 or ft.id_category = :id_category)
						and (ft.archived = 0 or (select lui.id from user_info lui where lui.id = :id_user and (lui.permissions & :flag_admin) = :flag_admin) is not null)
						and (fm.date_add > fr.date_last_read or fr.id_topic is null or fm.date_add = lfm.max_date_message)
					order by fm.date_add asc
					limit 0, 18446744073709551615
				) fm
				group by fm.id_topic
			) fm on fm.id_topic = ft.id
			left join (
				select fm.id_topic, fm.date_add
				from (
					select fm.*
					from forum_message fm
					order by fm.date_add desc
					limit 0, 18446744073709551615
				) fm
				group by fm.id_topic
			) lfm on lfm.id_topic = ft.id
			inner join forum_message cfm on cfm.id_topic = ft.id and cfm.date_add <= lfm.date_add
			group by fm.id_topic
			order by ft.pinned desc, lfm.date_add desc
			limit ' . $begin . ', ' . $size . ';
		', [
			'nb_msg_by_page' => MESSAGES_BY_PAGE,
			'id_category'    => $category ? $category->id : -1,
			'id_user'        => $user ? $user->id : -1,
			'flag_admin'     => P::ADMINISTRATOR
		])->all();
	}

	/**
	 * @param User $user
	 * @param array $data
	 *
	 * @return object
	 */
	public static function pushTopic($user, $data) {
		if ($user->isNotLogged())
			return null;

		if ($user->hasNot(P::POST_FORUM)) {
			Flash::pushError('Vous n\'avez pas le droit de créer un sujet');
			return null;
		}

		if (empty($data['title']))
			Flash::pushError('Veuillez saisir un titre');

		$test_category = ForumCategory::getBySlug($data['slug_category']);

		if (!$test_category)
			Flash::pushError('La catégorie spécifiée n\'existe pas');

		try {
			DB::begin();

			$slug_topic = static::genSlug($data['title']);

			$id_topic = DB::insert('forum_topic', [
				'id_category' => $test_category->id,
				'title'       => $data['title'],
				'slug'        => $slug_topic
			]);

			$topic = ForumTopic::getById($id_topic);

			$message = static::pushMessage([
				'content' => $data['content']
			], $topic, $user);

			$read = static::pushRead([
				'participate' => true,
				'creator'     => true
			], $topic, $user);

			if (Flash::hasErrors())
				throw new Exception();
			else if (!$id_topic || !$message || !$read)
				throw new Exception('Il y a eu une erreur lors de la création du sujet');

			DB::send();

			return ForumTopic::getById($id_topic);
		} catch (Exception $e) {
			$message = $e->getMessage();

			if (!empty($message)) {
				Flash::clean();
				Flash::pushError($message);
			}

			DB::back();
			return null;
		}
	}

	public static function pushMessage($data, $topic, $user) {
		$data = array_merge([
			'content'  => '',
			'date_add' => time()
		], $data);

		if ($user->isNotLogged())
			return null;

		if ($user->hasNot(P::POST_FORUM))
			return null;

		if (empty($data['content']))
			Flash::pushError('Veuillez saisir un contenu');

		if (Flash::hasErrors())
			return null;

		$message = new ForumMessage();
		$message->idTopic  = $topic->id;
		$message->idAuthor = $user->id;
		$message->content  = $data['content'];
		$message->dateAdd  = $data['date_add'];
		$message->dateUpd  = $data['date_upd'];

		$message->save();
		if ($message->id)
			return $message;
		else
			return null;
	}

	public static function pushRead($data, $topic, $user) {
		$data = array_merge([
			'date_last_read' => time(),
			'participate'    => false,
			'creator'        => false
		], $data);

		if ($user->isNotLogged())
			return null;

		if ($user->hasNot(P::POST_FORUM))
			return null;

		$read = new ForumRead();
		$read->idTopic      = $topic->id;
		$read->idUser       = $user->id;
		$read->dateLastRead = $data['date_last_read'];
		$read->participate  = (int) $data['participate'];
		$read->creator      = (int) $data['creator'];
		$read->save();

		return true;
	}

	public static function reversePinTopic($topic) {
		$topic = ForumTopic::getBySlug($topic->slug);
		$topic->pinned = 1 - $topic->pinned;

		return$topic->save();
	}

	public static function reverseLockTopic($topic) {
		$topic = ForumTopic::getBySlug($topic->slug);
		$topic->locked = 1 - $topic->locked;

		return $topic->save();
	}

	public static function reverseArchiveTopic($topic) {
		$topic = ForumTopic::getBySlug($topic->slug);
		$topic->archived = 1 - $topic->archived;

		return $topic->save();
	}

	public static function moveTopic($topic, $newCategory) {
		$topic = ForumTopic::getBySlug($topic->slug);
		$topic->idCategory = $newCategory->id;

		return $topic->save();
	}

	public static function renameTopic($topic, $newTitle) {
		$topic = ForumTopic::getBySlug($topic->slug);
		$topic->title = $newTitle;

		return $topic->save();
	}

	public static function removeTopic($topic) {
		DB::begin();

		DB::delete('
			forum_topic
			where id = :id
		', [
			'id' => $topic->id
		]);

		DB::delete('
			forum_message
			where id_topic = :id_topic
		', [
			'id_topic' => $topic->id
		]);

		DB::delete('
			forum_read
			where id_topic = :id_topic
		', [
			'id_topic' => $topic->id
		]);

		DB::delete('
			notif_info
			where id_ref = :id_ref
			and num_type = :num_type
		', [
			'id_ref'   => $topic->id,
			'num_type' => Notif::FORUM_REPLY
		]);

		return DB::send();
	}

	public static function nbMessagesTopic($topic, $user = null) {
		return DB::select('
			fm.id
			from forum_message fm
			inner join forum_topic ft on ft.id = fm.id_topic
			where id_topic = :id_topic
				and (ft.archived = 0 or (select id from user_info where id = :id_user and (permissions & :flag_admin) = :flag_admin) is not null)
		', [
			'id_topic'   => $topic->id,
			'id_user'    => $user ? $user->id : -1,
			'flag_admin' => P::ADMINISTRATOR
		])->count();
	}

	/* Récupérer les messages d'un sujet
	------------------------------------ */
	public static function listMessagesTopic($topic, $p, $user = null) {
		$begin = ($p->actual - 1) * $p->nb_by_page;
		$size  = $p->nb_by_page;

		// à faire : fm.* -> fm.id, ...
		return DB::select('
			fm.*, ui.id user_id, ui.username, ui.signature
			from forum_message fm
			inner join user_info ui on ui.id = fm.id_author
			inner join forum_topic ft on ft.id = fm.id_topic
			where id_topic = :id_topic
				and (ft.archived = 0 or (select id from user_info where id = :id_user and (permissions & :flag_admin) = :flag_admin) is not null)
			order by fm.id
			limit ' . $begin . ', ' . $size . ';
		', [
			'id_topic'   => $topic->id,
			'id_user'    => $user ? $user->id : -1,
			'flag_admin' => P::ADMINISTRATOR
		])->all();
	}

	/* Ajouter une réponse à un sujet
	--------------------------------- */
	public static function addReply($topic, $user, $data) {
		DB::begin();

		$date = time();

		$insert_reply = DB::insert('forum_message', [
			'id_topic'  => $topic->id,
			'id_author' => $user->id,
			'content'   => $data['content'],
			'date_add'  => $date,
			'date_upd'  => $date
		]);

		$update_participate = DB::update('
			forum_read
			set participate = :participate
			where id_topic = :id_topic
			and id_user = :id_user
		', [
			'participate' => (int) true,
			'id_topic'    => $topic->id,
			'id_user'     => $user->id
		]);

		if ($insert_reply && $update_participate && DB::send())
			return $insert_reply;
		else
			return false;
	}

	public static function oneMessage($message_id, $user) {
		$message = DB::select('
			fm.*,
			ft.slug
			from forum_message fm
			inner join forum_topic ft on ft.id = fm.id_topic
			where fm.id = :id
				and (ft.archived = 0 or (select id from user_info where id = :id_user and (permissions & :flag_admin) = :flag_admin) is not null);
		', [
			'id'         => $message_id,
			'id_user'    => $user ? $user->id : -1,
			'flag_admin' => P::ADMINISTRATOR
		])->one();

		if ($user->has(P::MODERATOR) || $message->id_author == $user->id)
			return $message;
		else
			return false;
	}

	public static function updateMessage($message, $user, $data) {
		if (!$user->has(P::MODERATOR) && $message->id_author != $user->id)
			return false;

		return DB::update('
			forum_message
			set content = :content,
			date_upd = :date_upd
			where id = :id;
		', [
			'content'  => $data['content'],
			'date_upd' => time(),
			'id'       => $message->id
		]);
	}

	public static function removeMessage($message) {
		DB::begin();

		$removeMessage = DB::delete('
			forum_message
			where id = :id;
		', [
			'id' => $message->id
		]);

		$remove_notifications = DB::delete('
			notif_info
			where num_type = :num_type
			and id_ref = :id_ref;
		', [
			'num_type' => Notif::FORUM_REPLY,
			'id_ref'   => $message->id
		]);

		return $removeMessage && $remove_notifications && DB::send();
	}

	/* Génère un slug pour les sujets
	--------------------------------- */
	public static function genSlug($title) {
		return Format::genSlugTable($title, __CLASS__ . '::oneTopic');
	}

	public static function getHtmlIcon($topic) {
		$icon = 'icon-forum';

		$icon .= $topic->pinned ? '-p' : '-t';
		$icon .= $topic->locked ? '-v' : '-nv';
		$icon .= UserInfo::isLogged() && ($topic->date_last_read < $topic->date_last)
			? '-nl'
			: '-l';
		$icon .= $topic->archived ? '-a' : '';

		return $icon;
	}
	
	public static function getHtmlStatus($topic) {
		$text = '';

		$text .= $topic->pinned ? ' - &Eacute;pinglé' : '';
		$text .= $topic->locked ? ' - Verrouillé' : '';
		$text .= UserInfo::isLogged() && ($topic->date_last_read < $topic->date_last)
			? ' - Derniers messages non lus'
			: ' - Dernier message lu';
		$text .= $topic->archived ? ' - Archivé' : '';

		return substr($text, 3);
	}
}
