<?php

class UserInfo extends Model {
	public $id               = 0;
	public $username         = '';
	public $passwd           = '';
	public $signature        = '';
	public $site             = '';
	public $permissions      = 0;
	public $chatHeartbeat    = 0;
	public $dateRegister     = 0;
	public $dateLastActivity = 0;
	public $notes            = '';
	public $deleted          = false;
	public $sid              = '';
	public $token            = '';

	protected $_banned;

	/** @return static */
	public static function getByUsername($username) {
		$self = new static();

		$self->_isNew = false;

		$fields = array_keys($self->getFields());

		$values = DB::select(
			join(', ', $fields) .
			' from ' . $self->getTableName() .
			' where username = :username;
		', [
			'username' => $username
		])->one(DB::ARR);

		if (!$values)
			return null;

		foreach ($values as $key => $value)
			$self->{Format::camelcasify($key)} = $value;

		return $self;
	}

	/** @return static */
	public static function getByAutoLogin($sid, $token, $username_hash) {
		$self = new static();

		$self->_isNew = false;

		$fields = array_keys($self->getFields());

		$values = DB::select(
			join(', ', $fields) .
			' from ' . $self->getTableName() .
			' where sid = :sid
			  and token = :token
			  and sha1(username) = :username_hash
		', [
			'sid'           => $sid,
			'token'         => $token,
			'username_hash' => $username_hash
		])->one(DB::ARR);

		if (!$values)
			return null;

		foreach ($values as $key => $value)
			$self->{Format::camelcasify($key)} = $value;

		return $self;
	}

	public function has($permission) {
		return $this->permissions & $permission;
	}

	public function hasNot($permission) {
		return !$this->has($permission);
	}

	public function isBanned() {
		return $this->_banned;
	}

	public function getAvatarUrl() {
		if (file_exists(realpath(__DIR__ . '/../../web/img/mava/cache/' . $this->username . '.png')))
			return '/web/img/mava/cache/' . $this->username . '.png';
		else
			return '/web/img/mava/mava.php?username=' . $this->username;
	}

	public static function getAvatarUrlByUsername($username) {
		if (file_exists(realpath(__DIR__ . '/../../web/img/mava/cache/' . $username . '.png')))
			return '/web/img/mava/cache/' . $username . '.png';
		else
			return '/web/img/mava/mava.php?username=' . $username;
	}

	public static function isLogged() {
		return isset($_SESSION['id']);
	}

	public static function isNotLogged() {
		return !static::isLogged();
	}
}

class NotifInfo extends Model {
	public $id      = 0;
	public $idRef   = 0;
	public $numType = 0;
	public $dateAdd = 0;
	public $message = '';
}

class ForumTopic extends Model {
	public $id         = 0;
	public $idCategory = 0;
	public $title      = '';
	public $slug       = '';
	public $pinned     = false;
	public $locked     = false;
	public $archived   = false;
}

class ForumMessage extends Model {
	public $id      = 0;
	public $idTopic = 0;
	public $idAuthor= 0;
	public $content = '';
	public $dateAdd = 0;
	public $dateUpd = 0;
}

class ForumCategory extends Model {
	public $id          = 0;
	public $title       = '';
	public $slug        = '';
	public $description = '';
	public $position    = 0;
	public $visible     = true;
}

class ChatMessage extends Model {
	public $id       = 0;
	public $idAuthor = 0;
	public $message  = '';
	public $dateAdd  = 0;
}

class ChatInfo extends Model {
	public $id           = 0;
	public $uid          = 0;
	public $writing      = false;
	public $hearbeath    = 0;
	public $soundEnabled = true;
	public $ban          = false;
}

class TutorialInfo extends Model {
	public $id          = 0;
	public $title       = '';
	public $slug        = '';
	public $progression = 0;
	public $visibility  = 0;
	public $dateAdd     = 0;
	public $dateUpd     = 0;
	public $content     = '';
	public $tags        = '';
}

class TutorialContributor extends Model {
	public $id          = 0;
	public $idTutorial  = 0;
	public $idUser      = 0;
	public $permissions = 0;
}

class ForumRead extends Model {
	public $id           = 0;
	public $idTopic      = 0;
	public $idUser       = 0;
	public $dateLastRead = 0;
	public $participate  = false;
	public $creator      = false;
}

class UserFlux extends Model {
	public $id         = 0;
	public $idUser     = 0;
	public $content    = '';
	public $dateAdd    = 0;
	public $visibility = 0;
}

class UserFluxComment extends Model {
	public $id        = 0;
	public $idUser    = 0;
	public $idMessage = 0;
	public $content   = '';
	public $dateAdd   = 0;
}

class UserBanishment extends Model {
	public $id                = 0;
	public $idUser            = 0;
	public $idBanisher        = 0;
	public $dateBegin         = 0;
	public $dateEnd           = 0;
	public $reason            = '';
	public $permissions       = 0;
	public $permissionsBefore = 0;
}

class MailTopic extends Model {
	public $id         = 0;
	public $idCategory = 0;
	public $title      = '';
	public $slug       = '';
	public $pinned     = false;
	public $locked     = false;
	public $archived   = false;
	public $private    = false;
}

class MailParticipant extends Model {
	public $id      = 0;
	public $idTopic = 0;
	public $idUser  = 0;
	public $deleted = false;
}

class MailRead extends Model {
	public $id           = 0;
	public $idTopic      = 0;
	public $idUser       = 0;
	public $dateLastRead = 0;
	public $participate  = false;
	public $creator      = false;
}

class MailMessage extends Model {
	public $id       = 0;
	public $idTopic  = 0;
	public $idAuthor = 0;
	public $content  = '';
	public $dateAdd  = 0;
}

class UserContact extends Model {
	public $id      = 0;
	public $idUser1 = 0;
	public $idUser2 = 0;
}

class ArticleInfo extends Model {
	const STATUS_PRIVATE     = 0; // accessible à l'auteur et aux administrateurs
	const STATUS_UNPUBLISHED = 1; // accessible par le lien
	const STATUS_PULISHED    = 2; // accessible à tous

	public $id          = 0;
	public $idAuthor    = 0;
	public $title       = '';
	public $slug        = '';
	public $content     = '';
	public $dateArticle = 0;
	public $tags        = '';
	public $status      = 2;
}

class ArticleCategory extends Model {
	public $id          = 0;
	public $title       = '';
	public $slug        = '';
	public $description = '';
}

class ArticleInfoCategory extends Model {
	public $idArticle  = 0;
	public $idCategory = 0;
}
