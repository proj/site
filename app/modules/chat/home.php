<?php

$app->get('chat.home', 'chat', function($app) {
	$app->filter('user.connected');

	$content = $app->view(__DIR__ . '/views/home.php');

	$page = $app->viewTpl();
	$page->title      = 'Chat';
	$page->content    = $content;
	$page->curModule  = 'chat';
	$page->breadcrumb = [
		'Chat' => false
	];

	echo $page;
});
