<?php

$app->post('tuto.remove', 'tutorial/remove/{alphanum}', function($app, $slug) {
	$app->filter('user.connected');

	$tuto = TutorialInfo::getBySlug($slug);

	if (!$tuto->id)
		$app->follow('tuto.my');

	$contibutors = DB::select('
		id_user id, permissions
		from tutorial_contributor
		where id_tutorial = :id_tutorial
	', [
		'id_tutorial' => $tuto->id
	])->all(DB::ARR);

	$key = array_search($app->user->id, array_column($contibutors, 'id'));

	if ($key === false)
		$app->follow('tuto.my');

	if (!($contibutors[$key]['permissions'] & Tuto::IS_CREATOR))
		$app->follow('tuto.my');

	if (Tuto::remove($tuto))
		Flash::pushSuccess('Tutoriel supprimé avec succès');
	else
		Flash::pushError('Il y a eu un problème durant la suppression du tutoriel');

	$app->follow('tuto.my');
});
