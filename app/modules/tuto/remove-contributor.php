<?php

$app->post('tuto.remove-contributor', 'tutorial/remove-contributor/{alphanum}', function($app, $slug) {
	$app->filter('user.connected');

	$tuto = TutorialInfo::getBySlug($slug);

	if (!$tuto->id)
		$app->follow('tuto.my');

	// Seul un contributeur ayant la permission CAN_MANAGE_CONTRIBUTORS peut
	// supprimer un contributeur
	$userPermissions = DB::select('
		permissions
		from tutorial_contributor
		where id_tutorial = :id_tutorial
		and id_user = :id_user
	', [
		'id_tutorial' => $tuto->id,
		'id_user' => $app->user->id
	])->one();

	if (!($userPermissions->permissions & ContributorPermission::CAN_MANAGE_CONTRIBUTORS))
		$app->follow('tuto.edit', $tuto->slug);

	$username = Input::get('username');

	$user = UserInfo::getByUsername($username);

	if (!$user) {
		Flash::pushError('Utilisateur inexistant : ' . htmlentities($username));

		$app->follow('tuto.edit', $tuto->slug);
	}

	$deleted = DB::delete('
		tutorial_contributor
		where id_tutorial = :id_tutorial
		and id_user = :id_user
	', [
		'id_tutorial' => $tuto->id,
		'id_user' => $user->id
	]);

	if ($deleted) {
		Flash::pushSuccess('Le contributeur ' . $user->username . ' a été supprimé
			du tutoriel « ' . htmlentities($tuto->title)  . ' »');

		$app->follow('tuto.edit', $tuto->slug);
	} else {
		Flash::pushError('Il y a eu un problème lors de la suppression du
			contributeur ' . $user->username . ' au tutoriel
			« ' . htmlentities($tuto->title) . ' »');

		$app->follow('tuto.edit', $tuto->slug);
	}
});
