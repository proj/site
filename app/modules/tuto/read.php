<?php

$app->get('tuto.read', 'tutorial/read/{alphanum}', function($app, $slug) {
	$tuto = TutorialInfo::getBySlug($slug);

	if (!$tuto)
		$app->follow('tuto.home');

	$contibutors = DB::select('
		id_user id
		from tutorial_contributor
		where id_tutorial = :id_tutorial
	', [
		'id_tutorial' => $tuto->id
	])->all();

	$author = $contibutors[0];

	if ($tuto->visibility == Tuto::VISIBILITY_CLOSED && $author->id != $app->user->id)
		$app->follow('tuto.home');

	$content = $app->view(__DIR__ . '/views/read.php');
	$content->tuto    = $tuto;
	$content->content = BBMarkDown::staticFormat($tuto->content);

	$page = $app->viewTpl();
	$page->title      = htmlspecialchars($tuto->title) . ' - Tutoriel';
	$page->content    = $content;
	$page->curModule  = 'tuto';
	$page->breadcrumb = [
		'Tutoriels' => $app->genLink('tuto.home'),
		htmlspecialchars($tuto->title) => false
	];

	echo $page;
});
