<?php

$app->post('tuto.edit-permissions', 'tutorial/edit-permissions/{alphanum}', function($app, $slug) {
	$app->filter('user.connected');

	$tuto = TutorialInfo::getBySlug($slug);

	if (!$tuto)
		$app->follow('tuto.home');

	// Seul un contributeur ayant la permission CAN_MANAGE_CONTRIBUTORS peut
	// modifier les permissions
	$userPermissions = DB::select('
		permissions
		from tutorial_contributor
		where id_tutorial = :id_tutorial
		and id_user = :id_user
	', [
		'id_tutorial' => $tuto->id,
		'id_user' => $app->user->id
	])->one();

	if (!($userPermissions->permissions & ContributorPermission::CAN_MANAGE_CONTRIBUTORS))
		$app->follow('tuto.edit', $tuto->slug);

	$permissions = Input::get('permissions');

	foreach ($permissions as $userId => $userPermissions) {
		$userId = (int) $userId;

		$contributor = DB::select('
			permissions
			from tutorial_contributor
			where id_tutorial = :id_tutorial
			and id_user = :id_user
		', [
			'id_tutorial' => $tuto->id,
			'id_user' => $userId
		])->one();

		// Le créateur ne peut pas modifier ses permissions
		if ($contributor->permissions & ContributorPermission::IS_CREATOR)
			$app->follow('tuto.edit', $tuto->slug);

		$permissionsComputed = 0;

		foreach ($userPermissions as $permission)
			$permissionsComputed += (int) $permission;

		// La permission IS_CREATOR n'est pas assignable
		$permissionsComputed &= ~ContributorPermission::IS_CREATOR;

		$a = DB::update('
			tutorial_contributor
			set permissions = :permissions
			where id_tutorial = :id_tutorial
			and id_user = :id_user
		', [
			'permissions' => $permissionsComputed,
			'id_tutorial' => $tuto->id,
			'id_user' => $userId
		]);
	}

	Flash::pushSuccess('Permisions mises à jour');
	$app->follow('tuto.edit', $tuto->slug);
});
