<?php

$app->get('tuto.home', 'tutorials', function($app) {
	$content = $app->view(__DIR__ . '/views/list.php');
	$content->tutos = Tuto::listTutos();

	$page = $app->viewTpl();
	$page->title      = 'Liste des tutoriels';
	$page->content    = $content;
	$page->curModule  = 'tuto';
	$page->breadcrumb = [
		'Tutoriels' => false
	];

	echo $page;
});
