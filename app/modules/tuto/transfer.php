<?php

$app->post('tuto.transfer', 'tutorial/transfer/{alphanum}', function($app, $slug) {
	$app->filter('user.connected');

	$tuto = TutorialInfo::getBySlug($slug);

	if (!$tuto->id)
		$app->follow('tuto.my');

	// Seul le créateur peut transférer son tutoriel
	$userPermissions = DB::select('
		permissions
		from tutorial_contributor
		where id_tutorial = :id_tutorial
		and id_user = :id_user
	', [
		'id_tutorial' => $tuto->id,
		'id_user' => $app->user->id
	])->one();

	if (!($userPermissions->permissions & ContributorPermission::IS_CREATOR))
		$app->follow('tuto.edit', $tuto->slug);

	$username = Input::get('username');

	$user = UserInfo::getByUsername($username);

	if (!$user) {
		Flash::pushError('Utilisateur inexistant : ' . htmlentities($username));

		$app->follow('tuto.edit', $tuto->slug);
	}

	$updated = DB::update('
		tutorial_contributor
		set id_user = :id_user
		where id = :id
	', [
		'id_user' => $user->id,
		'id'      => $tuto->id
	]);

	if ($updated) {
		Flash::pushSuccess('Le tutoriel « ' . htmlentities($tuto->title)  . ' » a
			été transféré à l\'utilisateur ' . $user->username);

		$app->follow('tuto.my');
	} else {
		Flash::pushError('Il y a eu un problème lors du transfert du tutoriel « ' .
			htmlentities($tuto->title) . ' » à ' . $user->username);

		$app->follow('tuto.edit', $tuto->slug);
	}
});
