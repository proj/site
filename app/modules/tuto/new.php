<?php

$app->post('tuto.new', 'tutorial/new', function($app) {
	$app->filter('user.connected');

	if (!$app->has(P::POST_TUTORIAL))
		Flash::pushError('Vous n\'avez pas les droits pour créer des tutoriels');

	$title = trim(Input::get('title'));

	if (empty($title))
		Flash::pushError('Veuillez saisir un titre');

	if (Flash::hasNoErrors()) {
		$insertTuto = Tuto::addTuto($app->user, [
			'title' => $title
		]);

		if (!$insertTuto)
			Flash::pushError('Il y a eu une erreur lors de la création du tutoriel');
	}

	if (Flash::hasNoErrors()) {
		Flash::pushSuccess('Tutoriel créé');

		$tuto = TutorialInfo::getBySlug($insertTuto);
	}

	$app->follow('tuto.edit', $tuto->slug);
});
