<?php

$app->get('tuto.search', 'tutorial/search', function($app) {
	$content = $app->view(__DIR__ . '/views/search.php');





	// PARTIE EN DEVELOPPEMENT
	$search = [ 'd', 'c' ];

	$q = '';
	$f = '';

	$first = true;
	foreach ($search as $s) {
		$q .= ($first ? ' where' : ' or') . ' concat(\',\', tags, \',\') like \'%' . $s . '%\'';
		$f .= ($first ? '' : ' + ') . 'if(concat(\',\', tags, \',\') like \'%,' . $s . ',%\', 1, 0)';
		$first = false;
	}

	//var_dump($f);

	$a = DB::query('
		select (' . $f . ') as nb
		from tutorial_info
		' . $q . '
		group by title
		order by nb desc;
	');

	var_dump($a);

	$a->execute();

	var_dump($a->fetchAll(PDO::FETCH_CLASS));
	// FIN PARTIE EN DEVELOPPEMENT





	$page = $app->viewTpl();
	$page->title      = 'Rechercher un tutoriel';
	$page->content    = $content;
	$page->curModule  = 'tuto';
	$page->breadcrumb = [
		'Rechercher un tutoriel' => false
	];

	echo $page;
})

->post('tuto.search.post', 'tuto/search', function($app) {
	$app->exec('tuto.search');
});
