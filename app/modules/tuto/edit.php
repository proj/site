<?php

$app->get('tuto.edit', 'tutorial/edit/{alphanum}', function($app, $slug) {
	$app->filter('user.connected');

	$tuto = TutorialInfo::getBySlug($slug);

	if (!$tuto)
		$app->follow('tuto.home');

	$visibilities = [
		Tuto::VISIBILITY_CLOSED,
		Tuto::VISIBILITY_SHARED,
		Tuto::VISIBILITY_BETA,
		Tuto::VISIBILITY_PUBLIC
	];

	$reflection = new ReflectionClass('ContributorPermission');
	$permissions  = $reflection->getConstants();

	// La permission IS_CREATOR n'est pas assignable
	unset($permissions['IS_CREATOR']);

	$contributors = DB::select('
		ui.id, ui.username, tc.permissions
		from tutorial_contributor tc
		inner join user_info ui
			on ui.id = tc.id_user
		where tc.id_tutorial = :id_tutorial
		and not (tc.permissions & :is_creator)
	', [
		'id_tutorial'    => $tuto->id,
		'is_creator' => ContributorPermission::IS_CREATOR
	])->all();

	// Seul le créateur peut transférer son tutoriel
	$userContributor = DB::select('
		permissions
		from tutorial_contributor
		where id_tutorial = :id_tutorial
		and id_user = :id_user
	', [
		'id_tutorial' => $tuto->id,
		'id_user' => $app->user->id
	])->one();

	$content = $app->view(__DIR__ . '/views/edit.php');
	$content->tuto            = $tuto;
	$content->visibilities    = $visibilities;
	$content->permissions     = $permissions;
	$content->contributors    = $contributors;
	$content->userContributor = $userContributor;

	$page = $app->viewTpl();
	$page->title      = '&Eacute;diter le tutoriel ' . htmlspecialchars($tuto->title);
	$page->content    = $content;
	$page->curModule  = 'tuto';
	$page->breadcrumb = [
		'Mes tutoriels' => $app->genLink('tuto.my'),
		'Modification de ' . htmlspecialchars($tuto->title) => false
	];

	echo $page;
})

->post('tuto.edit.confirm', 'tuto/edit/{alphanum}', function($app, $slug) {
	$app->filter('user.connected');

	if (!$app->has(P::POST_TUTORIAL))
		Flash::pushError('Vous n\'avez pas les droits pour modifier vos tutoriels');

	$tuto = TutorialInfo::getBySlug($slug);

	if (!$tuto->id)
		$app->follow('tuto.home');

	$title       = trim(Input::get('title'));
	$progression = Input::get('progression', 0);
	$visibility  = Input::get('visibility');
	$tags        = trim(Input::get('tags'));
	$content     = trim(Input::get('content'));

	$progression = max(0, min((int) $progression, 100));

	if (empty($title))
		Flash::pushError('Veuillez saisir un titre');

	if (empty($content))
		Flash::pushError('Veuillez saisir un contenu');

	if (Flash::hasNoErrors()) {
		$tuto->title       = $title;
		$tuto->progression = $progression;
		$tuto->visibility  = $visibility;
		$tuto->tags        = join(', ', Format::getListTags($tags));
		$tuto->content     = $content;

		if (!$tuto->save())
			Flash::pushError('Il y a eu une erreur lors de la modification du tutoriel');
	}

	if (Flash::hasNoErrors())
		Flash::pushSuccess('Tutoriel modifié');

	$app->follow('tuto.edit', $tuto->slug);
});
