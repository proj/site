<?php

$app->post('tuto.add-contributor', 'tutorial/add-contributor/{alphanum}', function($app, $slug) {
	$app->filter('user.connected');

	$tuto = TutorialInfo::getBySlug($slug);

	if (!$tuto)
		$app->follow('tuto.my');

	// Seul un contributeur ayant la permission CAN_MANAGE_CONTRIBUTORS peut
	// ajouter un contributeur
	$userPermissions = DB::select('
		permissions
		from tutorial_contributor
		where id_tutorial = :id_tutorial
		and id_user = :id_user
	', [
		'id_tutorial' => $tuto->id,
		'id_user' => $app->user->id
	])->one();

	// à faire : un système pour tester des permissions : has, hasNot, ...
	if (!($userPermissions->permissions & ContributorPermission::CAN_MANAGE_CONTRIBUTORS))
		$app->follow('tuto.edit', $tuto->slug);

	$username = Input::get('username');

	$user = UserInfo::getByUsername($username);

	if (!$user) {
		Flash::pushError('Utilisateur inexistant : ' . htmlentities($username));

		$app->follow('tuto.edit', $tuto->slug);
	}

	$inserted = DB::insert('tutorial_contributor', [
		'id_tutorial' => $tuto->id,
		'id_user' => $user->id,
		'permissions' => ContributorPermission::CAN_CHANGE_INFORMATIONS
	]);

	if (DB::isLastInsertSuccess()) {
		Flash::pushSuccess('L\'utilisateur ' . $user->username . ' a été ajouté au
		 	tutoriel « ' . htmlentities($tuto->title)  . ' »');

		$app->follow('tuto.edit', $tuto->slug);
	} else {
		Flash::pushError('Il y a eu un problème lors de l\'ajout du contributeur
			' . $user->username . ' au tutoriel « ' . htmlentities($tuto->title) . '
			»');

		$app->follow('tuto.edit', $tuto->slug);
	}
});
