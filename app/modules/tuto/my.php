<?php

$app->get('tuto.my', 'tutorials/my', function($app) {
	$app->filter('user.connected');

	$content = $app->view(__DIR__ . '/views/my.php');
	$content->tutos = Tuto::userTutos($app->user);

	$page = $app->viewTpl();
	$page->title      = 'Mes tutoriels';
	$page->content    = $content;
	$page->curModule  = 'tuto';
	$page->breadcrumb = [
		'Mes tutoriels' => false
	];

	echo $page;
});
