<div class="tabs">
	<h1>Tutoriels</h1>

	<a href="<?=$app->genLink('tuto.home')?>">Publi&eacute;s</a>
	<a href="<?=$app->genLink('tuto.my')?>" class="active">Mes tutoriels</a>
	<a href="<?=$app->genLink('tuto.search')?>">Rechercher</a>
</div>

<a href="#" class="toggle-link" data-toggle="newtuto">Nouveau tutoriel</a>

<div id="newtuto" class="toggle">
	<form method="post" action="<?=$app->genLink('tuto.new')?>">
		<input type="text" name="title" placeholder="Titre" />

		<input
			type="submit"
			name="new-tuto"
			value="Cr&eacute;er"
			class="btn btn-primary btn-colored" />
	</form>
</div>

<?php foreach($tutos as $tuto): ?>
	<div>
		<div class="topic-title">
			<a href="<?=$app->genLink('tuto.read', $tuto->slug)?>">
				<?=htmlspecialchars($tuto->title)?>
			</a>
		</div>

		<div class="topic-infos">
			Le <?=Date::format($tuto->date_add)?>
			; Derni&egrave;re mise &agrave; jour le <?=Date::format($tuto->date_upd)?>
			; Visibilité : <?=Tuto::toStr($tuto->visibility)?>
			; Avancement : <?=$tuto->progression?>%

			<div class="btn-group">
				<a
					class="btn btn-colored btn-secondary"
					href="<?=$app->genLink('tuto.edit', $tuto->slug)?>"
				>
					Modifier
				</a>

				<?php if ($user->has(P::MODERATOR)): ?>
					<button
						class="btn btn-colored btn-secondary dropdown-toggle"
						data-toggle="dropdown"
					>
						<span class="caret"></span>
					</button>

					<ul class="dropdown-menu">
						<li>
							<form
								method="post"
								action="<?=$app->genLink('tuto.remove', $tuto->slug)?>"
							>
								<input type="submit" value="Supprimer" class="link" />
							</form>
						</li>
					</ul>
				<?php endif; ?>
			</div>
		</div>
	</div>
<?php endforeach; ?>
