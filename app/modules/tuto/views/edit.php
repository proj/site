<a href="<?=$app->genLink('tuto.read', $tuto->slug)?>">Voir le rendu</a>

<form
	method="post"
	action="<?=$app->genLink('tuto.edit.confirm', $tuto->slug)?>"
	oninput="out_progression.value = in_progression.valueAsNumber;"
>
	<label>Titre</label>
		<input
			type="text"
			name="title"
			value="<?=htmlspecialchars($tuto->title)?>" />

	<label>Avancement</label>
		<input
			id="in_progression"
			type="range"
			min="0"
			max="100"
			step="1"
			name="progression"
			value="<?=$tuto->progression?>" />

		<output id="out_progression"><?=$tuto->progression?></output>%

	<label>Visibilité</label>
		<select name="visibility">
			<?php foreach ($visibilities as $visibility): ?>
				<?php if ($tuto->visibility == $visibility): ?>
					<option
						value="<?=$visibility?>"
						selected
					>
						<?=Tuto::toStr($visibility)?>
					</option>
				<?php else: ?>
					<option value="<?=$visibility?>">
						<?=Tuto::toStr($visibility)?>
					</option>
				<?php endif; ?>
			<?php endforeach; ?>
		</select>

	<label>
		&Eacute;tiquettes (<em>tags</em>)
		<small>(séparées par des virgules)</small>
	</label>
		<input type="text" name="tags" value="<?=htmlspecialchars($tuto->tags)?>" />

	<label>Contenu</label>
		<div class="write-message">
			<div class="message">
				<textarea
					name="content"
					placeholder="Contenu"
					style="height: 400px"><?=htmlspecialchars($tuto->content)?></textarea>
			</div>
		</div>

	<input
		type="submit"
		name="edit-tuto"
		value="Modifier"
		class="btn btn-colored btn-primary" />
</form>

<?php if ($userContributor->permissions & ContributorPermission::IS_CREATOR): ?>
	<form method="post" action="<?=$app->genLink('tuto.transfer', $tuto->slug)?>">
		<label>Transférer à</label>
			<input type="text" name="username" />

		<input
			type="submit"
			value="Transférer"
			class="btn btn-colored btn-primary" />
	</form>
<?php endif; ?>

<?php if ($userContributor->permissions & ContributorPermission::CAN_MANAGE_CONTRIBUTORS): ?>
	<form method="post" action="<?=$app->genLink('tuto.add-contributor', $tuto->slug)?>">
		<label>Ajouter un contributeur</label>
			<input type="text" name="username" />

		<input
			type="submit"
			value="Ajouter"
			class="btn btn-colored btn-primary" />
	</form>
<?php endif; ?>

<?php if ($userContributor->permissions & ContributorPermission::CAN_MANAGE_CONTRIBUTORS): ?>
	<form method="post" action="<?=$app->genLink('tuto.edit-permissions', $tuto->slug)?>">
		<table>
			<tr>
				<th>Utilisateur</th>

				<?php foreach ($permissions as $permission): ?>
					<th><?=ContributorPermission::toStr($permission)?></th>
				<?php endforeach; ?>
			</tr>

			<?php foreach ($contributors as $contributor): ?>
				<tr>
					<td>
						<?=$contributor->username?>
					</td>

					<?php foreach ($permissions as $permission): ?>
						<td>
							<?php if ($contributor->permissions & $permission): ?>
								<input
									type="checkbox"
									name="permissions[<?=$contributor->id?>][]"
									value="<?=$permission?>"
									checked />
							<?php else: ?>
								<input
									type="checkbox"
									name="permissions[<?=$contributor->id?>][]"
									value="<?=$permission?>" />
							<?php endif; ?>
						</td>
					<?php endforeach; ?>
				</tr>
			<?php endforeach; ?>
		</table>

		<input
			type="submit"
			value="Modifier les permissions"
			class="btn btn-colored btn-primary" />
	</form>

	<table style="max-width: 500px;">
		<tr>
			<th>Utilisateur</th>
			<th>Supprimer</th>
		</tr>

		<?php foreach ($contributors as $contributor): ?>
			<tr>
				<td>
					<?=$contributor->username?>
				</td>

				<td>
					<form
						method="post"
						action="<?=$app->genLink('tuto.remove-contributor', $tuto->slug)?>"
					>
						<input
							type="hidden"
							name="username"
							value="<?=$contributor->username?>" />

						<input
							type="submit"
							value="Supprimer"
							class="btn btn-colored btn-red" />
					</form>
				</td>
			</tr>
		<?php endforeach; ?>
	</table>
<?php endif; ?>
