<div class="tabs">
	<h1>Tutoriels</h1>

	<a href="<?=$app->genLink('tuto.home')?>" class="active">Publi&eacute;s</a>
	<?php if (UserInfo::isLogged()): ?>
		<a href="<?=$app->genLink('tuto.my')?>">Mes tutoriels</a>
	<?php endif; ?>
	<a href="<?=$app->genLink('tuto.search')?>">Rechercher</a>
</div>

<?php foreach ($tutos as $tuto): ?>
	<div>
		<div class="topic-title">
			<a href="<?=$app->genLink('tuto.read', $tuto->slug)?>">
				<?=htmlspecialchars($tuto->title)?>
			</a>
		</div>

		<div class="topic-infos">
			Par <?=$tuto->username?>
			le <?=Date::format($tuto->date_add)?>
			; Derni&egrave;re mise &agrave; jour le <?=Date::format($tuto->date_upd)?>
		</div>
	</div>
<?php endforeach; ?>
