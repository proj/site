<div class="tabs">
	<h1>Tutoriels</h1>

	<a href="<?=$app->genLink('tuto.home')?>">Publi&eacute;s</a>
	<?php if (UserInfo::isLogged()): ?>
		<a href="<?=$app->genLink('tuto.my')?>">Mes tutoriels</a>
	<?php endif; ?>
	<a href="<?=$app->genLink('tuto.search')?>" class="active">Rechercher</a>
</div>

<form method="post" action="<?=$app->genLink('tuto.search.post')?>">
	<input type="text" name="value" value="" />

	<input type="submit" value="Rechercher" class="btn btn-primary btn-colored" />
</form>
