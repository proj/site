<?php

$app->get('site.test', 'test', function($app) {
	$content = $app->view(__DIR__ . '/views/test.php');

	$page = $app->viewTpl();
	$page->title         = 'Test';
	$page->content       = $content;
	$page->curModule     = 'test';
	$page->breadcrumb    = [
		'Test' => false
	];

	echo $page;
});
