<?php

$app->get('site.about', 'about', function($app) {
	$content = $app->view(__DIR__ . '/views/about.php');

	$page = $app->viewTpl();
	$page->title      = '&Agrave; propos';
	$page->content    = $content;
	$page->curModule  = 'about';
	$page->breadcrumb = [
		'&Agrave; propos de Bit My Code' => false
	];

	echo $page;
});
