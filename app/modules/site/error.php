<?php

$app->error(function($app, $code) {
	$content = $app->view(__DIR__ . '/views/error.php');
	$content->code = $code;

	$page = $app->viewTpl();
	$page->title   = 'Erreur ' . $code;
	$page->content = $content;

	echo $page;
});
