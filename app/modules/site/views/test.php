<button class="btn">Bouton</button>
<button class="btn btn-red">Bouton</button>
<button class="btn btn-green">Bouton</button>
<button class="btn btn-blue">Bouton</button>
<button class="btn btn-orange">Bouton</button>
<button class="btn btn-primary">Bouton</button>
<button class="btn btn-secondary">Bouton</button>
<button class="btn" disabled>Bouton</button>

<br />

<button class="btn btn-colored">Bouton</button>
<button class="btn btn-colored btn-red">Bouton</button>
<button class="btn btn-colored btn-green">Bouton</button>
<button class="btn btn-colored btn-blue">Bouton</button>
<button class="btn btn-colored btn-orange">Bouton</button>
<button class="btn btn-colored btn-primary">Bouton</button>
<button class="btn btn-colored btn-secondary">Bouton</button>
<button class="btn btn-colored btn-colored" disabled>Bouton</button>

<br />

<button class="btn btn-shadow">Bouton</button>
<button class="btn btn-shadow btn-red">Bouton</button>
<button class="btn btn-shadow btn-green">Bouton</button>
<button class="btn btn-shadow btn-blue">Bouton</button>
<button class="btn btn-shadow btn-orange">Bouton</button>
<button class="btn btn-shadow btn-primary">Bouton</button>
<button class="btn btn-shadow btn-secondary">Bouton</button>
<button class="btn btn-shadow btn-colored" disabled>Bouton</button>

<br />

<button class="btn btn-colored btn-shadow">Bouton</button>
<button class="btn btn-colored btn-shadow btn-red">Bouton</button>
<button class="btn btn-colored btn-shadow btn-green">Bouton</button>
<button class="btn btn-colored btn-shadow btn-blue">Bouton</button>
<button class="btn btn-colored btn-shadow btn-orange">Bouton</button>
<button class="btn btn-colored btn-shadow btn-primary">Bouton</button>
<button class="btn btn-colored btn-shadow btn-secondary">Bouton</button>
<button class="btn btn-colored btn-shadow btn-colored" disabled>Bouton</button>

<br />

<button class="btn btn-colored btn-blue">Bouton</button>
<button class="btn btn-colored btn-orange">Bouton</button>

<div class="btn-group">
	<button class="btn btn-colored btn-secondary">Bouton</button>
	<button class="btn btn-colored btn-primary">Bouton</button>
	<button class="btn btn-colored btn-green">Bouton</button>
</div>

<div class="btn-group">
	<button class="btn btn-colored btn-secondary dropdown-toggle" data-toggle="dropdown">
		Bouton <span class="caret"></span>
	</button>

	<ul class="dropdown-menu">
		<li><a href="#">Ajouter aux favoris</a></li>
		<li><a href="#">Télécharger</a></li>
		<li><a href="#">Rafraichir</a></li>
		<li class="divider"></li>
		<li><a href="#">Afficher les choix</a></li>
	</ul>
</div>

<div class="btn-group">
	<button class="btn btn-colored btn-secondary">Bouton</button>

	<button class="btn btn-colored btn-secondary dropdown-toggle" data-toggle="dropdown">
		<span class="caret"></span>
	</button>

	<ul class="dropdown-menu">
		<li><a href="#">Ajouter aux favoris</a></li>
		<li><a href="#">Télécharger</a></li>
		<li><a href="#">Rafraichir</a></li>
		<li class="divider"></li>
		<li><a href="#">Afficher les choix</a></li>
	</ul>
</div>

<button class="btn btn-colored btn-red">Bouton</button>
