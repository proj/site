<ul>
	<li>
		<a href="<?=$app->genLink('user.list')?>">
			Liste des utilisateurs
		</a>
	</li>

	<?php if (UserInfo::isLogged()): ?>
		<li>
			<a href="<?=$app->genLink('user.notes')?>">
				Prise de notes
			</a>
		</li>
	<?php endif; ?>
</ul>
