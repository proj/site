<form method="post" action="<?=$app->genLink('site.login.valid')?>">
	<label for="username">Pseudo</label>
		<input type="text" name="username" id="username" /><br />

	<label for="passwd">Mot de passe</label>
		<input type="password" name="passwd" id="passwd" /><br />

	<label for="remember-me">Se souvenir de moi ?</label>
		<input type="checkbox" name="remember-me" id="remember-me" /><br />

	<input
		type="submit"
		name="login"
		value="Connexion"
		class="btn btn-primary btn-colored" />
</form>

<p class="cookies">
	Ce site utilise un cookie pour maintenir votre connexion. En vous connectant
	en ayant coché &laquo; Se souvenir de moi ? &raquo;, vous acceptez le dépôt de
	ce cookie.
</p>
