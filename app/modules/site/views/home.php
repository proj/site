<h1 class="home-sitename">Bit My Code</h1>

<div class="section-presentation">
	<div class="row">
		<div class="grid-md-100 grid-lg-30">
			<img src="/web/img/balloon.png" alt="" class="home-balloon" />
		</div>

		<div class="grid-md-100 grid-lg-70">
			<div class="wrapper">
				<h2>Partageons nos connaissances</h2>

				<p>Bit My Code est un site sur le <strong>développement
					informatique</strong>. Nous y partageons nos découvertes, nos
					apprentissages et notre culture.</p>

				<p>Initialement, nous sommes un groupe de personnes passionnées par
					l'informatique. En cela, nous avons <a href="#projects">divers
					projets</a>.</p>

				<p>En nous rejoignant, vous acceptez de recevoir une dose de
					connaissances.</p>
			</div>
		</div>
	</div>
</div>

<div class="section-presentation section-presentation-gray">
	<div class="wrapper">
		<h2 id="projects">Nos projets</h2>

		<p>Nos projets sont, pour la plupart, open-source, c’est-à-dire que vous
			pouvez lire les sources et participer au développement en donnant des
			idées ou en participant directement à l'écriture du code.</p>
	</div>

	<div class="row">
		<div class="grid-sm-100 grid-md-50 grid-xl-25 project-card">
			<h3 class="project-title">
				<a href="https://terranova.bitmycode.com/">TerraNova</a>
			</h3>

			<img src="/web/img/projects/terranova.png"
				class="project-image"
				alt="" />

			<p class="project-description">
				Découverte et construction dans un univers spatial 3D.
			</p>
		</div>

		<div class="grid-sm-100 grid-md-50 grid-xl-25 project-card">
			<h3 class="project-title">Bit My Code</h3>

			<img src="/web/img/projects/bit-my-code.png"
				class="project-image"
				alt="" />

			<p class="project-description">
				Partagez et découvrez l’immensité du monde.
			</p>
		</div>

		<div class="grid-sm-100 grid-md-50 grid-xl-25 project-card">
			<h3 class="project-title">Project Fighting</h3>

			<img src="/web/img/projects/project-fighting.png"
				class="project-image"
				alt="" />

			<p class="project-description">
				Jeu de tir en arène en ligne.
			</p>
		</div>

		<div class="grid-sm-100 grid-md-50 grid-xl-25 project-card">
			<h3 class="project-title">Project Monopoly</h3>

			<img src="/web/img/projects/project-monopoly.jpg"
				class="project-image"
				alt="" />

			<p class="project-description">
				Jeu du Monopoly sur ordinateur.
			</p>
		</div>
	</div>
</div>
