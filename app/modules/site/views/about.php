<p>
	Bit My Code est un groupe de développement qui a été fondé le 27 avril 2013
	par Themaxiboss après une idée de Xildu et a été légué à PifyZ le 25 mai 2013
	après l'inactivité de ses créateurs.
</p>

<p>
	Le groupe a par la suite évolué pour accepter des graphistes et même certaines
	personnes dont la passion n'est pas le développement sont acceptées.
</p>

<p>
	Nous sommes aujourd'hui très ouvert sur le domaine de la réflexion et nous
	n'hésitons pas à débattre sur certains sujets.
</p>

<p>
	Aujourd'hui, nous créons de nombreux projets individuels voire collectifs.
	Rien n'a vraiment abouti pour l'instant mais nous avons espoir que l'évolution
	du groupe mène à de grandes choses.
</p>

<p>
	Ce site est là pour unir les membres du groupe mais l'inscription a été
	ouverte à tous afin de ne pas freiner l'évolution du groupe. Nous souhaitons
	voir une communauté se former. &Agrave; travers vous, nous voyons un avenir
	meilleur.
</p>
