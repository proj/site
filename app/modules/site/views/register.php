<form method="post" action="<?=$app->genLink('site.register.valid')?>">
    <label for="username">Pseudo</label>
    <input type="text" name="username" id="username" /><br />

    <label for="passwd">Mot de passe</label>
    <input type="password" name="passwd" id="passwd" /><br />

    <div id="lbl-passwd2">
        <label for="passwd">Mot de passe</label>
        <input type="password" name="passwd2" id="passwd2" /><br />
    </div>

    <input
      type="submit"
      name="register"
      value="Inscription"
      id="btn-register"
      class="btn btn-primary btn-colored" />
</form>
