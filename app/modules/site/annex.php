<?php

$app->get('site.annex', 'annex', function($app) {
	$content = $app->view(__DIR__ . '/views/annex.php');

	$page = $app->viewTpl();
	$page->title      = 'Page annexes';
	$page->content    = $content;
	$page->curModule  = 'annex';
	$page->breadcrumb = [
		'Pages annexes' => false
	];

	echo $page;
});
