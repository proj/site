<?php

$app->get('site.register', 'register', function($app) {
	if (UserInfo::isLogged())
		$app->follow('site.home');

	$content = $app->view(__DIR__ . '/views/register.php');

	$page = $app->viewTpl();
	$page->title      = 'Inscription';
	$page->content    = $content;
	$page->curModule  = 'register';
	$page->breadcrumb = [
		'Inscription' => false
	];

	echo $page;
})

->post('site.register.valid', 'register', function($app) {
	if (UserInfo::isLogged())
		$app->follow('site.home');

	$username = Input::get('username');
	$passwd1  = Input::get('passwd');
	$passwd2  = Input::get('passwd2');

	if ($passwd1 == '' || $passwd2 == '')
  	Flash::pushError('Mot de passe obligatoire');
	else if ($passwd1 != $passwd2)
		Flash::pushError('Les deux mots de passes ne sont pas identiques');

	if (preg_match('~[^a-zA-Z0-9_-]+~', $username))
		Flash::pushError('Format du pseudo incorrect');

	$username = User::cleanUsername($username);

	if (strlen($username) < 3) {
		Flash::pushError('Pseudo incorrect, il doit faire plus de 3 caractères');
	} else {
		$testUser = UserInfo::getByUsername($username);

		if ($testUser && $testUser->id)
    	    Flash::pushError('Pseudo déjà utilisé');
	}

	if (Flash::hasNoErrors()) {
		$newUser = new UserInfo();

		$newUser->username    = $username;
		$newUser->passwd      = Secure::genHash($passwd1);
		$newUser->permissions = P::user();

		if (!$newUser->save())
			Flash::pushError('Il y a eu une erreur lors de l\'enregistrement de l\'utilisateur');
	}

	if (Flash::hasNoErrors())
		Flash::pushSuccess('Enregistrement de l\'utilisateur réussi');

	$app->follow('site.register');
});
