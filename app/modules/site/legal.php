<?php

$app->get('site.legal', 'legal', function($app) {
	$content = $app->view(__DIR__ . '/views/legal.php');

	$page = $app->viewTpl();
	$page->title     = 'Mentions légales';
	$page->content   = $content;
	$page->curModule = 'legal';

	echo $page;
});
