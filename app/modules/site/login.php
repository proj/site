<?php

$app->get('site.login', 'login', function($app) {
	if (UserInfo::isLogged())
		$app->follow('site.home');

	$content = $app->view(__DIR__ . '/views/login.php');

	$page = $app->viewTpl();
	$page->title      = 'Connexion';
	$page->content    = $content;
	$page->curModule  = 'login';
	$page->breadcrumb = [
		'Connexion' => false
	];

	echo $page;
})

->post('site.login.valid', 'login', function($app) {
	if (UserInfo::isLogged())
		$app->follow('site.home');

	$remember_me = Input::has('remember-me');
	$username    = Input::get('username');
	$passwd      = Input::get('passwd');

	$test_user = UserInfo::getByUsername($username);

	if ($test_user->id) {
		if (!Secure::testPassword($passwd, $test_user->passwd))
			Flash::pushError('Mauvais mot de passe');
	} else {
		Flash::pushError('Compte inexistant');
	}

	if (Flash::hasNoErrors()) {
		$_SESSION['id'] = $test_user->id;
		$test_user->id;

		if ($remember_me) {
			$token = sha1(Secure::randStr(32));

			$date_expire = time() + (60 * 60 * 24 * 7 * 2); // Deux semaines

			setcookie('bmc-username', $test_user->username, $date_expire);
			setcookie('bmc-sid', $test_user->sid, $date_expire);
			setcookie('bmc-token', $token, $date_expire);

			$test_user->token = $token;
			$test_user->save(true);
		}

		Flash::pushSuccess('Connexion réussie');
	}

	$app->follow('site.login');
});
