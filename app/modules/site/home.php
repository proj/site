<?php

$app->get('site.home', '/', function($app) {
	$content = $app->view(__DIR__ . '/views/home.php');

	$page = $app->viewTpl();
	$page->overrideTitle = true;
	$page->title         = 'Bit My Code - Développement informatique et graphisme';
	$page->description   = 'Communauté formée autour de l\'amour de la programmation informatique, du graphisme et du
		questionnement du monde';
	$page->content       = $content;
	$page->curModule     = 'home';

	echo $page;
});
