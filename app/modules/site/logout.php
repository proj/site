<?php

$app->get('site.logout', 'logout', function($app) {
	if (!UserInfo::isLogged())
		$app->follow('site.home');

	unset($_COOKIE['bmc-username']);
	unset($_COOKIE['bmc-sid']);
	unset($_COOKIE['bmc-token']);

	setcookie($_COOKIE['bmc-username'], null, -1, '/');
	setcookie($_COOKIE['bmc-sid'], null, -1, '/');
	setcookie($_COOKIE['bmc-token'], null, -1, '/');

	session_destroy();

	$app->follow('site.home');
});
