<?php

$app->get('user.list', 'users', function($app) {
	$content = $app->view(__DIR__ . '/views/list.php');
	$content->users = User::listUsers();

	$page = $app->viewTpl();
	$page->title      = 'Liste des utilisateurs';
	$page->content    = $content;
	$page->breadcrumb = [
		'Liste des utilisateurs' => false
	];

	echo $page;
});
