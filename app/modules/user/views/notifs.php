<ul class="list-notifications">
	<?php foreach($notifications as $notif): ?>
		<li>
			Une nouvelle réponse sur
			<a href="<?=$app->genLink('forum.topic', $notif->slug)?>#message-<?=$notif->id_ref?>">
				<?=$notif->title?>
			</a>
		</li>
	<?php endforeach; ?>
</ul>
