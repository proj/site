<a href="<?=$app->genLink('user.view', $user->username)?>">
	Voir mon profil public
</a>

<form method="post" action="<?=$app->genLink('user.change-passwd')?>">
	<label for="actual-passwd">Mot de passe actuel</label>
		<input type="password" name="actual-passwd" id="actual-passwd" /><br />

	<label for="new-passwd1">Nouveau mot de passe</label>
		<input type="password" name="new-passwd1" id="new-passwd1" /><br />

	<label for="new-passwd2">Nouveau mot de passe</label>
	<input type="password" name="new-passwd2" id="new-passwd2" /><br />

	<input
		type="submit"
		name="change-passwd"
		value="Modifier mon mot de passe"
		class="btn btn-primary btn-colored" />
</form>

<form method="post" action="<?=$app->genLink('user.chang-signature')?>">
	<label for="signature">Signature</label>
		<input
			type="text"
			name="signature"
			id="signature"
			value="<?=$user->signature?>" /><br />

	<input
		type="submit"
		name="change-signature"
		value="Modifier ma signature"
		class="btn btn-primary btn-colored" />
</form>

<form
	method="post"
	action="<?=$app->genLink('user.change-avatar')?>"
	enctype="multipart/form-data"
>
	<label for="avatar">Avatar</label>
		<img src="<?=$user->getAvatarUrl()?>" alt="" width="80" height="80" /><br />
		<input type="file" name="avatar" id="avatar" /><br />

	<input
		type="submit"
		name="change-avatar"
		value="Modifier mon avatar"
		class="btn btn-primary btn-colored" />
</form>

<form method="post" action="<?=$app->genLink('user.remove-avatar')?>">
	<input
		type="submit"
		value="Supprimer mon avatar"
		class="btn btn-primary btn-colored" />
</form>

<h2>Liste de vos bannissements</h2>
<?php if (!$bans): ?>
	<p>Vous êtes un bon élève, vous n'avez jamais été banni ! =)</p>
<?php else: ?>
	<table>
		<tr>
			<th>Date de début</th>
			<th>Date de fin</th>
			<th>Raison</th>
		</tr>

		<?php foreach ($bans as $ban): ?>
			<tr>
				<td><?=Date::format($ban->date_begin, '{DD} {MO_tr} {YYYY}')?></td>
				<td><?=Date::format($ban->date_end, '{DD} {MO_tr} {YYYY}')?></td>
				<td><?=$ban->reason?></td>
			</tr>
		<?php endforeach; ?>
	</table>
<?php endif; ?>
