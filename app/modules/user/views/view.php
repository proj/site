<img src="<?=$an_user->getAvatarUrl()?>" alt="" width="80" height="80" />

<ul>
	<li>
		Date d'inscription :
		<?=Date::format($an_user->dateRegister, '{DD_tr} {DD} {MO_tr} {YYYY}')?>
	</li>

	<li>
		Dernière activité :
		<?=Date::format($an_user->dateLastActivity, '{DD_tr} {DD} {MO_tr} {YYYY}')?>
	</li>
</ul>
