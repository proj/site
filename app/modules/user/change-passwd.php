<?php

$app->post('user.change-passwd', 'user/change_passwd', function($app) {
	$app->filter('user.connected');

	$actual_passwd = Input::get('actual-passwd');
	$new_passwd1   = Input::get('new-passwd1');
	$new_passwd2   = Input::get('new-passwd2');

	if (!Secure::testPassword($actual_passwd, $app->user->passwd))
		Flash::pushError('Mot de passe actuel invalide');

	if (empty($new_passwd1) && empty($new_passwd2))
		Flash::pushError('Le nouveau mot de passe doit être saisie');

	if ($new_passwd1 != $new_passwd2)
		Flash::pushError('Les nouveaux mots de passes doivent être identiques');

	if (Flash::hasNoErrors()) {
		$app->user->passwd = Secure::genHash($new_passwd1);

		if (!$app->user->save())
			Flash::pushError('Il y a eu une erreur lors de la modification du mot de passe');
	}

	if (Flash::hasNoErrors())
		Flash::pushSuccess('Modification de mot de passe réussie');

	$app->follow('user.edit');
});
