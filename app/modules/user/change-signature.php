<?php

$app->post('user.chang-signature', 'user/change-signature', function($app) {
	$app->filter('user.connected');

	$signature = trim(Input::get('signature'));

	if (Flash::hasNoErrors()) {
		$app->user->signature = $signature;

		if (!$app->user->save())
			Flash::pushError('Il y a eu une erreur lors de la modification de la signature');
	}

	if (Flash::hasNoErrors())
		Flash::pushSuccess('Modification de la signature réussie');

	$app->follow('user.edit');
});
