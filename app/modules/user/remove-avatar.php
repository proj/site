<?php

$app->post('user.remove-avatar', 'user/remove_avatar', function($app) {
	unlink(realpath('web/img/mava/cache/') . '/' . $app->user->username . '.png');

	$app->follow('user.edit');
});
