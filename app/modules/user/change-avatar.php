<?php

$app->post('user.change-avatar', 'user/change-avatar', function($app) {
	$app->filter('user.connected');

	if (!isset($_FILES['avatar'])) {
		Flash::pushError('Vous n\'avez sélectionné aucune image');
	} else {
		$file_type = pathinfo($_FILES['avatar']['name'], PATHINFO_EXTENSION);

		if (!in_array($file_type, [ 'jpg', 'jpeg', 'png', 'gif', 'bmp' ]))
			Flash::pushError('Le format d\'image est incorrect');

		if ($_FILES['avatar']['size'] > 500000)
			Flash::pushError('L\'image est trop lourde');
	}

	if (Flash::hasNoErrors()) {
		$uploadfile = realpath('web/img/mava/cache/') . '/' . $app->user->username . '.png';

		if (!move_uploaded_file($_FILES['avatar']['tmp_name'], $uploadfile))
			Flash::pushError('Il y a eu une erreur lors de la modification de l\'avatar');
	}

	if (Flash::hasNoErrors())
		Flash::pushSuccess('Modification de l\'avatar réussie');

	$app->follow('user.edit');
});
