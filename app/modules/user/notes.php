<?php

$app->get('user.notes', 'notes', function($app) {
	$app->filter('user.connected');

	$content = $app->view(__DIR__ . '/views/notes.php');

	$page = $app->viewTpl();
	$page->title      = 'Notes';
	$page->content    = $content;
	$page->curModule  = 'notes';
	$page->breadcrumb = [
		'Prise de notes' => false
	];

	echo $page;
})

->post('user.notes.post', 'notes', function($app) {
	$app->filter('user.connected');

	$content = Input::get('content');

	Flash::pushSuccess('Notes modifiées');

	$app->user->notes = $content;
	$app->user->save();

	$app->follow('user.notes');
});
