<?php

$app->get('user.edit', 'user/edit', function($app) {
	$app->filter('user.connected');

	$bans = DB::select('
		date_begin, date_end, reason
		from user_banishment ub
		where id_user = :id_user
	', [
		'id_user' => $app->user->id
	])->all();

	$content = $app->view(__DIR__ . '/views/edit.php');
	$content->bans = $bans;

	$page = $app->viewTpl();
	$page->title      = '&Eacute;dition du profil';
	$page->content    = $content;
	$page->breadcrumb = [
		'Votre profil'   => $app->genLink('user.view', $app->user->username),
		'&Eacute;dition' => false
	];

	echo $page;
});
