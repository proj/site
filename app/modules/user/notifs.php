<?php

$app->get('user.notifs', 'notifs', function($app) {
	$app->filter('user.connected');

	$notifications = Notif::getAllNotifs($app->user);

	$content = $app->view(__DIR__ . '/views/notifs.php');
	$content->notifications = $notifications;

	$page = $app->viewTpl();
	$page->title      = 'Mes notifications';
	$page->content    = $content;
	$page->breadcrumb = [
		'Notifications' => false
	];

	echo $page;
});
