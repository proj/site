<?php

$app->get('user.view', '~([a-zA-Z0-9_-]+)', function($app, $username) {
	$an_user = UserInfo::getByUsername($username);

	if (!$an_user)
		$app->follow('site.home');

	$content = $app->view(__DIR__ . '/views/view.php');
	$content->an_user = $an_user;

	$page = $app->viewTpl();
	$page->title      = 'Profil de ' . $username;
	$page->content    = $content;
	$page->breadcrumb = [
		'Profil de ' . $an_user->username => false
	];

	echo $page;
});
