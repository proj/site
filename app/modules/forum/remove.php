<?php

$app->get('forum.topic.remove', 'forum/topic/remove/{alphanum}', function($app, $slug) {
	$app->filter('user.connected');

	if (!$app->user->has(P::SUPER_ADMINISTRATOR))
		$app->follow('forum.home');

	$topic = Topic::oneTopic($slug, $app->user);

	if (!$topic)
		$app->follow('forum.home');

	$archive_topic = Topic::removeTopic($topic);

	if ($archive_topic)
		Flash::pushSuccess('Sujet supprimé');
	else
		Flash::pushError('Il y a eu un problème lors de la suppression du sujet');

	$app->follow('forum.home');
});
