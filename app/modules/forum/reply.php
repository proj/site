<?php

$app->post('forum.topic.reply', 'forum/topic/{alphanum}/{number}/reply', function($app, $slug, $page) {
	$app->filter('user.connected');

	$topic = Topic::oneTopic($slug, $app->user);

	if (!$topic)
		$app->follow('forum.home');

	$content = trim(Input::get('content'));

	if (empty($content))
		Flash::pushError('Veuillez saisir un contenu');

	if (!$app->user->has(P::POST_FORUM))
		Flash::pushError('Vous n\'avez pas les droits pour poster sur le forum');

	if ($topic->locked && !$app->user->has(P::MODERATOR))
		Flash::pushError('Le sujet est verrouillé');

	if (Flash::hasNoErrors()) {
		$insert_reply = Topic::addReply($topic, $app->user, [
			'content' => $content,
		]);

		Notif::push(Notif::FORUM_REPLY, $insert_reply);

		if (!$insert_reply)
			Flash::pushError('Il y a eu une erreur lors de la création de la réponse');
	}

	if (Flash::hasNoErrors())
		Flash::pushSuccess('Réponse ajoutée');

	$p = new Pagination(Topic::nbMessagesTopic($topic, $app->user), MESSAGES_BY_PAGE, $page);

	$app->follow('forum.topic', $topic->slug, $p->last);
});
