<?=$tabs?>

<?php if (UserInfo::isLogged()): ?>
	<a href="#" class="toggle-link" data-toggle="newtopic">Nouveau sujet</a>

	<div id="newtopic" class="toggle">
		<form method="post" action="<?=$app->genLink('forum.post-topic')?>">
			<label>Titre</label>
			<input type="text" name="title" /><br />

			<label>Catégorie</label>
			<select name="slug_category">
				<optgroup label="Cat&eacute;gorie"></optgroup>

				<?php foreach ($categories as $c): ?>
					<?php if ($category && $c->slug == $category->slug): ?>
						<option value="<?=$c->slug?>" selected>
							<?=htmlspecialchars($c->title)?>
						</option>
					<?php else: ?>
						<option value="<?=$c->slug?>">
							<?=htmlspecialchars($c->title)?>
						</option>
					<?php endif; ?>
				<?php endforeach; ?>
			</select><br />

			<label>Message</label>
			<div class="write-message">
				<div class="message">
					<textarea name="content"></textarea>
				</div>
			</div>

			<input
				type="submit"
				name="new-topic"
				value="Valider"
				class="btn btn-primary btn-colored" />
		</form>
	</div>
<?php endif; ?>

<?php if ($category): ?>
	<?=$app->pagination(realpath(__DIR__ . '/../../../views/tpl/pagination.php'), 'forum.category', $p, $category->slug)?>
<?php else: ?>
	<?=$app->pagination(realpath(__DIR__ . '/../../../views/tpl/pagination.php'), 'forum.home', $p)?>
<?php endif; ?>

<?php foreach ($topics as $key => $topic): ?>
	<div class="topic<?=$topic->locked ? ' topic-locked' : ''?>">
		<div class="topic-title">
			<span
				class="<?=Topic::getHtmlIcon($topic)?>"
				title="<?=Topic::getHtmlStatus($topic)?>"
			></span>

			<a href="<?=$app->genLink('forum.topic', $topic->slug) . '/' . $topics_msg_first_unread[$key]->page . '#message-' . $topics_msg_first_unread[$key]->id?>">
				<?=htmlspecialchars($topic->title)?>
			</a>
		</div>

		<div class="topic-creator">
			<a href="<?=$app->genLink('user.view', $topic->author_begin)?>">
				<?=$topic->author_begin?>
			</a>
		</div>

		<div class="topic-last">
			<?=Date::format($topic->date_last, '{DD} {MO_tr} {YYYY}, {HH} h {MI}')?>
		</div>

		<div class="topic-nb-messages">
			<?=$topic->nb_messages?>
		</div>

		<!--
			; <?=$topic->participate ? 'Particip&eacute;' : ''?>
			; <?=$topic->archived ? 'Archiv&eacute;' : ''?>
			; <?=$topic->pinned ? '&Eacute;pingl&eacute;' : ''?>
		-->
	</div>
<?php endforeach; ?>

<?php if ($category): ?>
	<?=$app->pagination(realpath(__DIR__ . '/../../../views/tpl/pagination.php'), 'forum.category', $p, $category->slug)?>
<?php else: ?>
	<?=$app->pagination(realpath(__DIR__ . '/../../../views/tpl/pagination.php'), 'forum.home', $p)?>
<?php endif; ?>
