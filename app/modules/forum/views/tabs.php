<div class="tabs">
	<h1>Forum</h1>

	<?php if ($category): ?>
		<a href="<?=$app->genLink('forum.home')?>">Tout</a>
	<?php else: ?>
		<a href="<?=$app->genLink('forum.home')?>" class="active">Tout</a>
	<?php endif; ?>

	<?php foreach ($categories as $c): ?>
		<?php if ($category && $c->slug == $category->slug): ?>
			<a href="<?=$app->genLink('forum.category', $c->slug)?>" class="active"><?=$c->title?></a>
		<?php else: ?>
			<a href="<?=$app->genLink('forum.category', $c->slug)?>"><?=$c->title?></a>
		<?php endif; ?>
	<?php endforeach; ?>
</div>
