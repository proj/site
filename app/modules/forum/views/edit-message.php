<form
	method="post"
	action="<?=$app->genLink('forum.edit-message.edit', $message->id)?>"
>
	<div class="write-message">
		<div class="message">
			<textarea
				name="content"
				placeholder="Message"
				style="height: 500px"><?=$message->content?></textarea>
		</div>
	</div>

	<input
		type="submit"
		name="edit_message"
		value="Modifier ce message"
		class="btn btn-primary btn-colored" />
</form>
