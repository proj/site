<?=$tabs?>

<div class="forum-wrapper">
	<div class="forum-content">
		<?php if ($app->has(P::MODERATOR) || $app->has(P::ADMINISTRATOR)): ?>
			<a href="#" class="toggle-link" data-toggle="manage">Administrer</a>

			<div id="manage" class="toggle">
				<?php if ($app->has(P::MODERATOR)): ?>
					<form method="post">
						<div class="btn-group">
							<input
								type="submit"
								value="<?=$topic->locked ? 'D&eacute;verrouiller' : 'Verrouiller'?>"
								class="btn btn-orange"
								formaction="<?=$app->genLink('forum.topic.lock', $topic->slug)?>" />

							<input
								type="submit"
								value="<?=$topic->pinned ? 'D&eacute;s&eacute;pingler' : '&Eacute;pingler'?>"
								class="btn btn-secondary"
								formaction="<?=$app->genLink('forum.topic.pin', $topic->slug)?>" />

							<?php if ($app->has(P::ADMINISTRATOR)): ?>
								<button
									type="button"
									class="btn dropdown-toggle"
									data-toggle="dropdown"
								>
									<span class="caret"></span>
								</button>

								<ul class="dropdown-menu">
									<li>
										<a href="<?=$app->genLink('forum.topic.archive', $topic->slug)?>">
											<?=$topic->archived ? 'D&eacute;sarchiver' : 'Archiver'?>
										</a>
									</li>

									<li>
										<a href="<?=$app->genLink('forum.topic.remove', $topic->slug)?>">
											Supprimer le sujet
										</a>
									</li>
								</ul>
							<?php endif; ?>
						</div>
					</form>
				<?php endif; ?>

				<?php if ($app->has(P::MODERATOR)): ?>
					<ul>
						<li>
							<form
								method="post"
								action="<?=$app->genLink('forum.topic.move', $topic->slug, $p->actual)?>"
							>
								D&eacute;placer vers

								<select name="new_slug_category">
									<?php foreach ($categories as $category): ?>
										<?php if ($topic->id_category == $category->id): ?>
											<option
												value="<?=$category->slug?>"
												selected
											>
												<?=htmlentities($category->title)?>
											</option>
										<?php else: ?>
											<option
												value="<?=$category->slug?>"
											>
												<?=htmlentities($category->title)?>
											</option>
										<?php endif; ?>
									<?php endforeach; ?>
								</select>

								<input
									type="submit"
									value="D&eacute;placer"
									name="moveTopic"
									class="btn btn-primary btn-colored" />
							</form>
						</li>
						<li>
							<form
								method="post"
								action="<?=$app->genLink('forum.topic.rename', $topic->slug, $p->actual)?>"
							>
								<input
									type="text"
									name="new_title"
									value="<?=$topic->title?>" />

								<input
									type="submit"
									value="Renommer"
									name="renameTopic"
									class="btn btn-primary btn-colored" />
							</form>
						</li>
					</ul>
				<?php endif; ?>
			</div>
		<?php endif; ?>

		<?=$app->pagination(realpath(__DIR__ . '/../../../views/tpl/pagination.php'), 'forum.topic', $p, $topic->slug)?>

		<div class="messages-list">
			<?php foreach($messages as $message): ?>
				<div class="message" id="message-<?=$message->id?>">
					<div class="message-avatar">
						<a href="<?=$app->genLink('user.view', $message->username)?>">
							<img
								src="<?=UserInfo::getAvatarUrlByUsername($message->username)?>"
								alt="" />
						</a>
					</div>

					<div class="message-content">
						<div class="message-infos">
							<a
								href="<?=$app->genLink('user.view', $message->username)?>"
								class="message-username"
							>
								<?=$message->username?>
							</a>

							<a href="#message-<?=$message->id?>" class="message-little">
								<?=Date::relative($message->date_add)?>
							</a>

							<?php if ($message->date_add != $message->date_upd): ?>
								<a href="#message-<?=$message->id?>" class="message-little">
									Màj : <?=Date::relative($message->date_upd)?>
								</a>
							<?php endif; ?>

							<?php if ($user->id == $message->user_id || $user->has(P::MODERATOR)): ?>
								<a
									href="<?=$app->genLink('forum.edit-message', $message->id)?>"
									class="message-little"
								>
									Modifier ce message
								</a>
							<?php endif; ?>

							<?php if ($user->has(P::MODERATOR)): ?>
								<form
									method="post"
									action="<?=$app->genLink('forum.remove-message', $message->id)?>"
									class="inline-form"
								>
									<input
										type="submit"
										value="Supprimer le message"
										class="button-link message-little" />
								</form>
							<?php endif; ?>
						</div>

						<div class="message-message wrapper">
							<?=BBMarkDown::staticFormat($message->content)?>
						</div>

						<?php if (!empty($message->signature)): ?>
							<div class="message-signature">
								<?=$message->signature?>
							</div>
						<?php endif; ?>
					</div>
				</div>
			<?php endforeach; ?>
		</div>

		<?=$app->pagination(realpath(__DIR__ . '/../../../views/tpl/pagination.php'), 'forum.topic', $p, $topic->slug)?>

		<?php if (UserInfo::isLogged()): ?>
			<div class="title-section">Répondre</div>

			<?php if ($topic->locked && $user->hasNot(P::MODERATOR)): ?>
				<p>Le sujet est verrouillé.</p>
			<?php else: ?>
				<?php if ($topic->locked && $user->has(P::MODERATOR)): ?>
					<p>Le sujet est verrouillé mais votre rôle de &laquo;
						<?=P::toStr(P::MODERATOR)?> &raquo; vous permet de poster.</p>
				<?php endif; ?>

				<div id="answer">
					<form
						method="post"
						action="<?=$app->genLink('forum.topic.reply', $topic->slug, $p->actual)?>"
					>
						<div class="write-message">
							<div class="message">
								<textarea name="content" placeholder="Message"></textarea>
							</div>
						</div>

						<input
							type="submit"
							name="reply-topic"
							value="Répondre"
							class="btn btn-secondary btn-colored" />
					</form>
				</div>
			<?php endif; ?>
		<?php else: ?>
			<p>Vous devez
				<a href="<?=$app->genLink('site.register')?>">être inscrit</a>
				pour répondre à ce sujet.</p>
		<?php endif; ?>
	</div>
</div>
