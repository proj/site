<?php

$app->get('forum.edit-message', 'forum/edit_message/{number}', function($app, $message_id) {
	$app->filter('user.connected');

	$message = Topic::oneMessage($message_id, $app->user);

	if (!$message)
		$app->follow('forum.home');

	$content = $app->view(__DIR__ . '/views/edit-message.php');
	$content->message = $message;

	$page = $app->viewTpl();
	$page->title      = 'Tout - Forum - &Eacute;diter un message';
	$page->content    = $content;
	$page->curModule  = 'forum';
	$page->breadcrumb = [
		'&Eacute;diter un message' => false
	];

	echo $page;
})

->post('forum.edit-message.edit', 'forum/edit_message/{number}/edit', function($app, $message_id) {
	$app->filter('user.connected');

	$message = Topic::oneMessage($message_id, $app->user);

	if (!$message)
		$app->follow('forum.home');

	$content = Input::get('content');

	if (empty($content))
		Flash::pushError('Le message ne peut pas être vide');

	if (Flash::hasNoErrors()) {
		$data = [
			'content' => $content
		];

		if (!Topic::updateMessage($message, $app->user, $data))
			Flash::pushError('Il y a eu un problème lors de la modification du message');
	}

	if (Flash::hasNoErrors())
		Flash::pushSuccess('Message modifié avec succès');

	$app->follow('forum.topic', $message->slug);
});
