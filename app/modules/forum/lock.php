<?php

$app->post('forum.topic.lock', 'forum/topic/lock/{alphanum}', function($app, $slug) {
	$app->filter('user.connected');

	if (!$app->has(P::MODERATOR))
		$app->follow('forum.topic', $slug);

	$topic = Topic::oneTopic($slug, $app->user);

	if (!$topic)
		$app->follow('forum.home');

	$lockTopic = Topic::reverseLockTopic($topic);

	if ($lockTopic) {
		if ($topic->locked)
			Flash::pushSuccess('Sujet déverrouillé');
		else
			Flash::pushSuccess('Sujet verrouillé');

		$app->follow('forum.topic', $topic->slug);
	} else {
		if ($topic->locked)
			Flash::pushError('Il y a eu un problème lors du déverrouillage du sujet');
		else
			Flash::pushError('Il y a eu un problème lors de verrouillage du sujet');
	}

	$app->follow('forum.home');
});
