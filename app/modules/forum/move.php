<?php

$app->post('forum.topic.move', 'forum/topic/{alphanum}{paginate}/move', function($app, $slug, $page) {
	$app->filter('user.connected');

	if (!$app->user->has(P::MODERATOR))
		Flash::pushError('Vous n\'avez pas les droits pour déplacer ce sujet');

	$topic = Topic::oneTopic($slug, $app->user);

	if (!$topic)
		$app->follow('forum.home');

	$new_slug_category = Input::get('new_slug_category');

	$test_category = Category::oneCategory($new_slug_category);

	if (!$test_category)
		Flash::pushError('Nouvelle catégorie inexistante');

	if (Flash::hasNoErrors()) {
		if (!Topic::moveTopic($topic, $test_category))
			Flash::pushError('Il y a eu une erreur lors du déplacement du sujet');
	}

	if (Flash::hasNoErrors())
		Flash::pushSuccess('Sujet déplacé');

	$app->follow('forum.topic', $topic->slug, $page);
});
