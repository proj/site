<?php

$app->post('forum.remove-message', 'forum/removeMessage/{number}', function($app, $id) {
	$app->filter('user.connected');

	$message = Topic::oneMessage($id, $app->user);

	if (!$message || !$app->has(P::MODERATOR))
		$app->follow('forum.home');

	if (Topic::removeMessage($message))
		Flash::pushSuccess('Message supprimé avec succès');

	$app->follow('forum.topic', $message->slug);
});
