<?php

$app->get('forum.category', 'forum/category/{alphanum}{paginate}', function($app, $slug, $page = 1) {
	$category = Category::oneCategory($slug);

	if (!$category)
		$app->follow('forum.home');

	$p = new Pagination(Topic::nbTopicsCategory($category, $app->user), TOPICS_BY_PAGE, $page);

	$categories              = Category::listCategories();
	$topics                  = Topic::listTopicsCategory($app->user, $category, $p);
	$topics_msg_first_unread = Topic::listTopicsCategoryFirstUnreadMessage($app->user, $p, $category);

	$tabs = $app->view(__DIR__ . '/views/tabs.php');
	$tabs->category   = $category;
	$tabs->categories = $categories;

	$content = $app->view(__DIR__ . '/views/list-topics.php');
	$content->p                       = $p;
	$content->tabs                    = $tabs;
	$content->topics                  = $topics;
	$content->category                = $category;
	$content->categories              = $categories;
	$content->topics_msg_first_unread = $topics_msg_first_unread;

	$page = $app->viewTpl();
	$page->title      = htmlspecialchars($category->title) . ' - Forum';
	$page->content    = $content;
	$page->curModule  = 'forum';
	$page->breadcrumb = [
		'Forum' => $app->genLink('forum.home'),
		htmlspecialchars($category->title) => false
	];

	echo $page;
});
