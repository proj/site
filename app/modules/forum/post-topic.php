<?php

$app->post('forum.post-topic', 'forum/post_topic', function($app) {
	$app->filter('user.connected');

	$title         = trim(Input::get('title'));
	$content       = trim(Input::get('content'));
	$slug_category = Input::get('slug_category');

	$insert_topic = Topic::pushTopic($app->user, [
		'title'         => $title,
		'content'       => $content,
		'slug_category' => $slug_category
	]);

	if ($insert_topic) {
		Flash::pushSuccess('Sujet créé');
		$app->follow('forum.topic', $insert_topic->slug);
	} else {
		$app->follow('forum.home');
	}
});
