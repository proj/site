<?php

$app->post('forum.topic.pin', 'forum/topic/pin/{alphanum}', function($app, $slug) {
	$app->filter('user.connected');

	if (!$app->user->has(P::MODERATOR))
		$app->follow('forum.topic', $slug);

	$topic = Topic::oneTopic($slug, $app->user);

	if (!$topic)
		$app->follow('forum.home');

	$pin_topic = Topic::reversePinTopic($topic);

	if ($pin_topic) {
		if ($topic->pinned)
			Flash::pushSuccess('Sujet désépinglé');
		else
			Flash::pushSuccess('Sujet épinglé');

		$app->follow('forum.topic', $topic->slug);
	} else {
		if ($topic->pinned)
			Flash::pushError('Il y a eu un problème lors du désépinglage du sujet');
		else
			Flash::pushError('Il y a eu un problème lors de l\'épinglage du sujet');
	}

	$app->follow('forum.home');
});
