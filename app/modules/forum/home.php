<?php

$app->get('forum.home', 'forum{paginate}', function($app, $page = 1) {
	$p = new Pagination(Topic::nbTopics($app->user), TOPICS_BY_PAGE, $page);

	$categories              = Category::listCategories();
	$topics                  = Topic::listTopics($app->user, $p);
	$topics_msg_first_unread = Topic::listTopicsCategoryFirstUnreadMessage($app->user, $p);

	$tabs = $app->view(__DIR__ . '/views/tabs.php');
	$tabs->category   = false;
	$tabs->categories = $categories;

	$content = $app->view(__DIR__ . '/views/list-topics.php');
	$content->p                       = $p;
	$content->tabs                    = $tabs;
	$content->topics                  = $topics;
	$content->category                = false;
	$content->categories              = $categories;
	$content->topics_msg_first_unread = $topics_msg_first_unread;

	$page = $app->viewTpl();
	$page->title      = 'Tout - Forum';
	$page->content    = $content;
	$page->curModule  = 'forum';
	$page->breadcrumb = [
		'Forum'           => $app->genLink('forum.home'),
		'Tous les sujets' => false
	];

	echo $page;
});
