<?php

$app->post('forum.topic.rename', 'forum/topic/{alphanum}/{number}/rename', function($app, $slug, $page) {
	$app->filter('user.connected');

	if (!$app->user->has(P::MODERATOR))
		$app->follow('forum.home');

	$topic = Topic::oneTopic($slug, $app->user);

	if (!$topic)
		$app->follow('forum.home');

	$new_title = trim(Input::get('new_title'));

	$renameTopic = Topic::renameTopic($topic, $new_title);

	if ($renameTopic)
		Flash::pushSuccess('Sujet renommé');
	else
		Flash::pushError('Il y a eu un problème lors du renommage du sujet');

	$app->follow('forum.topic', $topic->slug, $page);
});
