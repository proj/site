<?php

$app->get('forum.topic.archive', 'forum/topic/archive/{alphanum}', function($app, $slug) {
	$app->filter('user.connected');

	if (!$app->has(P::ADMINISTRATOR))
		$app->follow('forum.topic', $slug);

	$topic = Topic::oneTopic($slug, $app->user);

	if (!$topic)
		$app->follow('forum.home');

	$archive_topic = Topic::reverseArchiveTopic($topic);

	if ($archive_topic) {
		if ($topic->archived)
			Flash::pushSuccess('Sujet désarchivé');
		else
			Flash::pushSuccess('Sujet archivé');

		$app->follow('forum.topic', $topic->slug);
	} else {
		if ($topic->archived)
			Flash::pushError('Il y a eu un problème lors du "désarchivage" du sujet');
		else
			Flash::pushError('Il y a eu un problème lors de l\'archivage du sujet');
	}

	$app->follow('forum.home');
});
