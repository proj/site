<?php

$app->get('blog.home', 'blog', function($app) {
	$articles = ArticleInfo::getList();

	$content = $app->view(__DIR__ . '/views/home.php');
	$content->articles = $articles;

	$page = $app->viewTpl();
	$page->title      = 'Blog';
	$page->content    = $content;
	$page->curModule  = 'blog';
	$page->breadcrumb = [
		'Articles' => false
	];

	echo $page;
});
