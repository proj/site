<?php

$app->get('blog.read', 'blog/{alphanum}', function($app, $slug) {
	$article = ArticleInfo::getBySlug($slug);

	if (!$article)
		$app->follow('blog.home');

	$content = $app->view(__DIR__ . '/views/read.php');
	$content->article = $article;

	$page = $app->viewTpl();
	$page->title      = htmlspecialchars($article->title) . ' - Blog';
	$page->content    = $content;
	$page->curModule  = 'blog';
	$page->breadcrumb = [
		'Articles' => $app->genLink('blog.home'),
		htmlspecialchars($article->title) => false
	];

	echo $page;
});
