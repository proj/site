<div class="list-articles">
	<?php foreach ($articles as $article): ?>
		<div class="article-line">
			<h2>
				<a href="<?=$app->genLink('blog.read', $article->slug)?>">
					<?=htmlentities($article->title)?>
				</a>
			</h2>

			<p>
				<?=Date::relative($article->dateArticle)?>
			</p>

			<p>
				<?=BBMarkDown::staticFormat($article->content)?>
			</p>
		</div>
	<?php endforeach; ?>
</div>
