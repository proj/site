<div class="card-article">
	<h1>
		<?=htmlentities($article->title)?>
	</h1>

	<p>
		<?=Date::relative($article->dateArticle)?>
	</p>

	<p>
		<?=BBMarkDown::staticFormat($article->content)?>
	</p>
</div>
