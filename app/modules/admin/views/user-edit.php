<form method="post" action="<?=$app->genLink('admin.user-edit-informations', $an_user->username)?>">
	<h2>Informations</h2>
	<label for="username">Pseudo</label>
	<input type="text" name="username" id="username" value="<?=$an_user->username?>" /><br />

	<label for="signature">Signature</label>
	<input type="text" name="signature" id="signature" value="<?=$an_user->signature?>" /><br />

	<?php if ($app->has(P::SUPER_ADMINISTRATOR)): ?>
		<label for="passwd">Mot de passe <small>(laisser vide si inchang&eacute;)</small></label>
		<input type="password" name="passwd" id="passwd" value="" /><br />
	<?php endif; ?>

	<input type="submit" name="user-edit" value="Modifier les informations" class="btn btn-primary btn-colored" />
</form>

<form method="post" action="<?=$app->genLink('admin.user-edit-permissions', $an_user->username)?>">
	<input type="hidden" name="username" value="<?=$an_user->username?>" />

	<h2>Permissions</h2>
	<?php foreach ($permissions as $k => $v): ?>
		<?php if (
			($app->has($v) && (!$app->has(P::ADMINISTRATOR) && $app->has(P::MODERATOR) && $v != P::MODERATOR)) ||
			($app->has($v) && ($app->has(P::ADMINISTRATOR) && $v != P::ADMINISTRATOR)) ||
			$app->has(P::SUPER_ADMINISTRATOR)
		): ?>
			<input
				type="checkbox"
				name="permissions[]"
				value="<?=$v?>"
				id="edit_user_permission_<?=$k?>" <?=($an_user->permissions & $v) > 0 ? 'checked' : ''?> />

			<label for="edit_user_permission_<?=$k?>"><?=P::toStr($k)?></label><br />
		<?php endif; ?>
	<?php endforeach; ?>

	<input type="submit" name="user-edit" value="Modifier les permissions" class="btn btn-primary btn-colored" />
</form>

<form method="post" action="<?=$app->genLink('admin.user-edit.ban', $an_user->username)?>">
	<h2>Bannissement</h2>
	<label for="date_begin">Date de début (incluse)</label>
	<input type="text" name="date_begin" id="date_begin" value="<?=date('d-m-Y')?>" />

	<label for="date_end">Date de fin (incluse)</label>
	<input type="text" name="date_end" id="date_end" value="<?=date('d-m-Y')?>" />

	<label for="reason">Raison</label>
	<input type="text" name="reason" id="reason" value="" />

	<label>Permissions</label>
	<?php foreach ($permissions as $k => $v): ?>
		<?php if (
			($app->has($v) && (!$app->has(P::ADMINISTRATOR) && $app->has(P::MODERATOR) && $v != P::MODERATOR)) ||
			($app->has($v) && ($app->has(P::ADMINISTRATOR) && $v != P::ADMINISTRATOR)) ||
			$app->has(P::SUPER_ADMINISTRATOR)
		): ?>
			<input type="checkbox" name="permission[]" value="<?=$v?>" id="edit_user_permission_<?=$k?>" <?=($an_user->permissions & $v) > 0 ? 'checked ' : ''?>/> <label for="edit_user_permission_<?=$k?>"><?=P::toStr($k)?></label><br />
		<?php endif; ?>
	<?php endforeach; ?>

	<input type="submit" name="user_banishment" value="Bannir" class="btn btn-primary btn-colored" />

	<h3>Liste des bannissements</h3>
	<table>
		<tr>
			<th>Date de début</th>
			<th>Date de fin</th>
			<th>Bannisseur</th>
			<th>Raison</th>
			<th>Permissions</th>
			<th>Anciennes permissions</th>
		</tr>

		<?php foreach ($bans as $ban): ?>
			<tr>
				<td><?=Date::format($ban->date_begin, '{DD} {MO_tr} {YYYY}')?></td>
				<td><?=Date::format($ban->date_end, '{DD} {MO_tr} {YYYY}')?></td>
				<td><?=$ban->banisher_username?></td>
				<td><?=$ban->reason?></td>
				<td><?=$ban->permissions?></td>
				<td><?=$ban->permissions_before?></td>
			</tr>
		<?php endforeach; ?>
	</table>
</form>
