<?php if ($app->has(P::SUPER_ADMINISTRATOR)): ?>
	<ul>
		<li><a href="<?=$app->genLink('admin.exec-sql')?>">Ex&eacute;cuter des requ&ecirc;tes</a></li>
		<li><a href="<?=$app->genLink('admin.tables')?>">Tables du site</a></li>
		<li><a href="<?=$app->genLink('admin.crud')?>">CRUD : Create - Read - Update - Delete</a></li>
	</ul>
<?php endif; ?>

<ul>
	<li><a href="<?=$app->genLink('admin.register')?>">[+] Enregistrer un utilisateur</a></li>

	<?php foreach ($users as $user): ?>
		<li><a href="<?=$app->genLink('admin.user-edit', $user->username)?>"><?=$user->username?></a></li>
	<?php endforeach; ?>
</ul>
