<ul>
	<?php foreach ($tables as $table_name => $table_fields): ?>
		<li>
			<h2><?=$table_name?></h2>

			<table>
				<tr>
					<td>Field</td>
					<td>Type</td>
					<td>Null</td>
					<td>Key</td>
					<td>Default</td>
					<td>Extra</td>
				 </tr>

				<?php foreach ($table_fields as $field): ?>
					<tr>
						<td><?=$field['Field']?></td>
						<td><?=$field['Type']?></td>
						<td><?=$field['Null']?></td>
						<td><?=$field['Key']?></td>
						<td><?=$field['Default']?></td>
						<td><?=$field['Extra']?></td>
					</tr>
				<?php endforeach; ?>
			</table>
		</li>
	<?php endforeach; ?>
</ul>
