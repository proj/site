<?php

$app->get('admin.exec-sql', 'admin/exec-sql', function($app, $query_ret = '') {
	$app->filter('user.connected');

	if (!$app->has(P::SUPER_ADMINISTRATOR))
		$app->follow('forum.home');

	$content = $app->view(__DIR__ . '/views/exec-sql.php');
	$content->query_ret = $query_ret;

	$page = $app->viewTpl();
	$page->title      = 'Ex&eacute;cuter une requ&ecirc;te SQL';
	$page->content    = $content;
	$page->curModule  = 'admin';
	$page->breadcrumb = [
		'Administration' => $app->genLink('admin.home'),
		'Ex&eacute;cuter des requ&ecirc;tes' => false
	];

	echo $page;
})

->post('admin.exec-sql.exec', 'admin/exec-sql/exec', function($app) {
	$app->filter('user.connected');

	if (!$app->has(P::SUPER_ADMINISTRATOR))
		$app->follow('forum.home');

	$query = DB::query(Input::get('query'));
	$query->execute();
	$query_ret = $query->fetchAll(\PDO::FETCH_CLASS);
	$query_ret = var_export($query_ret, true);

	Flash::pushSuccess('Requête peut-être exécutée correctement');

	$app->exec('admin.exec-sql', $query_ret);
});
