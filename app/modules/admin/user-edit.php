<?php

$app->get('admin.user-edit', 'admin/user-edit/{alphanum}', function($app, $username) {
	$app->filter('user.connected');

	if (!$app->has(P::MODERATOR))
		$app->follow('forum.home');

	$reflection_p = new ReflectionClass('P');
	$permissions  = $reflection_p->getConstants();

	$an_user = UserInfo::getByUsername($username);

	if (!$an_user->id) {
		Flash::pushError('Utilisateur inexistant');
		$app->follow('admin.home');
	}

	$bans = DB::select('
		ui.username banisher_username, ub.date_begin, ub.date_end, ub.reason,
		ub.permissions, ub.permissions_before
		from user_banishment ub
		inner join user_info ui on ui.id = ub.id_banisher
		where id_user = :id_user
	', [
		'id_user' => $an_user->id
	])->all();

	$content = $app->view(__DIR__ . '/views/user-edit.php');
	$content->permissions = $permissions;
	$content->an_user     = $an_user;
	$content->bans        = $bans;

	$page = $app->viewTpl();
	$page->title      = 'Edition d\'un utilisateur';
	$page->content    = $content;
	$page->curModule  = 'admin';
	$page->breadcrumb = [
		'Administration' => $app->genLink('admin.home'),
		'Edition du profil de ' . $an_user->username => false
	];

	echo $page;
})

->post('admin.user-edit-informations', 'admin/user-edit-informations/{alphanum}', function($app, $username) {
	$app->filter('user.connected');

	if (!$app->has(P::MODERATOR))
		$app->follow('forum.home');

	$an_user = UserInfo::getByUsername($username);

	if (!$an_user->id) {
		Flash::pushError('Utilisateur inexistant');
		$app->follow('admin.home');
	}

	// Modifier les informations d'un utilisateur
	// (pseudo, signature et mot de passe)
	$username  = Input::get('username');
	$signature = Input::get('signature');
	$passwd    = Input::get('passwd');

	if (preg_match('~[^a-zA-Z0-9_-]+~', $username))
		Flash::pushError('Format du pseudo incorrect');

	$username = User::cleanUsername($username);

	if (strlen($username) < 3) {
		Flash::pushError('Pseudo incorrect, il doit faire plus de 3 caractères');
	} else {
		$test_user = UserInfo::getByUsername($username);

		if ($test_user->id && $username != $an_user->username)
			Flash::pushError('Pseudo déjà utilisé');
	}

	$new_passwd = empty($passwd) || !$app->has(P::SUPER_ADMINISTRATOR)
		? $an_user->passwd
		: Secure::genHash($passwd);

	if (Flash::hasNoErrors()) {
		$an_user->username    = $username;
		$an_user->passwd      = $new_passwd;
		$an_user->signature   = $signature;

		if (!$an_user->save())
			Flash::pushError('Il y a eu une erreur lors de la modification des informations de l\'utilisateur');
	}

	if (Flash::hasNoErrors())
		Flash::pushSuccess('Utilisateur modifié avec succès');

	$app->follow('admin.user-edit', $username);
})

->post('admin.user-edit-permissions', 'admin/user-edit-permissions/{alphanum}', function($app, $username) {
	$app->filter('user.connected');

	if (!$app->has(P::MODERATOR))
		$app->follow('forum.home');

	$an_user = UserInfo::getByUsername($username);

	if (!$an_user->id) {
		Flash::pushError('Utilisateur inexistant');
		$app->follow('admin.home');
	}

	$new_permissions = 0;

	$permissions = Input::get('permissions', []);

	foreach ($permissions as $k => $v)
		$new_permissions += (int) $v;

	if ($new_permissions < 0 || $new_permissions > P::superAdministrator())
		Flash::pushError('Les permissions sont incorrectes');

	if (Flash::hasNoErrors()) {
		$an_user->permissions = $new_permissions;

		if (!$an_user->save())
			Flash::pushError('Il y a eu une erreur lors de la modification des permissions de l\'utilisateur');
	}

	if (Flash::hasNoErrors())
		Flash::pushSuccess('Permissions de l\'utilisateur modifiées avec succès');

	$app->follow('admin.user-edit', $username);
})

->post('admin.user-edit.ban', 'admin/user-edit/{alphanum}/ban', function($app, $username) {
	$app->filter('user.connected');

	if (!$app->has(P::MODERATOR))
		$app->follow('forum.home');

	$an_user = UserInfo::getByUsername($username);

	if (!$an_user->id) {
		Flash::pushError('Utilisateur inexistant');
		$app->follow('admin.home');
	}

	$dateBegin = Input::get('date_begin');
	$dateEnd   = Input::get('date_end');
	$reason    = Input::get('reason');

	$dateBegin = strtotime($dateBegin); // le jour choisi à minuit
	$dateEnd   = strtotime($dateEnd) + 86399; // le jour choisi à 23h59

	$newPermissions = 0;

	$permission = Input::get('permission', []);

	foreach ($permission as $k => $v)
		$newPermissions += (int) $v;

	if ($newPermissions < 0 || $newPermissions > P::superAdministrator())
		Flash::pushError('Il y a un problème de droits');

	if (Flash::hasNoErrors()) {
		$banishment = new UserBanishment();

		$banishment->idUser            = $an_user->id;
		$banishment->idBanisher        = $app->user->id;
		$banishment->dateBegin         = $dateBegin;
		$banishment->dateEnd           = $dateEnd;
		$banishment->reason            = $reason;
		$banishment->permissions       = $newPermissions;
		$banishment->permissionsBefore = $an_user->permissions;

		$banishment->save();

		if (!$banishment->id)
			Flash::pushError('Il y a eu une erreur lors du bannissement de l\'utilisateur');
	}

	if (Flash::hasNoErrors())
		Flash::pushSuccess('Utilisateur banni');

	$app->follow('admin.user-edit', $username);
});
