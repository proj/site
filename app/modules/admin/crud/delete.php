<?php

$app->get('admin.crud.delete', 'admin/crud/delete/{string}/{number}', function($app, $table, $id) {
	$app->filter('user.connected');

	if (!$app->has(P::SUPER_ADMINISTRATOR))
		$app->follow('forum.home');

	$class = Format::camelcasify($table);

	$inst = $class::getById($id);

	$inst->remove();

	$app->follow('admin.crud.list', $table);
});
