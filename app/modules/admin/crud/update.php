<?php

$app->get('admin.crud.update', 'admin/crud/update/{string}/{number}', function($app, $table, $id) {
	$app->filter('user.connected');

	if (!$app->has(P::SUPER_ADMINISTRATOR))
		$app->follow('forum.home');

	$class = Format::camelcasify($table);
	$model = (new ReflectionClass($class))->getName();

	$virgin = new $class();

	$class = $class::getById($id);

	$a = new ReflectionClass($class);
	$a_virgin = new ReflectionClass($virgin);

	$attributes = $a->getProperties(ReflectionProperty::IS_PUBLIC);
	$attributesVirgin = $a_virgin->getProperties(ReflectionProperty::IS_PUBLIC);

	$content = $app->view(__DIR__ . '/views/update.php');
	$content->id                = $id;
	$content->attributes        = $attributes;
	$content->attributesVirgin  = $attributesVirgin;
	$content->table             = $table;
	$content->class             = $class;
	$content->virgin            = $virgin;

	$page = $app->viewTpl();
	$page->title      = 'Accueil - CRUD - Liste';
	$page->content    = $content;
	$page->curModule  = 'admin';
	$page->breadcrumb = [
		'Administration' => $app->genLink('admin.home'),
		'CRUD'           => $app->genLink('admin.crud'),
		$model           => $app->genLink('admin.crud.list', $table),
		'Modifier'       => false
	];

	echo $page;
})

->post('admin.crud.update.post', 'admin/crud/update/{string}/{number}', function($app, $table, $id) {
	$app->filter('user.connected');

	if (!$app->has(P::SUPER_ADMINISTRATOR))
		$app->follow('forum.home');

	$class = Format::camelcasify($table);

	$inst   = $class::getById($id);
	$virgin = new $class();

	$refl = new ReflectionClass($class);

	foreach ($_POST as $key => $value) {
		if ($key != 'update') {
			$k    = Format::camelcasify($key);
			$type = gettype($refl->getProperty($k)->getValue($virgin));

			if ($type == 'boolean')
				$inst->$k = true;
			else
				$inst->$k = $value;
		}
	}

	$inst->save();

	$app->follow('admin.crud.list', $table);
});
