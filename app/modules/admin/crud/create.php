<?php

$app->get('admin.crud.create', 'admin/crud/create/{string}', function($app, $table) {
	$app->filter('user.connected');

	if (!$app->has(P::SUPER_ADMINISTRATOR))
		$app->follow('forum.home');

	$class_name = Format::camelcasify($table);
	$model = (new ReflectionClass($class_name))->getName();

	$class = new $model();

	$a = new ReflectionClass($class);

	$attributes = $a->getProperties(ReflectionProperty::IS_PUBLIC);

	$content = $app->view(__DIR__ . '/views/create.php');
	$content->attributes = $attributes;
	$content->table      = $table;
	$content->class      = $class;

	$page = $app->viewTpl();
	$page->title      = 'Accueil - CRUD - Liste';
	$page->content    = $content;
	$page->curModule  = 'admin';
	$page->breadcrumb = [
		'Administration' => $app->genLink('admin.home'),
		'CRUD'           => $app->genLink('admin.crud'),
		$model           => $app->genLink('admin.crud.list', $table),
		'Ajouter'        => false
	];

	echo $page;
})

->post('admin.crud.create.post', 'admin/crud/create/{string}', function($app, $table) {
	$app->filter('user.connected');

	if (!$app->has(P::SUPER_ADMINISTRATOR))
		$app->follow('forum.home');

	$class = Format::camelcasify($table);

	$inst = new $class();

	$refl = new ReflectionClass($class);

	foreach ($_POST as $key => $value) {
		if ($key != 'create') {
			$k    = Format::camelcasify($key);
			$type = gettype($refl->getProperty($k)->getValue($inst));

			if ($type == 'boolean')
				$inst->$k = true;
			else
				$inst->$k = $value;
		}
	}

	$inst->save();

	$app->follow('admin.crud.list', $table);
});
