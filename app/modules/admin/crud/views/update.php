<form method="post" action="<?=$app->genLink('admin.crud.update.post', $table, $id)?>">
	<?php
		foreach ($attributesVirgin as $k => $attribute):
			$value = $attribute->getValue($class);
	?>
		<label><?=$attribute->getName()?></label>

		<?php if (is_int($attribute->getValue($virgin))): ?>
			<input
				name="<?=Format::underscorify($attribute->getName())?>"
				value="<?=$value?>"
				type="text" /><br />
		<?php elseif (is_string($attribute->getValue($virgin))): ?>
			<textarea
				name="<?=Format::underscorify($attribute->getName())?>""><?=$value?></textarea><br />
		<?php elseif (is_bool($attribute->getValue($virgin))): ?>
			<input
				name="<?=Format::underscorify($attribute->getName())?>"
				type="checkbox"
				<?=$value==1 ? ' checked' : ''?>/><br />
		<?php endif; ?>
	<?php endforeach; ?>

	<input type="submit" name="update" value="Modifier" class="btn btn-primary btn-colored" />
</form>
