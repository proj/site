<form method="post" action="<?=$app->genLink('admin.crud.create.post', $table)?>">
	<?php
		foreach ($attributes as $attribute):
			$value = $attribute->getValue($class);
	?>
		<label><?=$attribute->getName()?></label>

		<?php if (is_int($value)): ?>
			<input name="<?=Format::underscorify($attribute->getName())?>" type="text" /><br />
		<?php elseif (is_string($value)): ?>
			<textarea name="<?=Format::underscorify($attribute->getName())?>""></textarea><br />
		<?php elseif (is_bool($value)): ?>
			<input name="<?=Format::underscorify($attribute->getName())?>" type="checkbox" /><br />
		<?php endif; ?>
	<?php endforeach; ?>

	<input type="submit" name="create" value="Ajouter" class="btn btn-primary btn-colored" />
</form>
