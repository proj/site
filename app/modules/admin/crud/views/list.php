<a href="<?=$app->genLink('admin.crud.create', $table)?>">Ajouter</a>

<table>
	<tr>
		<th></th>

		<?php foreach ($attributes as $attribute): ?>
			<th><?=$attribute->getName()?></th>
		<?php endforeach; ?>
	</tr>

	<?php foreach ($results as $result): ?>
		<tr>
			<td>
				<a href="<?=$app->genLink('admin.crud.read', $table, $result->$pk)?>">Voir</a>
				<a href="<?=$app->genLink('admin.crud.update', $table, $result->$pk)?>">Modifier</a>
				<a href="<?=$app->genLink('admin.crud.delete', $table, $result->$pk)?>">Supprimer</a>
			</td>

			<?php foreach (get_object_vars($result) as $row): ?>
				<td><?=substr(Secure::html($row), 0, 100)?></td>
			<?php endforeach; ?>
		</tr>
	<?php endforeach; ?>
</table>
