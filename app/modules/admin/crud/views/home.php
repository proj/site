<ul>
	<?php foreach ($models as $model): ?>
		<li><a href="<?=$app->genLink('admin.crud.list', Format::underscorify($model))?>"><?=$model?></a></li>
	<?php endforeach; ?>
</ul>
