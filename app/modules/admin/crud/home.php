<?php

$app->get('admin.crud', 'admin/crud', function($app) {
	$app->filter('user.connected');

	if (!$app->has(P::SUPER_ADMINISTRATOR))
		$app->follow('forum.home');

	$classes      = get_declared_classes();
	$models       = [];
	$parent_model = new ReflectionClass('Model');

	foreach ($classes AS $class) {
		$cur_class = new ReflectionClass($class);

		if ($cur_class->isSubclassOf($parent_model)) {
			array_push($models, $cur_class->getName());
		}
	}

	$content = $app->view(__DIR__ . '/views/home.php');
	$content->models = $models;

	$page = $app->viewTpl();
	$page->title      = 'Accueil - CRUD - Administration';
	$page->content    = $content;
	$page->curModule  = 'admin';
	$page->breadcrumb = [
		'Administration' => $app->genLink('admin.home'),
		'CRUD' => false
	];

	echo $page;
});
