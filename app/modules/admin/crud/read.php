<?php

$app->get('admin.crud.read', 'admin/crud/read/{string}/{number}', function($app, $table, $id) {
	$app->filter('user.connected');

	if (!$app->has(P::SUPER_ADMINISTRATOR))
		$app->follow('forum.home');
});
