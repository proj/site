<?php

$app->get('admin.crud.list', 'admin/crud/list/{string}', function($app, $table) {
	$app->filter('user.connected');

	if (!$app->has(P::SUPER_ADMINISTRATOR))
		$app->follow('forum.home');

	$class_name = Format::camelcasify($table);
	$model      = (new ReflectionClass($class_name))->getName();
	$class      = new $model();
	$a          = new ReflectionClass($class);
	$attributes = $a->getProperties(ReflectionProperty::IS_PUBLIC);
	$results    = $class::getList();

	$_pk = $a->getProperty('_pk');
	$_pk->setAccessible(true);
	$pk = $_pk->getValue($class);
	$_pk->setAccessible(false);

	$content = $app->view(__DIR__ . '/views/list.php');
	$content->results    = $results;
	$content->attributes = $attributes;
	$content->table      = $table;
	$content->pk         = $pk;

	$page = $app->viewTpl();
	$page->title      = 'Accueil - CRUD - Liste';
	$page->content    = $content;
	$page->curModule  = 'admin';
	$page->breadcrumb = [
		'Administration' => $app->genLink('admin.home'),
		'CRUD'           => $app->genLink('admin.crud'),
		$model           => false
	];

	echo $page;
});
