<?php

$app->get('admin.manage-files', 'admin/manages_files', function($app) {
	$app->filter('user.connected');

	if (!$app->has(P::MODERATOR))
		$app->follow('forum.home');

	$content = $app->view(__DIR__ . '/views/manages-files.php');

	$page = $app->viewTpl();
	$page->title      = 'Gestion des fichiers';
	$page->content    = $content;
	$page->curModule  = 'admin';
	$page->breadcrumb = [
		'Administration' => $app->genLink('admin.home'),
		'Gestion des fichiers' => false
	];

	echo $page;
});
