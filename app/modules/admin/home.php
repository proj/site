<?php

$app->get('admin.home', 'admin', function($app) {
	$app->filter('user.connected');

	if (!$app->has(P::MODERATOR))
		$app->follow('forum.home');

	$content = $app->view(__DIR__ . '/views/home.php');
	$content->users = User::listUsers();

	$page = $app->viewTpl();
	$page->title      = 'Administration';
	$page->content    = $content;
	$page->curModule  = 'admin';
	$page->breadcrumb = [
		'Administration' => false
	];

	echo $page;
});
