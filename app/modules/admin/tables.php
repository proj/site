<?php

$app->get('admin.tables', 'admin/tables', function($app) {
	$app->filter('user.connected');

	if (!$app->has(P::SUPER_ADMINISTRATOR))
		$app->follow('forum.home');

	$list_tables = [];
	$result = DB::query('show tables');
	$result->execute();

	while ($row = $result->fetch(PDO::FETCH_NUM))
		$list_tables[] = $row[0];

	$tables = [];

	foreach ($list_tables as $table) {
		$result = DB::query('show columns from ' . $table);
		$result->execute();

		$tables[$table] = $result->fetchAll(PDO::FETCH_ASSOC);
	}

	$content = $app->view(__DIR__ . '/views/tables.php');
	$content->tables = $tables;

	$page = $app->viewTpl();
	$page->title      = 'Tables du site';
	$page->content    = $content;
	$page->curModule  = 'admin';
	$page->breadcrumb = [
		'Administration' => $app->genLink('admin.home'),
		'Tables du site' => false
	];

	echo $page;
});
