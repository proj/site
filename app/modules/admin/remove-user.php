<?php

$app->get('admin.remove-user', 'admin/remove_user/{alphanum}', function($app, $username) {
	if (!UserInfo::isLogged())
		$app->follow('site.home');

	if (!$app->has(P::SUPER_ADMINISTRATOR))
		$app->follow('forum.home');

	$an_user = UserInfo::getByUsername($username);

	if ($an_user->id) {
		$an_user->deleted = 1;
		$an_user->save();

		Flash::pushSuccess('Compte supprimé');
	} else {
		Flash::pushError('Compte inexistant');
	}

	$app->follow('admin.home');
});
