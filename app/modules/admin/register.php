<?php

$app->get('admin.register', 'admin/register', function($app) {
	if (!UserInfo::isLogged())
		$app->follow('site.home');

	if (!$app->has(P::ADMINISTRATOR))
		$app->follow('forum.home');

	$content = $app->view(__DIR__ . '/views/register.php');

	$page = $app->viewTpl();
	$page->title      = 'Enregistrement d\'un utilisateur';
	$page->content    = $content;
	$page->curModule  = 'admin';
	$page->breadcrumb = [
		'Administration' => $app->genLink('admin.home'),
		'Enregistrer un utilisateur' => false
	];

	echo $page;
})

->post('admin.register.confirm', 'admin/register/confirm', function($app) {
	if (!UserInfo::isLogged())
		$app->follow('site.home');

	if (!$app->has(P::ADMINISTRATOR))
		$app->follow('forum.home');

	$username = Input::get('username');
	$passwd1  = Input::get('passwd1');
	$passwd2  = Input::get('passwd2');

	if ($passwd1 == '' || $passwd2 == '')
		Flash::pushError('Mot de passe obligatoire');
	else if ($passwd1 != $passwd2)
		Flash::pushError('Les deux mots de passes ne sont pas identiques');

	if (preg_match('~[^a-zA-Z0-9_-]+~', $username))
		Flash::pushError('Format du pseudo incorrect');

	$username = User::cleanUsername($username);

	if (strlen($username) < 3) {
		Flash::pushError('Pseudo incorrect, il doit faire plus de 3 caractères');
	} else {
		$testUser = UserInfo::getByUsername($username);

		if ($testUser->id)
			Flash::pushError('Pseudo déjà utilisé');
	}

	if (Flash::hasNoErrors()) {
		$newUser = new UserInfo();

		$newUser->username = $username;
		$newUser->passwd   = Secure::genHash($passwd1);

		if (!$newUser->save())
			Flash::pushError('Il y a eu une erreur lors de l\'enregistrement de l\'utilisateur');
	}

	if (Flash::hasNoErrors())
		Flash::pushSuccess('Enregistrement de l\'utilisateur réussi');

	$app->follow('admin.home');
});
