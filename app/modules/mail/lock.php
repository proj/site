<?php

$app->get('mail.topic.lock', 'mail/topic/lock/{alphanum}', function($app, $slug) {
	$app->filter('user.connected');

	$topic = Mail::oneTopic($slug, $app->user);

	if (!$topic)
		$app->follow('mail.home');

	if (!$app->has(P::MODERATOR)) {
		if ($topic->locked)
			Flash::pushError('Vous n\'avez pas les droits pour déverrouiller ce sujet');
		else
			Flash::pushError('Vous n\'avez pas les droits pour verrouiller ce sujet');
	}

	$lockTopic = Mail::lockTopic($topic, $app->user);

	if ($lockTopic) {
		if ($topic->locked)
			Flash::pushSuccess('Sujet déverrouillé');
		else
			Flash::pushSuccess('Sujet verrouillé');

		$app->follow('mail.topic', $topic->slug);
	} else {
		if ($topic->locked)
			Flash::pushError('Il y a eu un problème lors du déverrouillage du sujet');
		else
			Flash::pushError('Il y a eu un problème lors de verrouillage du sujet');
	}

	$app->follow('mail.home');
});
