<a href="#" class="toggle-link" data-toggle="newtopic">Nouveau sujet</a>

<div id="newtopic" class="toggle">
		<form method="post" action="<?=$app->genLink('mail.post-mail')?>">
			<input type="text" name="title" placeholder="Titre" /><br />
			<input type="text" name="participants" placeholder="Destinataires" />

			<div class="write-message">
				<div class="message">
					<textarea name="content" placeholder="Message"></textarea>
				</div>
			</div>

			<input
				type="submit"
				name="new-topic"
				value="Valider" class="btn btn-primary btn-colored" />
		</form>
</div>

<?=$app->pagination(realpath(__DIR__ . '/../../../views/tpl/pagination.php'), 'mail.home', $p)?>

<div
<?php foreach ($topics as $topic): ?>
	<?php
		$class = 'icon-forum-t';
		$class .= $topic->date_last_read < $topic->date_last ? '-nl' : '-l';
	?>

	<div class="topic">
		<div class="topic-titre">
			<span class="<?=$class?>"></span>

			<a href="<?=$app->genLink('mail.topic', $topic->slug)?>">
				<?=htmlspecialchars($topic->title)?>
			</a>
		</div>

		<div class="topic-creator">
			<a href="<?=$app->genLink('user.view', $topic->author_begin)?>">
				<?=$topic->author_begin?>
			</a>
		</div>

		<div class="topic-last">
			<?=Date::format($topic->date_last, '{DD} {MO_tr} {YYYY}, {HH} h {MI}')?>
		</div>

		<div class="topic-nbmessages">
			<?=$topic->nb_messages?>
		</div>
	</div>
<?php endforeach; ?>

<?=$app->pagination(realpath(__DIR__ . '/../../../views/tpl/pagination.php'), 'mail.home', $p)?>
