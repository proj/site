<?=$app->pagination(realpath(__DIR__ . '/../../../views/tpl/pagination.php'), 'mail.topic', $p, $topic->slug)?>

<p>
	Participants :

	<?php foreach ($participants as $participant): ?>
		<a href="<?=$app->genLink('user.view', $participant->username)?>">
			<?=$participant->username?>
		</a>,
	<?php endforeach; ?>
</p>

<div class="messages-list">
	<?php foreach($messages as $message): ?>
		<div class="message" id="message-<?=$message->id?>">
			<div class="message-avatar">
				<a href="<?=$app->genLink('user.view', $message->username)?>">
					<img
						src="<?=UserInfo::getAvatarUrlByUsername($message->username)?>"
						alt="" />
				</a>
			</div>

			<div class="message-content">
				<div class="message-infos">
					<a
						href="<?=$app->genLink('user.view', $message->username)?>"
						class="message-username"
					>
						<?=$message->username?>
					</a>

					<a href="#message-<?=$message->id?>" class="message-little">
						<?=Date::relative($message->date_add)?>
					</a>
				</div>

				<div class="message-message">
					<?=BBMarkDown::staticFormat($message->content)?>
				</div>

				<?php if (!empty($message->signature)): ?>
					<div class="message-signature">
						<?=$message->signature?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	<?php endforeach; ?>
</div>

<?=$app->pagination(realpath(__DIR__ . '/../../../views/tpl/pagination.php'), 'mail.topic', $p, $topic->slug)?>

<div class="title-section">Répondre</div>

<div id="answer">
	<form
		method="post"
		action="<?=$app->genLink('mail.reply', $topic->slug, $p->actual)?>"
	>
		<div class="write-message">
			<div class="message">
				<textarea name="content" placeholder="Message"></textarea>
			</div>
		</div>

		<input
			type="submit"
			name="reply-topic"
			value="Répondre"
			class="btn btn-primary btn-colored" />
	</form>
</div>
