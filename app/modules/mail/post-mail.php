<?php

$app->post('mail.post-mail', 'mail/post_mail', function($app) {
	$app->filter('user.connected');

	if (!$app->has(P::POST_MAIL))
		Flash::pushError('Vous n\'avez pas les droits pour poster sur la messagerie');

	$title        = trim(Input::get('title'));
	$content      = trim(Input::get('content'));
	$participants = trim(Input::get('participants'));

	$participants = explode(',', $participants);

	$participants = array_map(function($partcipant) {
		return trim($partcipant);
	}, $participants);

	$ps = [];

	foreach ($participants as $participant) {
		$test_user = UserInfo::getByUsername($participant);

		array_push($ps, $test_user);

		if (is_null($test_user->id))
			Flash::pushError('Le destinataire "' . $participant . '" n\'existe pas');
	}

	if (empty($title))
		Flash::pushError('Veuillez saisir un titre');

	if (empty($content))
		Flash::pushError('Veuillez saisir un contenu');

	if (Flash::hasNoErrors()) {
		array_push($ps, $app->user);

		$insertTopic = Mail::addTopic($app->user, [
			'title'        => $title,
			'content'      => $content,
			'participants' => $ps
		]);

		if (!$insertTopic)
			Flash::pushError('Il y a eu une erreur lors de la création du sujet');
	}

	if (Flash::hasNoErrors()) {
		Flash::pushSuccess('Sujet créé');

		$topic = Mail::oneTopic($insertTopic, $app->user);

		$app->follow('mail.topic', $topic->slug);
	}

	$app->follow('mail.home');
});
