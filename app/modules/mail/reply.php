<?php

$app->post('mail.reply', 'mail/topic/{alphanum}{paginate}/reply', function($app, $slug, $page) {
	$app->filter('user.connected');

	if (!$app->user->has(P::POST_MAIL))
		Flash::pushError('Vous n\'avez pas les droits pour poster sur la messagerie');

	$topic = Mail::oneTopic($slug, $app->user);

	if ($topic->locked && !$app->user->has(P::MODERATOR))
		Flash::pushError('Le sujet est verrouillé');

	if (!$topic)
		$app->follow('mail.home');

	$content = trim(Input::get('content'));

	if (empty($content))
		Flash::pushError('Veuillez saisir un contenu');

	if (Flash::hasNoErrors()) {
		$insert_reply = Mail::addReply($topic, $app->user, [
			'content' => $content,
		]);

		if (!$insert_reply)
			Flash::pushError('Il y a eu une erreur lors de la création de la réponse');
	}

	if (Flash::hasNoErrors())
		Flash::pushSuccess('Réponse ajoutée');

	$p = new Pagination(Mail::nbMessagesTopic($topic), MESSAGES_BY_PAGE, $page);

	$app->follow('mail.topic', $topic->slug, $p->last);
});
