<?php

$app->get('mail.home', 'mail{paginate}', function($app, $page = 1) {
	$p = new Pagination(Mail::nbTopics($app->user), TOPICS_BY_PAGE, $page);

	$topics = Mail::listTopics($app->user, $p);

	$content = $app->view(__DIR__ . '/views/home.php');
	$content->topics = $topics;
	$content->p      = $p;

	$page = $app->viewTpl();
	$page->title      = 'Tout - Messagerie';
	$page->content    = $content;
	$page->curModule  = 'mail';
	$page->breadcrumb = [
		'Messagerie' => false
	];

	echo $page;
});
