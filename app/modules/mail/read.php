<?php

$app->get('mail.topic', 'mail/topic/{alphanum}{paginate}', function($app, $slug, $page = 1) {
	$app->filter('user.connected');

	$topic = Mail::oneTopic($slug, $app->user);

	if (!$topic)
		$app->follow('mail.home');

	$testRead = DB::select('
		date_last_read
		from mail_read
		where id_topic = :id_topic
		and id_user = :id_user
	', [
		'id_topic' => $topic->id,
		'id_user'  => $app->user->id
	])->one();

	if ($testRead) {
		DB::update('
			mail_read
			set date_last_read = :date_last_read
			where id_topic = :id_topic
			and id_user = :id_user
		', [
			'date_last_read' => time(),
			'id_topic'       => $topic->id,
			'id_user'        => $app->user->id
		]);
	} else {
		$read = new MailRead();
		$read->idTopic      = $topic->id;
		$read->idUser       = $app->user->id;
		$read->dateLastRead = time();
		$read->save();
	}

	$p = new Pagination(Mail::nbMessagesTopic($topic), MESSAGES_BY_PAGE, $page);

	$messages     = Mail::listMessagesTopic($topic, $p);
	$participants = Mail::participantsTopic($topic);

	$content = $app->view(__DIR__ . '/views/read.php');
	$content->topic        = $topic;
	$content->messages     = $messages;
	$content->p            = $p;
	$content->participants = $participants;

	$page = $app->viewTpl();
	$page->title      = htmlspecialchars($topic->title) . ' - Messagerie';
	$page->content    = $content;
	$page->curModule  = 'mail';
	$page->breadcrumb = [
		'Messagerie' => $app->genLink('mail.home'),
		htmlspecialchars($topic->title) => false
	];

	echo $page;
});
